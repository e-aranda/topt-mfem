/*
Convert MFEM Mesh files to VTK files for visualization in
Toptimiz3D
Also returns the spatial dimension of the mesh
*/


#include <fstream>
using std::ofstream;
#include <iostream>
using std::cout;
using std::endl;
using std::flush;


#include "mfem.hpp"

using namespace mfem;

class MyMesh : public Mesh
    {
    public:
        MyMesh(const char *_f) : Mesh(_f) {}
        void VTKConverter(std::ostream &out);
        ~MyMesh(){ }
    };

int main(int argc, char *argv[])
  {

    const char *mesh_file = "";
    const char *out_file = "";

    OptionsParser args(argc, argv);
    args.AddOption(&mesh_file, "-m", "--mesh",
                  "Mesh file to convert.");
    args.AddOption(&out_file, "-o", "--output",
                  "Output VTK file.");
    args.Parse();
 
    if (strcmp(mesh_file,"")==0)
    {
        cout << "No mesh file !!" << endl;
        args.PrintUsage(cout);
        return 0;
    }
    // Mesh reading
    MyMesh *mesh = new MyMesh(mesh_file);
    cout << "Read file: " << mesh_file << endl;
    
    int dim = mesh->Dimension();
    int ret = 0;
    if (dim==2 || dim==3) ret = dim;
    cout << "Mesh Dimension: " << dim << endl;

    if (strcmp(out_file,"") != 0)
    {
        ofstream ff(out_file);
        mesh->VTKConverter(ff);  
        cout << "Converted VTK file: " << out_file << endl;
    }
 
    delete mesh;

    return ret;
  }


void MyMesh::VTKConverter(std::ostream &out)
 {
    out <<
        "# vtk DataFile Version 3.0\n"
        "Generated by ERNESTO/MFEM\n"
        "ASCII\n"
        "DATASET UNSTRUCTURED_GRID\n";
 
    if (Nodes == NULL)
    {
       out << "POINTS " << NumOfVertices << " float\n";
       for (int i = 0; i < NumOfVertices; i++)
       {
          out << vertices[i](0);
          int j;
          for (j = 1; j < spaceDim; j++)
          {
             out << ' ' << vertices[i](j);
          }
          for ( ; j < 3; j++)
          {
             out << ' ' << 0.0;
          }
          out << '\n';
       }
    }
    else
    {
       Array<int> vdofs(3);
       out << "POINTS " << Nodes->FESpace()->GetNDofs() << " double\n";
       for (int i = 0; i < Nodes->FESpace()->GetNDofs(); i++)
       {
          vdofs.SetSize(1);
          vdofs[0] = i;
          Nodes->FESpace()->DofsToVDofs(vdofs);
          out << (*Nodes)(vdofs[0]);
          int j;
          for (j = 1; j < spaceDim; j++)
          {
             out << ' ' << (*Nodes)(vdofs[j]);
          }
          for ( ; j < 3; j++)
          {
             out << ' ' << 0.0;
          }
          out << '\n';
       }
    }
 

    int NumTotal = NumOfElements + NumOfBdrElements;

    int order = -1;
    if (Nodes == NULL)
    {
       int size = 0;
       for (int i = 0; i < NumOfElements; i++)
       {
          size += elements[i]->GetNVertices() + 1;
       }
       
       for (int i = 0; i< NumOfBdrElements; i++)
       {
          size += boundary[i]->GetNVertices() + 1;
       } 


       out << '\n';

       out << "CELLS " << NumTotal << ' ' << size << '\n';
       for (int i = 0; i < NumOfElements; i++)
       {
          const int *v = elements[i]->GetVertices();
          const int nv = elements[i]->GetNVertices();
          out << nv;
          for (int j = 0; j < nv; j++)
          {
             out << ' ' << v[j];
          }
          out << '\n';
       }
       for (int i = 0; i < NumOfBdrElements; i++)
       {
          const int *v = boundary[i]->GetVertices();
          const int nv = boundary[i]->GetNVertices();
          out << nv;
          for (int j = 0; j < nv; j++)
          {
             out << ' ' << v[j];
          }
          out << '\n';
       }
       order = 1;
    }
    else
    {
       Array<int> dofs;
       int size = 0;
       for (int i = 0; i < NumOfElements; i++)
       {
          Nodes->FESpace()->GetElementDofs(i, dofs);
          MFEM_ASSERT(Dim != 0 || dofs.Size() == 1,
                      "Point meshes should have a single dof per element");
          size += dofs.Size() + 1;
       }

       out << "CELLS " << NumOfElements << ' ' << size << '\n';
       const char *fec_name = Nodes->FESpace()->FEColl()->Name();
 
       if (!strcmp(fec_name, "Linear") ||
           !strcmp(fec_name, "H1_0D_P1") ||
           !strcmp(fec_name, "H1_1D_P1") ||
           !strcmp(fec_name, "H1_2D_P1") ||
           !strcmp(fec_name, "H1_3D_P1"))
       {
          order = 1;
       }
       else if (!strcmp(fec_name, "Quadratic") ||
                !strcmp(fec_name, "H1_1D_P2") ||
                !strcmp(fec_name, "H1_2D_P2") ||
                !strcmp(fec_name, "H1_3D_P2"))
       {
          order = 2;
       }
       if (order == -1)
       {
          mfem::err << "Mesh::PrintVTK : can not save '"
                    << fec_name << "' elements!" << endl;
          mfem_error();
       }
       for (int i = 0; i < NumOfElements; i++)
       {
          Nodes->FESpace()->GetElementDofs(i, dofs);
          out << dofs.Size();
          if (order == 1)
          {
             for (int j = 0; j < dofs.Size(); j++)
             {
                out << ' ' << dofs[j];
             }
          }
          else if (order == 2)
          {
             const int *vtk_mfem;
             switch (elements[i]->GetGeometryType())
             {
                case Geometry::SEGMENT:
                case Geometry::TRIANGLE:
                case Geometry::SQUARE:
                   vtk_mfem = vtk_quadratic_hex; break; // identity map
                case Geometry::TETRAHEDRON:
                   vtk_mfem = vtk_quadratic_tet; break;
                case Geometry::CUBE:
                default:
                   vtk_mfem = vtk_quadratic_hex; break;
             }
             for (int j = 0; j < dofs.Size(); j++)
             {
                out << ' ' << dofs[vtk_mfem[j]];
             }
          }
          out << '\n';
       }
    }

    out << "\n";
 
    out << "CELL_TYPES " << NumTotal << '\n';
    for (int i = 0; i < NumOfElements; i++)
    {
       int vtk_cell_type = 5;
       if (order == 1)
       {
          switch (elements[i]->GetGeometryType())
          {
             case Geometry::POINT:        vtk_cell_type = 1;   break;
             case Geometry::SEGMENT:      vtk_cell_type = 3;   break;
             case Geometry::TRIANGLE:     vtk_cell_type = 5;   break;
             case Geometry::SQUARE:       vtk_cell_type = 9;   break;
             case Geometry::TETRAHEDRON:  vtk_cell_type = 10;  break;
             case Geometry::CUBE:         vtk_cell_type = 12;  break;
             default: break;
          }
       }
       else if (order == 2)
       {
          switch (elements[i]->GetGeometryType())
          {
             case Geometry::SEGMENT:      vtk_cell_type = 21;  break;
             case Geometry::TRIANGLE:     vtk_cell_type = 22;  break;
             case Geometry::SQUARE:       vtk_cell_type = 28;  break;
             case Geometry::TETRAHEDRON:  vtk_cell_type = 24;  break;
             case Geometry::CUBE:         vtk_cell_type = 29;  break;
             default: break;
          }
       }
 
       out << vtk_cell_type << '\n';
    }

    for (int i = 0; i < NumOfBdrElements; i++)
    {
       int vtk_cell_type = 5;
       if (order == 1)
       {
          switch (boundary[i]->GetGeometryType())
          {
             case Geometry::POINT:        vtk_cell_type = 1;   break;
             case Geometry::SEGMENT:      vtk_cell_type = 3;   break;
             case Geometry::TRIANGLE:     vtk_cell_type = 5;   break;
             case Geometry::SQUARE:       vtk_cell_type = 9;   break;
             case Geometry::TETRAHEDRON:  vtk_cell_type = 10;  break;
             case Geometry::CUBE:         vtk_cell_type = 12;  break;
             default: break;
          }
       }
       else if (order == 2)
       {
          switch (boundary[i]->GetGeometryType())
          {
             case Geometry::SEGMENT:      vtk_cell_type = 21;  break;
             case Geometry::TRIANGLE:     vtk_cell_type = 22;  break;
             case Geometry::SQUARE:       vtk_cell_type = 28;  break;
             case Geometry::TETRAHEDRON:  vtk_cell_type = 24;  break;
             case Geometry::CUBE:         vtk_cell_type = 29;  break;
             default: break;
          }
       }
 
       out << vtk_cell_type << '\n';
    }




 
    // write attributes
    out << "CELL_DATA " << NumTotal << '\n'
        << "SCALARS material int\n"
        << "LOOKUP_TABLE default\n";
    for (int i = 0; i < NumOfElements; i++)
    {
       out << elements[i]->GetAttribute() << '\n';
    }
    for (int i = 0; i < NumOfBdrElements; i++)
    {
       out << boundary[i]->GetAttribute() << '\n';
    }
 
    out.flush();
 }
 