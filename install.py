#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Install script for toptimiz3D
"""
import sys
if sys.version_info < (3,0):
    print("Wrong Python version. You need Python 3")
    exit(1)
    
import importlib
import argparse
import os
import shutil
import tempfile
import platform
import subprocess

def printred(s):
    print('\033[91m' + s + '\033[0m')

def printgreen(s):
    print('\033[92m' + s + '\033[0m')

def printyellow(s):
    print('\033[93m' + s + '\033[0m')

    
    
sourcemake = """
{var}


FC = g++

FFLAGS = -O3 -std=c++11 $(CPPFLAGS)
OTHER =  $(LDFLAGS) -lmfem -lm {umf} $(LIBRARY)

DIR=$(BASEDIR)/src
BIN=$(BASEDIR)/bin

nlopt : $(DIR)/MAIN.cpp $(DIR)/functions.cpp $(DIR)/FUN_NLOPT.cpp
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER)	

ipopt : $(DIR)/MAIN.cpp $(DIR)/functions.cpp $(DIR)/FUN_IPOPT.cpp
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER)	

oc : $(DIR)/MAIN.cpp $(DIR)/functions.cpp $(DIR)/FUN_OC.cpp
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER)		

"""    

# arguments (directory for installation)

parser = argparse.ArgumentParser()
parser.add_argument('--dir', nargs='?', default=None, 
                    help = "Optional installation directory")
parser.add_argument('--ld', nargs='+', default=None, 
                    help = 'LDDFLAGS for configure: example --ld="-L/path/to/1 -L/path/to/2"')

parser.add_argument('--cpp', nargs='+', default=None, 
                    help = 'CPPFLAGS for configure: example --cpp="-I/path/to/1 -I/path/to/2"')

parser.add_argument('--suitedir', nargs='+', default=None, 
                    help = 'SuiteSparse dir for compilation of MFEM: example --suitedir=/path/to/SuiteSparse')

parser.add_argument('--suitesparse', default=False, action="store_true",
                    help = 'Tell configuration script that MFEM is or has to be compiled with SuiteSparse')

parser.add_argument('--yes', default=False, action="store_true",
                    help = 'Yes answer to questions')

args = parser.parse_args()


############################ 
# check platform
if platform.system().lower() != "linux" and platform.system().lower() != "darwin":
    print("Sorry, platform " + platform.system() + " not supported")
    exit(1)
else:
    print("Platform: " + platform.system()+'\n')


############################

############################    
# Check Python modules installed
    
############################    
# Check modules installed
install = True
    
try:
    import vtk
    printgreen("vtk is installed in version " + vtk.vtkVersion.GetVTKVersion())
    if int(vtk.vtkVersion.GetVTKVersion().split('.')[0]) < 6:
        printred("vtk version is not correct. Minimum version required is 6")
        install = False
except ImportError:
    printred("VTK module is required")
    install = False
try:
    import wx
    printgreen("WxPython is installed in version " + wx.version().split()[0])
    if int(wx.version()[0]) != 4:
        printred("WxPython version is not correct. Version required: 4")
        install = False
except ImportError:
    printred("WxPython module is required")
    install = False
       
for x in ["numpy","pexpect","requests","dictdiffer"]:
    try:
        importlib.import_module(x)
        printgreen(x + " is installed in version " + importlib.import_module(x).__version__)
    except:
        print(x + " module is required")
        install = False        

if not install:
    printred("\nToptimiz3D cannot be installed")
    printred("Check problems above")
    exit(1)
    
    
import requests
import tarfile


############################    
# Download function
    
def download(url,filename):
    fullurl = url + filename
    try:
        r = requests.get(fullurl)
    except IOError as e:
        printred("Can't retrieve "+ fullurl)
        exit(1)
    tmpdir = tempfile.mkdtemp()
    filezip = os.path.join(tmpdir,filename)
    open(filezip,'wb').write(r.content)
    try:
        tar = tarfile.open(filezip, "r:gz")
    except:
        print("Can't uncompress")
        exit(1)
   
    tar.extractall()
    dirname = os.path.commonprefix(tar.getnames())
    tar.close()     
    
    return dirname
    


############################    
# Base folder 
    
initfolder = os.path.dirname(os.path.abspath(__file__))    
print('')
os.chdir(initfolder)


############################################
# preconfiguration: processing arguments
############################################
lflags = cflags = ''

os.chdir(os.path.join(initfolder,'config','preconf'))
if args.ld:
    lflags = 'LDFLAGS="' + args.ld[0] + '"'
if args.cpp:
    cflags = 'CPPFLAGS="' + args.cpp[0] + '"'    


if args.dir:
    folder = args.dir
else:
    folder =  os.path.join(os.path.expanduser('~'),'toptimiz3D')
mypath = 'MYPATH=' + os.path.join(folder,'bin')


if args.suitesparse:
    extra = '--enable-suitesparse'
    if args.suitedir:
        withsparse = ' MFEM_USE_SUITESPARSE=YES SUITESPARSE_DIR=' + args.suitedir[0] 
        if args.ld:
            lflags = lflags[:-1] + ' -L' + os.path.join(args.suitedir[0],'lib') + '"'
        else:
            lflags = 'LDFLAGS="-L' + os.path.join(args.suitedir[0],'lib') + '"'
        if args.cpp:
            cflags = cflags[:-1] + ' -I' + os.path.join(args.suitedir[0],'include') + '"'
        else:
            cflags = 'CPPFLAGS="-I' + os.path.join(args.suitedir[0],'include') + '"'
    else:
        withsparse = ' MFEM_USE_SUITESPARSE=YES'
        if args.cpp:
            withsparse += ' SUITESPARSE_OPT=' + args.cpp[0]
        printyellow("Warning! You asked compilation with SuiteSparse but you have \
not provided a directory for SuiteSparse. If you find errors try to add the right libraries")
else:
    extra = ''   
    withsparse = ''



command = './configure ' + lflags + ' ' + cflags + ' ' + mypath + ' ' + extra
print(command)
err = subprocess.call(command,shell=True) 

if err:
    printred("Pre-Configuration Error! Check config.log")
    exit(1)

# preconfiguration results
results = []
with open("config.log",'r') as f:
    for line in f:
        if 'topt_' in line:
            results.append(line.strip())               
        
for x in results[:]:
    if 'yes' in x:
        results.remove(x)

missing_lib = []     
missing_head = []   
missing_prog = []

for x in results:
    if 'lib' in x:
        missing_lib.append(x.split('=')[0].split('_')[-1])
    elif 'header' in x:
        missing_head.append(x.split('=')[0].split('_')[-1])
        continue
    elif 'prog' in x:
        missing_prog.append(x.split('=')[0].split('_')[-1])
     
# checking what to install        
b_glvis = True
b_cmake = True
bgl = 0

for x in missing_prog:
    if 'glvis' in x:
        bgl += 1
    elif  'other' in x:
        bgl += 1
    elif 'cmake' in x:
        b_cmake = False
if bgl == 2:
    b_glvis = False        
b_gsl = True
b_mfem = True
b_ipopt = True
b_nlopt = True
for x in missing_lib:
    if 'gsl' in x:
        b_gsl = False
    elif 'mfem' in x:
        b_mfem = False
    elif 'ipopt' in x:
        b_ipopt = False
    elif 'nlopt' in x:
        b_nlopt = False
    
b_eigen = True
for x in missing_head:
    if 'eigen' in x:
        b_eigen = False
    elif b_mfem and 'mfem' in x:
        printred('Error: MFEM library is installed but headers are not found or there is an error. Check installation.')
        exit(1)
    elif b_ipopt and 'ipopt' in x:
        printred('Error: IPOPT library is installed but headers are not found. Check installation. Aborting ...')
        exit(1)
    elif b_nlopt and 'nlopt' in x:
        printred('Error: NLOPT library is installed but headers are not found. Check installation. Aborting ...')
        exit(1)

if not b_cmake and not b_nlopt:
    printred('Error: cmake is not installed and it is required by NLOPT installation. Please, install cmake first. Aborting ...')
    exit(1)
    
printgreen('\n\n\nSummary:\n******************************')
if b_mfem:
    print('MFEM is installed')
else:
    print('MFEM is going to be installed',end=' ')
    if args.suitesparse:
        print('with SUITESPARSE')
    else:
        print()
if b_ipopt:
   print('IPOPT is installed')
else:
    printred('IPOPT is not found. You can continue with the installation but you cannot use IPOPT method')        
if b_nlopt:
    print('NLOPT is installed')
else:    
    print('NLOPT is going to be installed')        
if b_eigen:
    print('EIGEN is installed')
else:    
    print('EIGEN is going to be installed')    
if b_glvis:
    print('GLVIS is installed')
else:    
    print('GLVIS is going to be installed')    
if b_gsl:
    print('GSL is installed')
else:    
    print('GSL is going to be installed')    
printgreen('******************************\n\n\n')

asking = True
if b_mfem and not b_glvis:
    while asking:
        srcmfem = input("Warning: MFEM is installed but GLVIS is not. We need to know the MFEM \
          installation folder (Enter X to abort)\n")
        if srcmfem == "X" or srcmfem == "x":
            exit(0)
        elif not os.path.exists(srcmfem):
            print("Directory does not exist. Try again")
        else:
            asking = False
    

########################################
# base folders
#######################################


 
if not os.path.exists(folder):
    try:
        print('Destination folder: '+folder)    
        os.makedirs(folder)
    except OSError as e:
        print(e.args)
        printred("Installation directory can not be created. Please run again this script with a valid destination folder")
        exit(1)
else:
    print("Attention! installation directory exists. Content could be overwritten.")
    
if args.yes:
    asking = False
else:
    asking = True
while asking:
    answer = input('Continue? [Y/n] ')
    if answer in 'yY':
        asking = False
    elif answer in 'nN':
        asking = False
        exit(0)
    else:
        print("Please answer 'y' or 'n'")


os.chdir(initfolder)
# Create folder for .py files
codefolder = os.path.join(folder,'code')
if not os.path.exists(codefolder):
    try:
        os.makedirs(codefolder)
    except OSError as e:
        raise Exception(e.args)

# Copy VERSION and LICENSE files
for x in ('LICENSE','VERSION','README.md'):        
    dst = os.path.join(folder,x)
    print("Copying " + x)
    try:
        shutil.copyfile(x,dst)
    except IOError as e:
        raise Exception(e.args)

# Copying Python files and icons
print("Coping Python code ... \n")
for x in os.listdir('code'):
    src = os.path.join('code',x)
    dst = os.path.join(codefolder,x)
    if not os.path.isdir(src):
        print("Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)
    else:
        try:
            os.makedirs(dst)
            print("Directory " + x + " created")
        except OSError as e:
            print("Enter in " + x + " ...")
        for y in os.listdir(src):
            print("  Copying " + y + " ...")
            ini = os.path.join(src,y)
            dst = os.path.join(folder,ini)
            try:
                shutil.copyfile(ini,dst)
            except IOError as e:
                raise Exception(e.args)

# Example folder
print("Copying Examples ...\n")        
examplefolder = os.path.join(folder,'examples')
if not os.path.exists(examplefolder):
    try:
        os.makedirs(examplefolder)
        print("Directory examples created")
    except OSError as e:
        raise Exception(e.args)

# Copying examples
for x in os.listdir('examples'):
    src = os.path.join('examples',x)
    dst = os.path.join(examplefolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)

# C++ folder
print("Copying C++ files ... \n")        
cppfolder = os.path.join(folder,'src')
if not os.path.exists(cppfolder):
    try:
        os.makedirs(cppfolder)
        print("Directory src created")
    except OSError as e:
        raise Exception(e.args)

# Copying C++ files
for x in os.listdir('src'):
    src = os.path.join('src',x)
    dst = os.path.join(cppfolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)            

# Manual folder
print("Copying manual file ... \n")        
docfolder = os.path.join(folder,'doc')
if not os.path.exists(docfolder):
    try:
        os.makedirs(docfolder)
        print("Directory doc created")
    except OSError as e:
        raise Exception(e.args)

# Copying manual file
for x in os.listdir('doc'):
    src = os.path.join('doc',x)
    dst = os.path.join(docfolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)            
print('')

carpetas = ['bin']
nflags = ''
if not (b_mfem and b_ipopt and b_nlopt and b_gsl):
    carpetas.extend(['lib','include'])
    if not lflags:
        lflags = 'LDFLAGS="-L' + os.path.join(folder,'lib') + '"'
    else:
        lflags = lflags[:-1] + ' -L' + os.path.join(folder,'lib') + '"'
    if not cflags:
        cflags = 'CPPFLAGS="-I' + os.path.join(folder,'include') + '"'
    else:
        cflags = cflags[:-1] + ' -I' + os.path.join(folder,'include') + '"'
elif not b_eigen:
    carpetas.append('include')
    if not cflags:
        cflags = 'CPPFLAGS="-I' + os.path.join(folder,'include') + '"'
    else:
        cflags = cflags[:-1] + ' -I' + os.path.join(folder,'include') + '"'


# bin lib include folders
for x in carpetas:
    newfolder = os.path.join(folder,x)
    if not os.path.exists(newfolder):
        try:
            os.makedirs(newfolder)
            print("Directory",x,"created")
        except OSError as e:
            raise Exception(e.args)
############################################
    

# Platform adjustment
if platform.system().lower() == "darwin":
    confipopt = ' --with-blas="-framework Accelerate"'
    shared = "-DBUILD_SHARED_LIBS=OFF" 
else:
    confipopt = ''
    shared = ''


############################################        
# Download and install packages
     
downfolder = os.path.join(initfolder,'download')    
    
# MFEM download and compilation            
if not b_mfem:
    srcmfem = os.path.join(downfolder,'mfem')            
    print("Downloading MFEM")    
    dirname = download('https://github.com/mfem/mfem/archive/','master.tar.gz')
    if os.path.exists(srcmfem):
        shutil.rmtree(srcmfem,ignore_errors=True)
    shutil.move(dirname,srcmfem)
    os.chdir(srcmfem)
    subprocess.call("make serial -j 4" + withsparse,shell=True)
    subprocess.call("make check",shell=True)
    subprocess.call("make install PREFIX="+folder,shell=True)
    os.chdir(initfolder)
    
# GLVIS download and compilation
if not b_glvis:
    src = os.path.join(downfolder,'glvis')            
    print("Downloading GLVIS")   
    dirname = download('https://github.com/GLVIS/glvis/archive/','master.tar.gz')
    if os.path.exists(src):
        shutil.rmtree(src,ignore_errors=True)
    shutil.move(dirname,src)
    os.chdir(src)
    subprocess.call("make MFEM_DIR="+srcmfem,shell=True)
    shutil.copy("glvis",os.path.join(folder,'bin','glvis'))
    os.chdir(initfolder)

# NLOPT download and compilation
if not b_nlopt:
    src = os.path.join(downfolder,'nlopt')            
    print("Downloading NLOPT")    
    dirname = download('https://github.com/stevengj/nlopt/archive/','v2.5.0.tar.gz')
    if os.path.exists(src):
        shutil.rmtree(src,ignore_errors=True)    
    shutil.move(dirname,src)
    os.chdir(src)    
    os.makedirs('build')
    os.chdir(os.path.join(src,'build'))
    subprocess.call("cmake " + shared + " -DCMAKE_INSTALL_PREFIX="+folder+" ..",shell=True)
    subprocess.call("make",shell=True)
    subprocess.call("make install",shell=True)
    os.chdir(initfolder)


#
## IPOPT download and compilation
#if not b_ipopt:
#    src = os.path.join(downfolder,'Ipopt')            
#    print("Downloading IPOPT")    
#    dirname = download('https://www.coin-or.org/download/source/Ipopt/','Ipopt-3.12.12.tgz')
#    if os.path.exists(src):
#        shutil.rmtree(src,ignore_errors=True)    
#    shutil.move(dirname,src)
#    os.chdir(src)    
#    os.makedirs('build')
#    os.chdir(os.path.join(src,'build'))
#    subprocess.call("../configure --prefix="+folder+confipopt,shell=True)
#    subprocess.call("make",shell=True)
#    subprocess.call("make install",shell=True)
#    os.chdir(initfolder)


# GSL download and compilation
if not b_gsl:
    src = os.path.join(downfolder,'gsl')            
    print("Downloading GSL")    
    dirname = download('https://ftp.gnu.org/gnu/gsl/','gsl-latest.tar.gz')
    if os.path.exists(src):
        shutil.rmtree(src,ignore_errors=True)    
    shutil.move(dirname,src)
    os.chdir(src)    
    subprocess.call("./configure --prefix="+folder,shell=True)
    subprocess.call("make",shell=True)
    subprocess.call("make install",shell=True)
    os.chdir(initfolder)    
    
# Eigen download
    
if not b_eigen:
    src = os.path.join(folder,'include','Eigen')            
    os.chdir(downfolder)
    print("Downloading Eigen")    
    dirname = download('http://bitbucket.org/eigen/eigen/get/','3.3.7.tar.gz')
    if os.path.exists(src):
        shutil.rmtree(src,ignore_errors=True)    
    shutil.move(os.path.join(dirname,'Eigen'),src)
    os.chdir(initfolder)


############################################
    
############################################        
# Compilation of meshtovtk
if b_ipopt:
    extra += ' --enable-ipopt'
    
command = './configure --prefix=' + folder + ' ' + lflags + ' ' + cflags + ' ' + mypath + ' ' + extra
print(command)
os.chdir(os.path.join(initfolder,'config'))
err = subprocess.call(command,shell=True) 

if err:
    printred("Configuration Error! Check config.log")
    exit(1)
else:
    er = subprocess.call("make",shell=True)
    if er:
        printred("Compilation Error!")
        exit(1)
    else:
        subprocess.call("make install",shell=True)



############################################
    
############################################     
# Creating makefile        

# correct path for eigen        
with open("config.log",'r') as f:
    for line in f:
        if 'ac_cv_header' in line and 'yes' in line and 'eigen' in line.lower():
            eig = line
            break
    else:
        print("Error: eigen headers are not found")
        exit(1)
eigpath = eig.split('header')[-1].split('=')[0].split('_')
eig = os.path.join(*eigpath)

with open(os.path.join(folder,'src','functions.hpp'),'r') as f:
    functions = f.read()
        
with open(os.path.join(folder,'src','functions.hpp'),'w') as f:
    f.write(functions.replace('???',eig))

    
ldcam = subprocess.getoutput("grep LDFLAGS Makefile")
cpcam = subprocess.getoutput("grep CPPFLAGS Makefile")

varss = ldcam + '\n' + cpcam + '\n' + 'BASEDIR = ' + folder 

if args.suitesparse:
    umf = '-lklu -lumfpack'
else:
    umf = ''

makef = os.path.join(folder,'src','makefile')
with open(makef,'w') as f:
    f.write(sourcemake.format(var=varss,umf=umf))

        
#################################################


#################################################

os.chdir(initfolder)

# Creating desktop launcher
asking = True
while asking:
    if args.yes:
        answer = 'y'
    else:
        answer = input('Do you want to create a Desktop Launcher? [Y/n] ')

    if answer == '' or answer in 'yY':
        if platform.system().lower() == "linux":
            print("Creating Desktop Launcher for Linux")
            # Determine the name of Desktop folder
            try:
                desktopfolder = subprocess.check_output(['xdg-user-dir', 'DESKTOP'])[:-1]
            except:
                desktopfolder = os.path.join(os.path.expanduser('~'),'Desktop')

            lanzador = \
"""
[Desktop Entry]
Name=Toptimiz3d
Exec=bash -i {pathtolauncher}
Terminal=false
StartupNotify=true
Type=Application
Icon={icon}
"""

            launcher = \
"""
#!/usr/bin/bash
cd {home}
{path}
"""
            
            # create executable desktop file
            appfile = os.path.join(desktopfolder.decode('utf-8'),'Toptimiz3D.desktop')
            appcommand = subprocess.getoutput('which python3') + ' ' + os.path.join(folder,'code','toptimiz3d.py')
            applauncher = os.path.join(folder,'code','icons','launcher.sh')
            with open(applauncher,'w') as f:
                f.write(launcher.format(home=os.path.expanduser('~'),path=appcommand))
            dst = os.path.join(folder,'code','icons','topicon.gif')
            
            with open(appfile,'w') as f:
                f.write(lanzador.format(pathtolauncher=applauncher,icon=dst))
            os.chmod(appfile,0o755)
            os.chmod(applauncher,0o755)

        elif platform.system().lower() == "darwin":
            print("Creating Desktop Launcher for MacOS")
            desktopfolder = os.path.join(os.path.expanduser('~'),'Desktop')
            shellscript = 'do shell script "{path} ; launchctl setenv PATH $PATH; cd ' + os.path.join(folder,'code') + ' ;  {command}"'

            appfile = os.path.join(desktopfolder,'Toptimiz3D.app')
            special = "$'" + appfile + r"/Icon\r'"
            setpath = 'PATH='+ subprocess.getoutput('which python')[:-7] +':$PATH '
            appcommand = 'pythonw ' + os.path.join(folder,'code','toptimiz3d.py')
            with open('shellscript.scpt','w') as f:
                f.write(shellscript.format(path=setpath,command=appcommand))
            os.system('osacompile -o ' + appfile + ' shellscript.scpt')
            # set icon to code folder
            icon = os.path.join('code','icons','topicon.gif')
            fileicon = os.path.join('code','icons','fileicon.sh')
            os.system('bash ' + fileicon + ' set ' + appfile + ' ' + icon)

        asking = False
    elif answer in 'nN' or answer == '':
        asking = False
    else:
        print("Please, answer y or n")



print("\nInstallation is done")

