#include "functions.hpp"
#include "func_OC.hpp"


#ifndef TOPTIMIZ_OC
#define TOPTIMIZ_OC

// evaluate compliance and volume in OC
void ToptimizOC::evaluation() 
	{
		Filter &u = *rho;

		$filtering$
		$extra$

		a->Update();
		a->Assemble();
		
		$density$

		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
		    
		// Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif
		
		compliance = ((Vector) *b) * U;
		double obj_value = Mobj*compliance;

		vol->Update();
		vol->Assemble();
		double volumen = ((Vector) *y) * ((Vector) *vol);
		volumen /= Volconstant;

		der->Update();
		der->Assemble();


		rho->FilMult(*der,Y);

		$vectorD_OC$

		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ")  Restr: " << volumen << endl; 
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Compliance: " << compliance << "'" << endl;          
		}

		siter++;
	}

// OC iteration	
void ToptimizOC::OCiteration(int &maxiter, double &tol)
	{
		Filter &u = *rho;
		Vector x(n);
		// initial rho
		for (int i=0; i< n; i++)
			x(i) = u(i);

		// main iteration of OC
		int k = 0;
		double change = 1.;
		double l1, l2, lmid, volumen;
		Vector rl(n);

		while ( k<maxiter && change > tol )
 		{ 
    		k++;
    		evaluation();
    		l1=0.; 
    		l2=1.e9; 
    		for (int i=0; i< n; i++)
    			rl(i) = x(i)*pow(max(0.,-Mobj*Y(i)/D(i)),neta);	
    		while ( (l2-l1)/(l1+l2) > 1.e-3) 
    		{
    			lmid = 0.5*(l1+l2);
    			double plmid = 1./pow(lmid,neta);
    			for (int i=0; i< n; i++)
    			{
       				u(i) = max( 0., max(x(i)*(1-zeta) , min( 1. , min( x(i)*(1+zeta) , rl(i)*plmid ) ) ) );       
    			}

    			$filtering$

    			vol->Update();
				vol->Assemble();
				volumen = ((Vector) *y) * ((Vector) *vol);
				(volumen > Volconstant) ? l1 = lmid : l2 = lmid;
	    	}
	    	for (int i=0; i<n; i++)
	    		rl(i) = x(i)-u(i);
	    	change = rl.Normlinf();
	    	// update
	    	for (int i=0; i < n; i++)
			    x(i) = u(i);
	    }

    }



// evaluate compliance and volume in OC
void ToptimizOCML::evaluation() 
	{
		Filter &u = *rho;

		$filtering$
		$extra$

		SparseMatrix A[n_f];
	    Vector B[n_f], U[n_f];
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M;
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	    #endif
	    compliance = 0.;
	    double compli;

	    a->Update();
	    a->Assemble();
	    for (int i=0; i<n_f; i++)
	    {
	        a->FormLinearSystem(ess_tdof_list, *Udisp[i], *b[i], A[i], U[i], B[i]);

	        // Solving system with PCG or UMFPACK (if installed)   
	        #ifndef MFEM_USE_SUITESPARSE
	            M = GSSmoother(A[i]);
	            PCG(A[i], M, B[i], U[i], 0, ITER, TOL, 0.0);
	        #else
	            umf_solver.SetOperator(A[i]);
	            umf_solver.Mult(B[i], U[i]);
	        #endif
	        
	        a->RecoverFEMSolution(U[i], *b[i], *Udisp[i]);
	        compli = ((Vector) *b[i]) * U[i];
	        compliance += lambdacoef[i]*compli;
	    }

	    double obj_value = Mobj*compliance;

		vol->Update();
		vol->Assemble();
		double volumen = ((Vector) *y) * ((Vector) *vol);
		volumen /= Volconstant;

		Vector Ys[n_f];
		for (int i=0;i<n_f;i++)
		{
			Ys[i].SetSize(n);
			der[i]->Update();
			der[i]->Assemble();
			rho->FilMult(*der[i],Ys[i]);
		}

		for (int i=0; i<n; i++)
		{
			Y(i) = 0.;
			for (int k=0;k<n_f;k++)
    			Y(i) += Mobj*lambdacoef[k]*Ys[k](i);
		}
 	
		
		$vectorD_OC$

		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ")  Restr: " << volumen << endl; 
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Compliance: " << compliance << "'" << endl;          
		}

		siter++;
	}


// OC iteration	
void ToptimizOCML::OCiteration(int &maxiter, double &tol)
	{
		Filter &u = *rho;
		Vector x(n);
		// initial rho
		for (int i=0; i< n; i++)
			x(i) = u(i);

		// main iteration of OC
		int k = 0;
		double change = 1.;
		double l1, l2, lmid, volumen;
		Vector rl(n);

		while ( k<maxiter && change > tol )
 		{ 
    		k++;
    		evaluation();
    		l1=0.; 
    		l2=1.e9; 
    		for (int i=0; i< n; i++)
    			rl(i) = x(i)*pow(max(0.,-Mobj*Y(i)/D(i)),neta);	
    		while ( (l2-l1)/(l1+l2) > 1.e-3) 
    		{
    			lmid = 0.5*(l1+l2);
    			double plmid = 1./pow(lmid,neta);
    			for (int i=0; i< n; i++)
    			{
       				u(i) = max( 0., max(x(i)*(1-zeta) , min( 1. , min( x(i)*(1+zeta) , rl(i)*plmid ) ) ) );       
    			}

    			$filtering$

    			vol->Update();
				vol->Assemble();
				volumen = ((Vector) *y) * ((Vector) *vol);
				(volumen > Volconstant) ? l1 = lmid : l2 = lmid;
	    	}
	    	for (int i=0; i<n; i++)
	    		rl(i) = x(i)-u(i);
	    	change = rl.Normlinf();
	    	// update
	    	for (int i=0; i < n; i++)
			    x(i) = u(i);
	    }

    }




#endif