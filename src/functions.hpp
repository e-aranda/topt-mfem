#include <string>
using std::string;
#include <ctime>

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <iostream>
using std::cout;
using std::endl;
using std::flush;

#include <vector>
using std::vector;

using std::max;
using std::min;

// for random initialization
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <eigen3/Eigen/Eigen>

#include "mfem.hpp"

using namespace mfem;

#ifndef TOPTIMIZ_GNR
#define TOPTIMIZ_GNR

// constants of Toptimiz3D
#include "constants.hpp"



// Personal class for extend mesh-vtkwriter

class MyMesh : public Mesh
    {
    public:
        MyMesh(const char *_f) : Mesh(_f) {}
        void VTKdensity(std::ostream &out, GridFunction *y);
        void VTKdisplace(std::ostream &out, GridFunction *w, GridFunction *U);
        void VTKConverter(std::ostream &out,GridFunction *y);
        void VTKConverter(std::ostream &out,GridFunction *y, GridFunction *w, GridFunction *U);
        ~MyMesh(){ }
    };


// Abstrac class for filters
class Filter : public GridFunction
	{
	public:
	   Mesh *mesh;
	   const double radius;
	   double ep;
	   double beta,eta,tb,tbi;
	   Filter(Mesh *m, const double r, const GridFunction &_u) : 
	       GridFunction(_u), mesh(m), radius(r) 
		{ 
			ep = radius*radius;
		}
		Filter(Mesh *m, const double r, const GridFunction &_u,
			double _beta, double _eta) : GridFunction(_u),
            mesh(m), radius(r), beta(_beta), eta(_eta)  
		{ 
			ep = radius*radius;
			tbi = 1./(tanh(beta*eta) + tanh(beta*(1.-eta)));
			tb = tanh(beta*eta)*tbi;
		}
	   virtual void Filtering(GridFunction &r) = 0;
	   virtual void FilteringWH(GridFunction &r) = 0;
	   virtual void UpdateConstant(double &b, double &e);
	   virtual void FilMult(const Vector &x, Vector &y) = 0; 
	   ~Filter(){ }
	};


// Conic filter
class ConicFilter : public Filter
	{
	public:
	   SparseMatrix * filtermatrix;
	   ConicFilter(Mesh *m, const double r, const GridFunction &_u) : 
	   Filter(m,r,_u) 
		{ 
			filtermatrix = convolution();
		}

		ConicFilter(Mesh *m, const double r, const GridFunction &_u,
			double _beta, double _eta) : 
	    Filter(m,r,_u,_beta,_eta)  
		{ 
			filtermatrix = convolution();
		}

	   SparseMatrix * convolution();
	   void Filtering(GridFunction &r);
	   void FilteringWH(GridFunction &r);
	   void FilMult(const Vector &x, Vector &y);
	   ~ConicFilter(){ delete filtermatrix; }
	};


// Helmholtz filter
class HelmholtzFilter : public Filter
	{
	public:
        FiniteElementCollection *fec1, *fec0;
		FiniteElementSpace *p1espace, *p0espace;
		SparseMatrix A;
		SparseMatrix PI;
		SparseMatrix B;
        Vector U;
		int dimp1;
		#ifndef MFEM_USE_SUITESPARSE
        	GSSmoother *Precon;
        #else
        	UMFPackSolver umf_solver;
        #endif

		HelmholtzFilter(Mesh *m, const double r, const GridFunction &_u, 
			FiniteElementCollection *_f1, FiniteElementCollection *_f0) : 
	    Filter(m,r,_u) , fec1(_f1), fec0(_f0)
		{ 
			Initializer();
		}

		HelmholtzFilter(Mesh *m, const double r, const GridFunction &_u,
			FiniteElementCollection *_f1, FiniteElementCollection *_f0,
			double _beta, double _eta) : Filter(m,r,_u,_beta,_eta),
	    fec1(_f1), fec0(_f0)  
		{
			Initializer();
        }    
        void Initializer(); 
	    void Filtering(GridFunction &r);
	    void FilteringWH(GridFunction &r);
	    void FilMult(const Vector &x, Vector &y);
	    ~HelmholtzFilter()     
        {
            delete p1espace; 
            delete p0espace; 
            #ifndef MFEM_USE_SUITESPARSE
            	delete Precon;
            #endif
        }
	};



// ***********************************************
// Classes for different coefficients
// ***********************************************

// Coefficient for SIMP interpolation	
class SIMPCoeff : public Coefficient
	{
	private:
		GridFunction &r;	
	public: 
		SIMPCoeff(GridFunction &_r) : r(_r)	{ }
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~SIMPCoeff() { }
	};

// Coefficient for compliance derivative
class CoeffDerivative : public Coefficient
	{
	private:
		GridFunction &r;
		GridFunction &U;
		Coefficient &Psip;
		DenseMatrix eps;

	public: 
		CoeffDerivative(GridFunction &_r, GridFunction &_u, 
			Coefficient &_p) : r(_r), U(_u), Psip(_p)
			{ }
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~CoeffDerivative() { }
	};


// Coefficient for mechanism derivative
class CoeffDerivativeAdj : public Coefficient
	{
	private:
		GridFunction &r;
		GridFunction &U;
		GridFunction &Q;
		Coefficient &Psip;
		DenseMatrix eps;
		DenseMatrix qeps;
	public: 
		CoeffDerivativeAdj(GridFunction &_r, GridFunction &_u, GridFunction &_q,
			Coefficient &_p) : r(_r), U(_u), Q(_q), Psip(_p)
			{ }
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~CoeffDerivativeAdj() { }
	};


// Coefficient for volume constraint derivative
class CoeffPsip : public Coefficient
	{
	private:
		GridFunction &r;
		double beta,eta,tb,tbi;
	public:
		CoeffPsip(GridFunction &_r, double _beta, double _eta) : 
		r(_r), beta(_beta), eta(_eta)
		{ 
			tbi = 1./(tanh(beta*eta) + tanh(beta*(1.-eta)));
			tb = tanh(beta*eta)*tbi;
		}
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		void UpdateConstant(double &b, double &e);
		virtual ~CoeffPsip() { }
	};	


// Coefficient for self-weight hypothesis
class DensityCoeff : public Coefficient
	{
	private:
		GridFunction &r;
		double density;
	public:
		DensityCoeff(GridFunction &_r, double _d) : 
		r(_r), density(_d) { }
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~DensityCoeff() { }
	};

// Coefficient of derivative for self-weight-hypothesis
class DerDensityCoeff : public Coefficient
	{
	private:
		GridFunction &U;
		Coefficient &Psip;
		double density;
	public:
		DerDensityCoeff(GridFunction &_U, Coefficient &_p, double _d) : 
		U(_U), Psip(_p), density(_d) { }
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~DerDensityCoeff() { }
	};


// ***********************************************
// Classes for coefficient for computing relaxed Von Mises stress 
// 3 cases: 2D, 2D plain strain, 3D	
// ***********************************************

// generic class
class TensionVonMises : public Coefficient
	{
	protected:
	   GridFunction &u;
	   DenseMatrix eps, sigma;
	   double l,m;
	public:
	   TensionVonMises(GridFunction &_u) : u(_u) 
	       { l = E1*lambda; m = E1*mu; }
	   virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip)=0;
	   virtual ~TensionVonMises() { }
	};


class VonMises2D : public TensionVonMises
	{
	public:
	        VonMises2D(GridFunction &_u) : TensionVonMises(_u) { }
	        virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		~VonMises2D(){ }
	};

class VonMises2Ddp : public TensionVonMises
	{
	public:
	        VonMises2Ddp(GridFunction &_u) : TensionVonMises(_u) { }
	        virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		~VonMises2Ddp(){ }
	};

class VonMises3D : public TensionVonMises
	{
	public:
	        VonMises3D(GridFunction &_u) : TensionVonMises(_u) { }
	        virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);	        
		~VonMises3D(){ }
	};

// ***********************************************
// Coefficient for relaxed stress 
// ***********************************************

class RelaxStress : public Coefficient
	{
	protected:
		GridFunction &r;
		Coefficient &SVM;	
	public:
		RelaxStress(GridFunction &_r, Coefficient &_s): r(_r), SVM(_s) { } 
		virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~RelaxStress() { }

	};



// ***********************************************
// Coefficient for stress derivative
// ***********************************************

// generic class	
class CoeffDerivativeStress : public Coefficient
	{	
	protected:
		GridFunction &r;
		Coefficient &SVM;
		Coefficient &Psip;
        double &cte;
	public:
		CoeffDerivativeStress(GridFunction &_r, Coefficient &_s, 
        Coefficient &_p, double &_cte) : r(_r), SVM(_s), Psip(_p), cte(_cte)
        { }
        virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
		virtual ~CoeffDerivativeStress() { }
	};

// ***********************************************
// Vector Array Coefficient for Linearform of the rhs of 
// adjoint problem for stress derivative
// ***********************************************

// generic class
class MatrixAdjStress : public MatrixArrayCoefficient
    {
    protected:
        GridFunction &r;
        GridFunction &U;
        double &cte;
        double l,m;
        DenseMatrix eps,sigma;
    public: 
        MatrixAdjStress(GridFunction &_r, GridFunction &_u, double &_cte, const int dim) : 
        MatrixArrayCoefficient(dim), r(_r), U(_u), cte(_cte) 
      	{ l = E1*lambda; m = E1*mu; }
        virtual void Eval(DenseMatrix &V, ElementTransformation &T, const IntegrationPoint &ip)=0;
        virtual ~MatrixAdjStress() { }
    };


// Coefficient for sigma(u) (plane stress)
class MatrixAdjStress2D : public MatrixAdjStress
    {
    public: 
        MatrixAdjStress2D(GridFunction &_r, GridFunction &_u, double &_cte) : 
        MatrixAdjStress(_r,_u,_cte, 2) { }
        virtual void Eval(DenseMatrix &V, ElementTransformation &T, const IntegrationPoint &ip);
        virtual ~MatrixAdjStress2D() { }
    };

// Coefficient for sigma(u) (plane strain)
class MatrixAdjStress2Ddp : public MatrixAdjStress
    {
    public: 
        MatrixAdjStress2Ddp(GridFunction &_r, GridFunction &_u, double &_cte) : 
        MatrixAdjStress(_r,_u,_cte, 2) { }
        virtual void Eval(DenseMatrix &V, ElementTransformation &T, const IntegrationPoint &ip);
        virtual ~MatrixAdjStress2Ddp() { }
    };

// Coefficient for sigma(u) (3D)
class MatrixAdjStress3D : public MatrixAdjStress
    {
    public: 
        MatrixAdjStress3D(GridFunction &_r, GridFunction &_u, double &_cte) : 
        MatrixAdjStress(_r,_u,_cte, 3) { }
        virtual void Eval(DenseMatrix &V, ElementTransformation &T, const IntegrationPoint &ip);
        virtual ~MatrixAdjStress3D() { }
    };



// Linear form integrator for variational derivative of stress
class SigmaIntegrator : public LinearFormIntegrator
    {
    private:
        DenseMatrix dshape,Jinv,gshape;
        MatrixArrayCoefficient &Q;
        DenseMatrix qvec;
    public:
        SigmaIntegrator(MatrixArrayCoefficient &QF) : Q(QF) 
        { }
        void AssembleRHSElementVect(const FiniteElement &el, ElementTransformation &Tr, Vector &elvect);
        ~SigmaIntegrator(){ }

    };



// Toptimiz base class
class ToptimizBase
	{
	public:
		Mesh *mesh;
		GridFunction *Rhobar, *y;
		BilinearForm *a;
		LinearForm *vol, *dconres;
		double &Mobj;
		double &Volconstant;
		Array<int> &ess_tdof_list;
		Filter *rho;
		bool &visualization;
		Vector D;
		Vector Y;
		double compliance;
		static socketstream sout;
		int siter;
		static bool boolbeta;
		// Base constructor
		ToptimizBase(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual) : 
		mesh(_m), 
		Rhobar(_rhob),
		y(_y),
		a(_a),
		vol(_v),
		dconres(_d),
		Mobj(_obj),
		Volconstant(_volc),
		ess_tdof_list(_ess), 
		rho(_rho),
		visualization(_visual)
		{ 	
			siter = 0;
			Graphic(); 
		}
		~ToptimizBase() { }

		// initialize graphic
		virtual void Graphic();
	};



// generic class for any problem except Multiload
class Toptimiz : public ToptimizBase
	{
	public:
		GridFunction *Udisp;
		LinearForm *b, *der;
		Toptimiz(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual) : 
		ToptimizBase(_m, _rhob, _y, _a, _v, _d, 
			_obj, _volc, _ess, _rho, _visual),
		Udisp(_u), 
		b(_b),
		der(_der)
		{ }
		~Toptimiz() { }
	};



// generic class for multiload problem
class ToptimizML : public ToptimizBase
	{
	public:
		GridFunction *Udisp[n_f];
		LinearForm *b[n_f], *der[n_f];
		ToptimizML(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u[n_f],  
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b[n_f], 
			LinearForm *_der[n_f], 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual) : 
		ToptimizBase(_m, _rhob, _y, _a, _v, _d, 
			_obj, _volc, _ess, _rho, _visual)
		{ 	
			for (int i=0;i<n_f;i++)
			{
				Udisp[i] = _u[i];
				b[i] = _b[i];
				der[i] = _der[i];
			}
		}
		~ToptimizML() { }
	};





class Thresholding 
	{
	public:
		int ndivisiones;
		GridFunction *Rhobar;
		GridFunction *y;
		LinearForm *vol;
		double &medida;
		double threshold;
		double mn;
		double *xabcisa;
		double *rhordenada;
		Vector copia;
		double &Volfrac;
		Thresholding(int _n, GridFunction *_r, GridFunction *_y, 
			LinearForm *_v, double &_m, double &_f,
			string name1, string name2) :
		ndivisiones(_n), Rhobar(_r), y(_y), vol(_v), medida(_m),
		Volfrac(_f)
		{ 
			copia = Vector(Rhobar->Size());
			copia = *Rhobar;
			threshold = compute();
			mn = mnd();
			cout << "--------------------------------" << endl;
			cout << "-------- Threshold: " << threshold << endl;
			cout << "--------------------------------" << endl;
			cout << "--------    MND:    " << mn*100 << "%" << endl;
			cout << "--------------------------------" << endl;
			dumpfiles(name1,name2);

		}
		Thresholding(GridFunction *_r, GridFunction *_y, 
			LinearForm *_v, double &_m, double &_f,
			string name1, string name2, int _n) :
		Rhobar(_r), y(_y), vol(_v), medida(_m), Volfrac(_f), 
		ndivisiones(_n)
		{ 
			copia = Vector(Rhobar->Size());
			copia = *Rhobar;
			mn = mnd();
			fakecompute();
			cout << "--------------------------------" << endl;
			cout << "--------    MND:    " << mn*100 << "%" << endl;
			cout << "--------------------------------" << endl;
			dumpfiles(name1,name2);

		}

		double compute();
		void fakecompute();
		double volume(double d);
		double volumes(double d);
		double mnd();
		double bisection(double &a, double &b);
		void dumpfiles(string n1, string n2);
		~Thresholding() { }
	};

#endif
