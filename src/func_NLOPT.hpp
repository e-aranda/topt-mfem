
// Class for using NLOPT (MMA)
class ToptimizNLOPT : public Toptimiz
	{	
	public:
		GridFunction *Rhohat;
	    double beta;
		double eta;     
		// Constructor without Heaviside Projection
		ToptimizNLOPT(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual) : 
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual) 
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizNLOPT(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual,
			GridFunction *_rhat,  
			double _beta,
			double _eta) : 
            		Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual), Rhohat(_rhat), beta(_beta), eta(_eta)
			{ }

		~ToptimizNLOPT() { }

		// objective function and constraint for NLOPT
		double myfunc(const vector<double> &x, vector<double> &grad);
		double myconstraint(const vector<double> &x, vector<double> &grad);
		double myfuncVol(const vector<double> &x, vector<double> &grad);
		double myconstraintVol(const vector<double> &x, vector<double> &grad);
	};


// Class for using NLOPT (MMA)
class ToptimizNLOPTML : public ToptimizML
	{	
	public:
		GridFunction *Rhohat;
	    double beta;
		double eta;     
		// Constructor without Heaviside Projection
		ToptimizNLOPTML(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u[n_f],  
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b[n_f], 
			LinearForm *_der[n_f], 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual) : 
			ToptimizML(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual) 
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizNLOPTML(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u[n_f],  
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b[n_f], 
			LinearForm *_der[n_f], 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual,
			GridFunction *_rhat,  
			double _beta,
			double _eta) : 
            ToptimizML(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual), Rhohat(_rhat), beta(_beta), eta(_eta)
			{ }

		~ToptimizNLOPTML() { }

		// objective function and constraint for NLOPT
		double myfunc(const vector<double> &x, vector<double> &grad);
		double myconstraint(const vector<double> &x, vector<double> &grad);
	};







// Class for using NLOPT (MMA) for Mechanism Problem
class ToptimizNLOPTMec : public Toptimiz
	{	
	public:
		GridFunction *Qdisp;
		LinearForm *c;
		GridFunction *Rhohat;
		double beta;
		double eta;
		// Constructor without Heaviside Projection
		ToptimizNLOPTMec(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_c,
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual) : 
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual), Qdisp(_q), c(_c) 
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizNLOPTMec(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_c,
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual,
			GridFunction *_rhat,  
			double _beta,
			double _eta) : 
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual),
			Qdisp(_q), c(_c), Rhohat(_rhat), beta(_beta), eta(_eta)
			{ }

		~ToptimizNLOPTMec() { }

		// objective function and constraint for NLOPT
		double myfunc(const vector<double> &x, vector<double> &grad);
		double myconstraint(const vector<double> &x, vector<double> &grad);
	};


// Class for using NLOPT (MMA) for Stress Problem
class ToptimizNLOPTStr : public Toptimiz
	{	
	public:
		GridFunction *Qdisp;
    	LinearForm *adjrhs;
    	double &maxstress;
	    GridFunction *MyStress;
	    RelaxStress *strcoef;
		GridFunction *Rhohat;
		double beta;
		double eta;
        const IntegrationRule *irs[Geometry::NumGeom];
        double cte,mstr;
        int umbral;
		// Constructor without Heaviside Projection
		ToptimizNLOPTStr(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
        	LinearForm  *_adjrhs,
        	const IntegrationRule *_irs[Geometry::NumGeom],
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			double &_mstres,
			Filter *_rho,
			bool &_visual,
			int &_umb,
			GridFunction *_stress,
			RelaxStress *_coef) : 
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d, 
				_obj, _volc, _ess, _rho, _visual), Qdisp(_q), 
			adjrhs(_adjrhs), maxstress(_mstres), umbral(_umb), MyStress(_stress), 
			strcoef(_coef)
			{
				cte = 1.;
    			for(int i = 0; i < Geometry::NumGeom ;i++)
    				irs[i] = _irs[i];
				D = Vector(y->Size());
		    	rho->FilMult(*dconres,D);
		    	D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizNLOPTStr(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
            LinearForm  *_adjrhs,
            const IntegrationRule *_irs[Geometry::NumGeom],
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			double &_mstres,
			Filter *_rho,
			bool &_visual,
			int &_umb,
			GridFunction *_stress,
			RelaxStress *_coef,
			GridFunction *_rhat,  
			double _beta,
			double _eta) : 
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual), Qdisp(_q), 
			adjrhs(_adjrhs), maxstress(_mstres), umbral(_umb), MyStress(_stress), 
			strcoef(_coef), Rhohat(_rhat), beta(_beta), eta(_eta)
                { 
                	cte = 1.;
    				for(int i = 0; i < Geometry::NumGeom ;i++)
    					irs[i] = _irs[i];
    	        }

    	    void Update()
    	    {
    	    	cte = (siter < umbral) ? mstr/compliance : cte;
    	    }
		~ToptimizNLOPTStr() { }

		// objective function and constraint for NLOPT
		double myfunc(const vector<double> &x, vector<double> &grad);
		double myconstraint(const vector<double> &x, vector<double> &grad);
	};





// wrapper functions for NLOPT
double wrapper(const vector<double> &x, vector<double> &grad, void *other);
double conswrapper(const vector<double> &x, vector<double> &grad, void *other);
