#include "functions.hpp"
#include "func_IPOPT.hpp"

#ifndef TOPTIMIZ_IPOPT
#define TOPTIMIZ_IPOPT

// ********************************************************
// IPOPT functions class for Compliance problem
// ********************************************************

// returns the size of the problem
bool ToptimizIPOPT::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	Index& nnz_h_lag, IndexStyleEnum& index_style)
	{
  		// number of variables
  		n = mesh->GetNE();
  		// number of constraints
  		m = 1;
  		// number of nonzeros in contraint's gradient
		nnz_jac_g = n;
		// no hessian computation
		nnz_h_lag = 0;
  		// use the C style indexing (0-based)
  		index_style = TNLP::C_STYLE;
  		return true;
	}

// returns the variable bounds
bool ToptimizIPOPT::get_bounds_info(Index n, Number* x_l, Number* x_u,
	Index m, Number* g_l, Number* g_u)
	{
  		// the variables bounds

		for (Index i=0; i<n; i++) 
	  	{
    		x_l[i] = lower_bound(i);
    		x_u[i] = 1.;
	  	}
  		// the bounds on the constraint
  		g_l[0] = -2e19;
  		g_u[0] = 1.;
  		return true;
	}

// returns the initial point for the problem
bool ToptimizIPOPT::get_starting_point(Index n, bool init_x, Number* x, bool init_z, 
	Number* z_L, Number* z_U, Index m, bool init_lambda, Number* lambda)
	{
  		// Starting values only for x 
		assert(init_x == true);
		assert(init_z == false);
		assert(init_lambda == false);

		Filter &u = *rho;
  		for (int i = 0; i<n ; i++)
			x[i] = u(i);
		return true;
	}

// returns the value of the objective function
bool ToptimizIPOPT::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
	{
		Filter &u = *rho;
		for (int i = 0; i<n ; i++)
			u(i) = x[i];

		$filtering$

		a->Update();
		a->Assemble();

		$density$

		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

		// Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif
		
		compliance = ((Vector) *b) * U;
		obj_value = Mobj*compliance;
		
		return true;
	}

// return the gradient of the objective function grad_{x} f(x)
bool ToptimizIPOPT::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
	{
		Vector Y(n);
		
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$

			$extra$

			a->Update();
			a->Assemble();
			SparseMatrix A;
			Vector B, U;
			a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

			// Solving system with PCG or UMFPACK (if installed)   
		    #ifndef MFEM_USE_SUITESPARSE
		        GSSmoother M(A);
		        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
		    #else
		        UMFPackSolver umf_solver(true);
		        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
		        umf_solver.SetOperator(A);
		        umf_solver.Mult(B, U);
		    #endif

		}

		

		der->Update();
		der->Assemble();
		rho->FilMult(*der,Y);
		Y *= Mobj;

		for (int i=0; i<n; i++)
    		grad_f[i] = Y(i);
 	
		return true;
	}


// return the value of the constraints: g(x)
bool ToptimizIPOPT::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
	{
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$

		    vol->Update();
			vol->Assemble();
		}
		g[0] = ((Vector) *y) * ((Vector) *vol);
		return true;
	}

// return the structure or values of the jacobian
bool ToptimizIPOPT::eval_jac_g(Index n, const Number* x, bool new_x,
    Index m, Index nele_jac, Index* iRow, Index *jCol,
    Number* values)
	{
		if (values == NULL) 
		{
		    // return the structure of the jacobian of the constraints
		    for (int i=0; i<n; i++){
		    	iRow[i] = 0;
		    	jCol[i] = i;
		    }
		}
		else
		{
			$vectorD$
		}

	  return true;
	}

// No hessian
bool ToptimizIPOPT::eval_h(Index n, const Number* x, bool new_x, Number obj_factor, 
	Index m, const Number* lambda, bool new_lambda, Index nele_hess, Index* iRow,
    Index* jCol, Number* values)
	{  
		return false;
	}

void ToptimizIPOPT::finalize_solution(SolverReturn status,
    Index n, const Number* x, const Number* z_L, const Number* z_U,
    Index m, const Number* g, const Number* lambda,
    Number obj_value, const IpoptData* ip_data,
	IpoptCalculatedQuantities* ip_cq)
	{
	  // here is where we would store the solution to variables, or write to a file, etc
	  // so we could use the solution.
		if (!boolbeta)
		{	
			cout << "FIN OK" << endl;
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

			$filtering$

			if (visualization && sout.good())
			{
    			sout << "solution\n" << *mesh << *Rhobar << endl; 
    			sout << "plot_caption 'Final Result -- Iterations: " << siter <<
    			"  Compliance: " << obj_value/Mobj << "'" << endl;    
			}

			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

		}
	}

bool ToptimizIPOPT::intermediate_callback(AlgorithmMode mode, Index iter, 
	Number obj_value, Number inf_pr, Number inf_du,
	Number mu, Number d_norm, Number regularization_size,
	Number alpha_du, Number alpha_pr, Index ls_trials,
	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
	{

		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ")" << endl;
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Compliance: " << compliance << "'" << endl;          
		}
		siter ++;
		return true;
	}





// ********************************************************
// IPOPT functions class for Multiload problem
// ********************************************************

// returns the size of the problem
bool ToptimizIPOPTML::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	Index& nnz_h_lag, IndexStyleEnum& index_style)
	{
  		// number of variables
  		n = mesh->GetNE();
  		// number of constraints
  		m = 1;
  		// number of nonzeros in contraint's gradient
		nnz_jac_g = n;
		// no hessian computation
		nnz_h_lag = 0;
  		// use the C style indexing (0-based)
  		index_style = TNLP::C_STYLE;
  		return true;
	}

// returns the variable bounds
bool ToptimizIPOPTML::get_bounds_info(Index n, Number* x_l, Number* x_u,
	Index m, Number* g_l, Number* g_u)
	{
  		// the variables bounds

		for (Index i=0; i<n; i++) 
	  	{
    		x_l[i] = lower_bound(i);
    		x_u[i] = 1.;
	  	}
  		// the bounds on the constraint
  		g_l[0] = -2e19;
  		g_u[0] = 1.;
  		return true;
	}

// returns the initial point for the problem
bool ToptimizIPOPTML::get_starting_point(Index n, bool init_x, Number* x, bool init_z, 
	Number* z_L, Number* z_U, Index m, bool init_lambda, Number* lambda)
	{
  		// Starting values only for x 
		assert(init_x == true);
		assert(init_z == false);
		assert(init_lambda == false);

		Filter &u = *rho;
  		for (int i = 0; i<n ; i++)
			x[i] = u(i);
		return true;
	}

// returns the value of the objective function
bool ToptimizIPOPTML::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
	{
		Filter &u = *rho;
		for (int i = 0; i<n ; i++)
			u(i) = x[i];

		$filtering$

		SparseMatrix A[n_f];
	    Vector B[n_f], U[n_f];
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M;
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	    #endif
	    compliance = 0.;
	    double compli;

	    a->Update();
	    a->Assemble();
	    for (int i=0; i<n_f; i++)
	    {
	        a->FormLinearSystem(ess_tdof_list, *Udisp[i], *b[i], A[i], U[i], B[i]);

	        // Solving system with PCG or UMFPACK (if installed)   
	        #ifndef MFEM_USE_SUITESPARSE
	            M = GSSmoother(A[i]);
	            PCG(A[i], M, B[i], U[i], 0, ITER, TOL, 0.0);
	        #else
	            umf_solver.SetOperator(A[i]);
	            umf_solver.Mult(B[i], U[i]);
	        #endif
	        
	        a->RecoverFEMSolution(U[i], *b[i], *Udisp[i]);
	        compli = ((Vector) *b[i]) * U[i];
	        compliance += lambdacoef[i]*compli;
	    }
		
		obj_value = Mobj*compliance;
		
		return true;
	}

// return the gradient of the objective function grad_{x} f(x)
bool ToptimizIPOPTML::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
	{
		Vector Y[n_f];
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$

			$extra$
	
			SparseMatrix A[n_f];
		    Vector B[n_f], U[n_f];
		    #ifndef MFEM_USE_SUITESPARSE
		        GSSmoother M;
		    #else
		        UMFPackSolver umf_solver(true);
		        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
		    #endif

			a->Update();
		    a->Assemble();

		    for (int i=0; i<n_f; i++)
		    {
		        a->FormLinearSystem(ess_tdof_list, *Udisp[i], *b[i], A[i], U[i], B[i]);

		        // Solving system with PCG or UMFPACK (if installed)   
		        #ifndef MFEM_USE_SUITESPARSE
		            M = GSSmoother(A[i]);
		            PCG(A[i], M, B[i], U[i], 0, ITER, TOL, 0.0);
		        #else
		            umf_solver.SetOperator(A[i]);
		            umf_solver.Mult(B[i], U[i]);
		        #endif
		        
		        a->RecoverFEMSolution(U[i], *b[i], *Udisp[i]);
		    }

		}
	

		for (int i=0;i<n_f;i++)
		{
			Y[i].SetSize(n);
			der[i]->Update();
			der[i]->Assemble();
			rho->FilMult(*der[i],Y[i]);
		}
            
		for (int i=0; i<n; i++)
		{
			grad_f[i] = 0.;
				for (int k=0;k<n_f;k++)
    				grad_f[i] += Mobj*lambdacoef[k]*Y[k](i);
			}
 	
		return true;
	}


// return the value of the constraints: g(x)
bool ToptimizIPOPTML::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
	{
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$

		    vol->Update();
			vol->Assemble();
		}
		g[0] = ((Vector) *y) * ((Vector) *vol);
		return true;
	}

// return the structure or values of the jacobian
bool ToptimizIPOPTML::eval_jac_g(Index n, const Number* x, bool new_x,
    Index m, Index nele_jac, Index* iRow, Index *jCol,
    Number* values)
	{
		if (values == NULL) 
		{
		    // return the structure of the jacobian of the constraints
		    for (int i=0; i<n; i++){
		    	iRow[i] = 0;
		    	jCol[i] = i;
		    }
		}
		else
		{
			$vectorD$
		}

	  return true;
	}

// No hessian
bool ToptimizIPOPTML::eval_h(Index n, const Number* x, bool new_x, Number obj_factor, 
	Index m, const Number* lambda, bool new_lambda, Index nele_hess, Index* iRow,
    Index* jCol, Number* values)
	{  
		return false;
	}

void ToptimizIPOPTML::finalize_solution(SolverReturn status,
    Index n, const Number* x, const Number* z_L, const Number* z_U,
    Index m, const Number* g, const Number* lambda,
    Number obj_value, const IpoptData* ip_data,
	IpoptCalculatedQuantities* ip_cq)
	{
	  // here is where we would store the solution to variables, or write to a file, etc
	  // so we could use the solution.
		if (!boolbeta)
		{	
			cout << "FIN OK" << endl;
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

			$filtering$

			if (visualization && sout.good())
			{
    			sout << "solution\n" << *mesh << *Rhobar << endl; 
    			sout << "plot_caption 'Final Result -- Iterations: " << siter <<
    			"  Compliance: " << obj_value/Mobj << "'" << endl;    
			}

			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

		}
	}

bool ToptimizIPOPTML::intermediate_callback(AlgorithmMode mode, Index iter, 
	Number obj_value, Number inf_pr, Number inf_du,
	Number mu, Number d_norm, Number regularization_size,
	Number alpha_du, Number alpha_pr, Index ls_trials,
	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
	{

		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ")" << endl;
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Compliance: " << compliance << "'" << endl;          
		}
		siter ++;
		return true;
	}













// ********************************************************
// IPOPT functions class for Volume problem
// ********************************************************



// returns the size of the problem
bool ToptimizIPOPTVol::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	Index& nnz_h_lag, IndexStyleEnum& index_style)
	{
  		// number of variables
  		n = mesh->GetNE();
  		// number of constraints
  		m = 1;
  		// number of nonzeros in contraint's gradient
		nnz_jac_g = n;
		// no hessian computation
		nnz_h_lag = 0;
  		// use the C style indexing (0-based)
  		index_style = TNLP::C_STYLE;
  		return true;
	}

// returns the variable bounds
bool ToptimizIPOPTVol::get_bounds_info(Index n, Number* x_l, Number* x_u,
	Index m, Number* g_l, Number* g_u)
	{
  		// the variables bounds

		for (Index i=0; i<n; i++) 
	  	{
    		x_l[i] = lower_bound(i);
    		x_u[i] = 1.;
	  	}
  		// the bounds on the constraint
  		g_l[0] = -2e19;
  		g_u[0] = 1.;
  		return true;
	}

// returns the initial point for the problem
bool ToptimizIPOPTVol::get_starting_point(Index n, bool init_x, Number* x, bool init_z, 
	Number* z_L, Number* z_U, Index m, bool init_lambda, Number* lambda)
	{
  		// Starting values only for x 
		assert(init_x == true);
		assert(init_z == false);
		assert(init_lambda == false);

		Filter &u = *rho;
  		for (int i = 0; i<n ; i++)
			x[i] = u(i);
		return true;
	}


// returns the value of the objective function
bool ToptimizIPOPTVol::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
	{
		Filter &u = *rho;
		for (int i = 0; i<n ; i++)
			u(i) = x[i];

		$filtering$

	    vol->Update();
		vol->Assemble();

		obj_value = ((Vector) *y) * ((Vector) *vol);
		
		return true;
	}

// return the gradient of the objective function grad_{x} f(x)
bool ToptimizIPOPTVol::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
	{

		$vectorVolD$ 

		return true;
	}


// return the value of the constraints: g(x)
bool ToptimizIPOPTVol::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
	{
		Filter &u = *rho;
		for (int i = 0; i<n ; i++)
			u(i) = x[i];

		$filtering$

		a->Update();
		a->Assemble();
		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
			
		// Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif
		
		compliance = ((Vector) *b) * U;
		double cons = Mobj*compliance;

		g[0] = cons;
		return true;
	}

// return the structure or values of the jacobian
bool ToptimizIPOPTVol::eval_jac_g(Index n, const Number* x, bool new_x,
    Index m, Index nele_jac, Index* iRow, Index *jCol,
    Number* values)
	{
		if (values == NULL) 
		{
		    // return the structure of the jacobian of the constraints
			// for (int i=0; i<y->Size(); i++)
		    for (int i=0; i<n; i++){
		    	iRow[i] = 0;
		    	jCol[i] = i;
		    }
		}
		else
		{
			Vector Y(n);
			
			if (new_x)
			{
				Filter &u = *rho;
				for (int i = 0; i<n ; i++)
					u(i) = x[i];

				$filtering$
				$extra$

				a->Update();
				a->Assemble();
				SparseMatrix A;
				Vector B, U;
				a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
							
				// Solving system with PCG or UMFPACK (if installed)   
			    #ifndef MFEM_USE_SUITESPARSE
			        GSSmoother M(A);
			        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
			    #else
			        UMFPackSolver umf_solver(true);
			        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
			        umf_solver.SetOperator(A);
			        umf_solver.Mult(B, U);
			    #endif

			}

			der->Update();
			der->Assemble();
			rho->FilMult(*der,Y);
			Y *= Mobj;

			for (int i=0; i<n; i++)
				values[i] = Y(i);
		}

	  return true;
	}



bool ToptimizIPOPTVol::intermediate_callback(AlgorithmMode mode, Index iter, 
	Number obj_value, Number inf_pr, Number inf_du,
	Number mu, Number d_norm, Number regularization_size,
	Number alpha_du, Number alpha_pr, Index ls_trials,
	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
	{

		cout << "Iteration:  " << siter << "  Obj: " << obj_value << endl;
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl;  
			sout << "plot_caption 'Iteration: " << siter <<
			"  Volume: " << obj_value << "'" << endl;          
		}
		siter ++;
		return true;
	}

// No hessian
bool ToptimizIPOPTVol::eval_h(Index n, const Number* x, bool new_x, Number obj_factor, 
	Index m, const Number* lambda, bool new_lambda, Index nele_hess, Index* iRow,
    Index* jCol, Number* values)
	{  
		return false;
	}

void ToptimizIPOPTVol::finalize_solution(SolverReturn status,
    Index n, const Number* x, const Number* z_L, const Number* z_U,
    Index m, const Number* g, const Number* lambda,
    Number obj_value, const IpoptData* ip_data,
	IpoptCalculatedQuantities* ip_cq)
	{
	  // here is where we would store the solution to variables, or write to a file, etc
	  // so we could use the solution.
		if (!boolbeta)
		{	
			cout << "FIN OK" << endl;
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

			$filtering$

			if (visualization && sout.good())
			{
    			sout << "solution\n" << *mesh << *Rhobar << endl; 
    			sout << "plot_caption 'Final Result -- Iterations: " << siter <<
    			"  Volume: " << obj_value << "'" << endl;    
			}

			for (int i = 0; i<n ; i++)
			    u(i) = x[i];
		}
	}




// ********************************************************
// IPOPT functions class for Mechanism problem
// ********************************************************


// returns the size of the problem
bool ToptimizIPOPTMec::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	Index& nnz_h_lag, IndexStyleEnum& index_style)
	{
  		// number of variables
  		n = mesh->GetNE();
  		// number of constraints
  		m = 1;
  		// number of nonzeros in contraint's gradient
		nnz_jac_g = n;
		// no hessian computation
		nnz_h_lag = 0;
  		// use the C style indexing (0-based)
  		index_style = TNLP::C_STYLE;
  		return true;
	}

// returns the variable bounds
bool ToptimizIPOPTMec::get_bounds_info(Index n, Number* x_l, Number* x_u,
	Index m, Number* g_l, Number* g_u)
	{
  		// the variables bounds

		for (Index i=0; i<n; i++) 
	  	{
    		x_l[i] = lower_bound(i);
    		x_u[i] = 1.;
	  	}
  		// the bounds on the constraint
  		g_l[0] = -2e19;
  		g_u[0] = 1.;
  		return true;
	}

// returns the initial point for the problem
bool ToptimizIPOPTMec::get_starting_point(Index n, bool init_x, Number* x, bool init_z, 
	Number* z_L, Number* z_U, Index m, bool init_lambda, Number* lambda)
	{
  		// Starting values only for x 
		assert(init_x == true);
		assert(init_z == false);
		assert(init_lambda == false);

		Filter &u = *rho;
  		for (int i = 0; i<n ; i++)
			x[i] = u(i);
		return true;
	}




// returns the value of the objective function
bool ToptimizIPOPTMec::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
	{
		Filter &u = *rho;
		for (int i = 0; i<n ; i++)
			u(i) = x[i];

		$filtering$

		a->Update();
		a->Assemble();
		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
	
		// Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif
		
		compliance = ((Vector) *c) * U;
		obj_value = Mobj*compliance;
		
		return true;
	}

// return the gradient of the objective function grad_{x} f(x)
bool ToptimizIPOPTMec::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
	{
		Vector Y(n);
		
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$
			$extra$

			a->Update();
			a->Assemble();
		}
		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Qdisp, *c, A, U, B);
		
		// Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif
		
		der->Update();
		der->Assemble();
		rho->FilMult(*der,Y);
		Y *= Mobj;

		for (int i=0; i<n; i++)
    		grad_f[i] = Y(i);
 	
		return true;
	}


// return the value of the constraints: g(x)
bool ToptimizIPOPTMec::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
	{
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$

		    vol->Update();
			vol->Assemble();
		}
		g[0] = ((Vector) *y) * ((Vector) *vol);
		return true;
	}

// return the structure or values of the jacobian
bool ToptimizIPOPTMec::eval_jac_g(Index n, const Number* x, bool new_x,
    Index m, Index nele_jac, Index* iRow, Index *jCol,
    Number* values)
	{
		if (values == NULL) 
		{
		    // return the structure of the jacobian of the constraints
			// for (int i=0; i<y->Size(); i++)
		    for (int i=0; i<n; i++){
		    	iRow[i] = 0;
		    	jCol[i] = i;
		    }
		}
		else
		{
			$vectorD$
		}

	  return true;
	}



bool ToptimizIPOPTMec::intermediate_callback(AlgorithmMode mode, Index iter, 
	Number obj_value, Number inf_pr, Number inf_du,
	Number mu, Number d_norm, Number regularization_size,
	Number alpha_du, Number alpha_pr, Index ls_trials,
	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
	{

		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ")"<< endl;
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			// sout << "valuerange 0.0 1.0\n";  
			sout << "plot_caption 'Iteration: " << siter <<
			"  Objective: " << compliance << "'" << endl;          
		}
		siter ++;
		return true;
	}




// No hessian
bool ToptimizIPOPTMec::eval_h(Index n, const Number* x, bool new_x, Number obj_factor, 
	Index m, const Number* lambda, bool new_lambda, Index nele_hess, Index* iRow,
    Index* jCol, Number* values)
	{  
		return false;
	}

void ToptimizIPOPTMec::finalize_solution(SolverReturn status,
    Index n, const Number* x, const Number* z_L, const Number* z_U,
    Index m, const Number* g, const Number* lambda,
    Number obj_value, const IpoptData* ip_data,
	IpoptCalculatedQuantities* ip_cq)
	{
	  // here is where we would store the solution to variables, or write to a file, etc
	  // so we could use the solution.
		if (!boolbeta)
		{	
			cout << "FIN OK" << endl;
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

			$filtering$

			if (visualization && sout.good())
			{
    			sout << "solution\n" << *mesh << *Rhobar << endl; 
    			sout << "plot_caption 'Final Result -- Iterations: " << siter <<
    			"  Compliance: " << obj_value/Mobj << "'" << endl;    
			}

			for (int i = 0; i<n ; i++)
			    u(i) = x[i];
			
		}
	}





// ********************************************************
// IPOPT functions class for Stress problem
// ********************************************************

// returns the size of the problem
bool ToptimizIPOPTStr::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	Index& nnz_h_lag, IndexStyleEnum& index_style)
	{
  		// number of variables
  		n = mesh->GetNE();
  		// number of constraints
  		m = 1;
  		// number of nonzeros in contraint's gradient
		nnz_jac_g = n;
		// no hessian computation
		nnz_h_lag = 0;
  		// use the C style indexing (0-based)
  		index_style = TNLP::C_STYLE;
  		return true;
	}

// returns the variable bounds
bool ToptimizIPOPTStr::get_bounds_info(Index n, Number* x_l, Number* x_u,
	Index m, Number* g_l, Number* g_u)
	{
  		// the variables bounds

		for (Index i=0; i<n; i++) 
	  	{
    		x_l[i] = lower_bound(i);
    		x_u[i] = 1.;
	  	}
  		// the bounds on the constraint
  		g_l[0] = -2e19;
  		g_u[0] = 1.;
  		return true;
	}

// returns the initial point for the problem
bool ToptimizIPOPTStr::get_starting_point(Index n, bool init_x, Number* x, bool init_z, 
	Number* z_L, Number* z_U, Index m, bool init_lambda, Number* lambda)
	{
  		// Starting values only for x 
		assert(init_x == true);
		assert(init_z == false);
		assert(init_lambda == false);

		Filter &u = *rho;
  		for (int i = 0; i<n ; i++)
			x[i] = u(i);
		return true;
	}




// returns the value of the objective function
bool ToptimizIPOPTStr::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
	{
		Filter &u = *rho;
		for (int i = 0; i<n ; i++)
			u(i) = x[i];

		$filtering$

		vol->Update();
		vol->Assemble();

		obj_value = ((Vector) *y) * ((Vector) *vol);
        
		return true;
	}

// return the gradient of the objective function grad_{x} f(x)
bool ToptimizIPOPTStr::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
	{
		$vectorVolD$ 
 	
		return true;
	}


// return the value of the constraints: g(x)
bool ToptimizIPOPTStr::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
	{
		if (new_x)
		{
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
				u(i) = x[i];

			$filtering$

			a->Update();
			a->Assemble();
			
			SparseMatrix A;
			Vector B, U;
			a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

		    // Solving system with PCG or UMFPACK (if installed)   
		    #ifndef MFEM_USE_SUITESPARSE
		        GSSmoother M(A);
		        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
		    #else
		        UMFPackSolver umf_solver(true);
		        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
		        umf_solver.SetOperator(A);
		        umf_solver.Mult(B, U);
		    #endif

			a->RecoverFEMSolution(U, *b, *Udisp);
	        
	        compliance = ComputeLpNorm(PNORM, *strcoef, *mesh, irs);
	        MyStress->ProjectCoefficient(*strcoef);
	        mstr = MyStress->Max();
			
	        Mobj = cte * pow(compliance,1-PNORM);
		}
		g[0] = cte*compliance/maxstress - 1.;
        

		return true;
	}

// return the structure or values of the jacobian
bool ToptimizIPOPTStr::eval_jac_g(Index n, const Number* x, bool new_x,
    Index m, Index nele_jac, Index* iRow, Index *jCol,
    Number* values)
	{
		if (values == NULL) 
		{
		    // return the structure of the jacobian of the constraints
			// for (int i=0; i<y->Size(); i++)
		    for (int i=0; i<n; i++){
		    	iRow[i] = 0;
		    	jCol[i] = i;
		    }
		}
		else
		{
			Vector Y(n);
			if (new_x)
			{
				Filter &u = *rho;
				for (int i = 0; i<n ; i++)
					u(i) = x[i];

				$filtering$
				$extra$

	            adjrhs->Update();
	            adjrhs->Assemble();
	            
				// adjoint problem
				SparseMatrix A;
				Vector BAdj, QAdj;
				a->FormLinearSystem(ess_tdof_list, *Qdisp, *adjrhs, A, QAdj, BAdj);

			    // Solving system with PCG or UMFPACK (if installed)   
			    #ifndef MFEM_USE_SUITESPARSE
			    	GSSmoother M(A);
			        PCG(A, M, BAdj, QAdj, 0, ITER, TOL, 0.0);
			    #else
			        UMFPackSolver umf_solver(true);
			        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
			        umf_solver.SetOperator(A);
			        umf_solver.Mult(BAdj, QAdj);
			    #endif

	            a->RecoverFEMSolution(QAdj, *adjrhs, *Qdisp);
			}
			der->Update();
			der->Assemble();
			rho->FilMult(*der,Y);
			Y /= maxstress;

			for (int i=0; i<n; i++)
				values[i] = Y(i);
		}
	  	
	  	return true;
	}



bool ToptimizIPOPTStr::intermediate_callback(AlgorithmMode mode, Index iter, 
	Number obj_value, Number inf_pr, Number inf_du,
	Number mu, Number d_norm, Number regularization_size,
	Number alpha_du, Number alpha_pr, Index ls_trials,
	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq)
	{

		cout << "Iteration:  " << siter << "  : " << obj_value <<
		" (P-nomr: " << cte*compliance << ", Max-norm: " << mstr << ")" << endl;
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Volume: " << obj_value << "'" << endl;          
		}
		siter ++;
		Update();
		return true;
	}




// No hessian
bool ToptimizIPOPTStr::eval_h(Index n, const Number* x, bool new_x, Number obj_factor, 
	Index m, const Number* lambda, bool new_lambda, Index nele_hess, Index* iRow,
    Index* jCol, Number* values)
	{  
		return false;
	}

void ToptimizIPOPTStr::finalize_solution(SolverReturn status,
    Index n, const Number* x, const Number* z_L, const Number* z_U,
    Index m, const Number* g, const Number* lambda,
    Number obj_value, const IpoptData* ip_data,
	IpoptCalculatedQuantities* ip_cq)
	{
	  // here is where we would store the solution to variables, or write to a file, etc
	  // so we could use the solution.
		if (!boolbeta)
		{	
			cout << "FIN OK" << endl;
			Filter &u = *rho;
			for (int i = 0; i<n ; i++)
			    u(i) = x[i];

			$filtering$

			if (visualization && sout.good())
			{
    			sout << "solution\n" << *mesh << *Rhobar << endl; 
    			sout << "plot_caption 'Final Result -- Iterations: " << siter <<
    			"  Volume: " << obj_value << " P-norm: " << compliance << "'" << endl;    
			}

			for (int i = 0; i<n ; i++)
			    u(i) = x[i];
			
		}
	}

#endif
