
// Class for using Optimality Conditions Compliance case
class ToptimizOC : public Toptimiz
	{
	public:
		int n;
		double zeta;
		double neta;
		GridFunction *Rhohat;
		double beta;
		double eta;
		// Constructor without Heaviside Projection
		ToptimizOC(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual,  
			double _zeta,
			double _neta) :
			zeta(_zeta), neta(_neta),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{ 
				n = y->Size();
				D = Vector(n);
				rho->FilMult(*dconres,D);
			    Y = Vector(n); 
			}

		// Constructor with Heaviside Projections
		ToptimizOC(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u, 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b, 
			LinearForm *_der, 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual, 
			double _zeta,
			double _neta,
			GridFunction *_rhat, 
			double _beta,
			double _eta) : 
			beta(_beta), eta(_eta), zeta(_zeta), neta(_neta),
			Rhohat(_rhat), 
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{
				n = y->Size();
				D = Vector(n);
			    Y = Vector(n); 
			}

			~ToptimizOC(){ }
			
		// evaluation of compliance, volume y derivative for OC
		void evaluation();
		void OCiteration(int &m, double &tol);
	};	


// Class for using Optimality Conditions for Multiload case
class ToptimizOCML : public ToptimizML
	{
	public:
		int n;
		double zeta;
		double neta;
		GridFunction *Rhohat;
		double beta;
		double eta;
		// Constructor without Heaviside Projection
		ToptimizOCML(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u[n_f], 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b[n_f], 
			LinearForm *_der[n_f], 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual,  
			double _zeta,
			double _neta) :
			zeta(_zeta), neta(_neta),
			ToptimizML(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{ 
				n = y->Size();
				D = Vector(n);
				rho->FilMult(*dconres,D);
			    Y = Vector(n); 
			}

		// Constructor with Heaviside Projections
		ToptimizOCML(
			Mesh *_m, 
			GridFunction *_rhob,
			GridFunction *_u[n_f], 
			GridFunction *_y,
			BilinearForm *_a, 
			LinearForm *_b[n_f], 
			LinearForm *_der[n_f], 
			LinearForm *_v,
			LinearForm *_d,
			double &_obj, 
			double &_volc,
			Array<int> &_ess, 
			Filter *_rho,
			bool &_visual, 
			double _zeta,
			double _neta,
			GridFunction *_rhat, 
			double _beta,
			double _eta) : 
			beta(_beta), eta(_eta), zeta(_zeta), neta(_neta),
			Rhohat(_rhat), 
			ToptimizML(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{
				n = y->Size();
				D = Vector(n);
			    Y = Vector(n);
			}

			~ToptimizOCML(){ }
			
		// evaluation of compliance, volume y derivative for OC
		void evaluation();
		void OCiteration(int &m, double &tol);
	};	


