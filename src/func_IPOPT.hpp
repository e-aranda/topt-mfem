#define HAVE_CSTDDEF
#include "coin/IpTNLP.hpp"
#undef HAVE_CSTDDEF

using Ipopt::Index;
using Ipopt::Number;
using Ipopt::IpoptData;
using Ipopt::IpoptCalculatedQuantities;
using Ipopt::SolverReturn;
using Ipopt::AlgorithmMode;
using Ipopt::Solve_Succeeded;


// class for using IPOPT for Compliance problem
class ToptimizIPOPT : public Ipopt::TNLP, Toptimiz
	{
	public:
		double beta;
		double eta;
		GridFunction *Rhohat;
		Vector &lower_bound;
		// Constructor without Heaviside Projection
		ToptimizIPOPT(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual) :
			lower_bound(_lb),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizIPOPT(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual,
			GridFunction *_rhat,
			double _beta,
			double _eta) :
			lower_bound(_lb), Rhohat(_rhat), beta(_beta), eta(_eta),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual){ }

		~ToptimizIPOPT() { }

		 // Method to return some info about the nlp
	 	virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	        Index& nnz_h_lag, IndexStyleEnum& index_style);

	  	// Method to return the bounds for my problem
	  	virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
	        Index m, Number* g_l, Number* g_u);

	  	// Method to return the starting point for the algorithm
	  	virtual bool get_starting_point(Index n, bool init_x, Number* x,
	        bool init_z, Number* z_L, Number* z_U, Index m, bool init_lambda,
	        Number* lambda);

	  	// Method to return the objective value
	  	virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);

	  	// Method to return the gradient of the objective
	  	virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);

	  	// Method to return the constraint residuals
	  	virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);

	  	// Method to return:
	   	//  1) The structure of the jacobian (if "values" is NULL)
	   	//  2) The values of the jacobian (if "values" is not NULL)
	  	virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
	        Index m, Index nele_jac, Index* iRow, Index *jCol, Number* values);

	  	// Method to return:
	   	//  1) The structure of the hessian of the lagrangian (if "values" is NULL)
	   	//  2) The values of the hessian of the lagrangian (if "values" is not NULL)
	  	virtual bool eval_h(Index n, const Number* x, bool new_x,
	    	Number obj_factor, Index m, const Number* lambda, bool new_lambda,
	    	Index nele_hess, Index* iRow, Index* jCol, Number* values);

	  	// This method is called when the algorithm is complete so the TNLP can store/write the solution
	  	virtual void finalize_solution(SolverReturn status,
	 		Index n, const Number* x, const Number* z_L, const Number* z_U, Index m,
	 		const Number* g, const Number* lambda, Number obj_value,
	 		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

	 	virtual bool intermediate_callback(AlgorithmMode mode,
	    	Index iter, Number obj_value, Number inf_pr, Number inf_du,
	    	Number mu, Number d_norm, Number regularization_size,
	      	Number alpha_du, Number alpha_pr, Index ls_trials,
	      	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 };



// class for using IPOPT for Compliance problem
class ToptimizIPOPTML : public Ipopt::TNLP, ToptimizML
	{
	public:
		double beta;
		double eta;
		GridFunction *Rhohat;
		Vector &lower_bound;
		// Constructor without Heaviside Projection
		ToptimizIPOPTML(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u[n_f],
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b[n_f],
			LinearForm *_der[n_f],
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual) :
			lower_bound(_lb),
			ToptimizML(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizIPOPTML(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u[n_f],
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b[n_f],
			LinearForm *_der[n_f],
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual,
			GridFunction *_rhat,
			double _beta,
			double _eta) :
			lower_bound(_lb), Rhohat(_rhat), beta(_beta), eta(_eta),
			ToptimizML(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual){ }

		~ToptimizIPOPTML() { }

		 // Method to return some info about the nlp
	 	virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	        Index& nnz_h_lag, IndexStyleEnum& index_style);

	  	// Method to return the bounds for my problem
	  	virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
	        Index m, Number* g_l, Number* g_u);

	  	// Method to return the starting point for the algorithm
	  	virtual bool get_starting_point(Index n, bool init_x, Number* x,
	        bool init_z, Number* z_L, Number* z_U, Index m, bool init_lambda,
	        Number* lambda);

	  	// Method to return the objective value
	  	virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);

	  	// Method to return the gradient of the objective
	  	virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);

	  	// Method to return the constraint residuals
	  	virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);

	  	// Method to return:
	   	//  1) The structure of the jacobian (if "values" is NULL)
	   	//  2) The values of the jacobian (if "values" is not NULL)
	  	virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
	        Index m, Index nele_jac, Index* iRow, Index *jCol, Number* values);

	  	// Method to return:
	   	//  1) The structure of the hessian of the lagrangian (if "values" is NULL)
	   	//  2) The values of the hessian of the lagrangian (if "values" is not NULL)
	  	virtual bool eval_h(Index n, const Number* x, bool new_x,
	    	Number obj_factor, Index m, const Number* lambda, bool new_lambda,
	    	Index nele_hess, Index* iRow, Index* jCol, Number* values);

	  	// This method is called when the algorithm is complete so the TNLP can store/write the solution
	  	virtual void finalize_solution(SolverReturn status,
	 		Index n, const Number* x, const Number* z_L, const Number* z_U, Index m,
	 		const Number* g, const Number* lambda, Number obj_value,
	 		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);

	 	virtual bool intermediate_callback(AlgorithmMode mode,
	    	Index iter, Number obj_value, Number inf_pr, Number inf_du,
	    	Number mu, Number d_norm, Number regularization_size,
	      	Number alpha_du, Number alpha_pr, Index ls_trials,
	      	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 };







// class for using IPOPT for Volume problem
class ToptimizIPOPTVol : public Ipopt::TNLP, Toptimiz
	{
	public:
		double beta;
		double eta;
		GridFunction *Rhohat;
		Vector &lower_bound;
		// Constructor without Heaviside Projection
		ToptimizIPOPTVol(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual) :
			lower_bound(_lb),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizIPOPTVol(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual,
			GridFunction *_rhat,
			double _beta,
			double _eta) :
			lower_bound(_lb), Rhohat(_rhat), beta(_beta), eta(_eta),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual){ }

		~ToptimizIPOPTVol() { }

		// same functions as ToptimizIPOPT
	 	virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	        Index& nnz_h_lag, IndexStyleEnum& index_style);
	  	virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
	        Index m, Number* g_l, Number* g_u);
	  	virtual bool get_starting_point(Index n, bool init_x, Number* x,
	        bool init_z, Number* z_L, Number* z_U, Index m, bool init_lambda,
	        Number* lambda);
	  	virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);
	  	virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);
	  	virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);
	  	virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
	        Index m, Index nele_jac, Index* iRow, Index *jCol, Number* values);
	  	virtual bool eval_h(Index n, const Number* x, bool new_x,
	    	Number obj_factor, Index m, const Number* lambda, bool new_lambda,
	    	Index nele_hess, Index* iRow, Index* jCol, Number* values);
	  	virtual void finalize_solution(SolverReturn status,
	 		Index n, const Number* x, const Number* z_L, const Number* z_U, Index m,
	 		const Number* g, const Number* lambda, Number obj_value,
	 		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 	virtual bool intermediate_callback(AlgorithmMode mode,
	    	Index iter, Number obj_value, Number inf_pr, Number inf_du,
	    	Number mu, Number d_norm, Number regularization_size,
	      	Number alpha_du, Number alpha_pr, Index ls_trials,
	      	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 };


// class for using IPOPT for Mechanism problem
class ToptimizIPOPTMec : public Ipopt::TNLP, Toptimiz
	{
	public:
		double beta;
		double eta;
		GridFunction *Rhohat;
		Vector &lower_bound;
		GridFunction *Qdisp;
		LinearForm *c;
		// Constructor without Heaviside Projection
		ToptimizIPOPTMec(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_c,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual) :
			lower_bound(_lb), Qdisp(_q), c(_c),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual)
			{
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizIPOPTMec(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_c,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			Filter *_rho,
			Vector &_lb,
			bool &_visual,
			GridFunction *_rhat,
			double _beta,
			double _eta) :
			lower_bound(_lb), Qdisp(_q), c(_c), Rhohat(_rhat), beta(_beta), eta(_eta),
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual){ }

		~ToptimizIPOPTMec() { }

		// same functions as in ToptimizIPOPT
	 	virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	        Index& nnz_h_lag, IndexStyleEnum& index_style);
	  	virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
	        Index m, Number* g_l, Number* g_u);
	  	virtual bool get_starting_point(Index n, bool init_x, Number* x,
	        bool init_z, Number* z_L, Number* z_U, Index m, bool init_lambda,
	        Number* lambda);
	  	virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);
	  	virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);
	  	virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);
	  	virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
	        Index m, Index nele_jac, Index* iRow, Index *jCol, Number* values);
	  	virtual bool eval_h(Index n, const Number* x, bool new_x,
	    	Number obj_factor, Index m, const Number* lambda, bool new_lambda,
	    	Index nele_hess, Index* iRow, Index* jCol, Number* values);
	  	virtual void finalize_solution(SolverReturn status,
	 		Index n, const Number* x, const Number* z_L, const Number* z_U, Index m,
	 		const Number* g, const Number* lambda, Number obj_value,
	 		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 	virtual bool intermediate_callback(AlgorithmMode mode,
	    	Index iter, Number obj_value, Number inf_pr, Number inf_du,
	    	Number mu, Number d_norm, Number regularization_size,
	      	Number alpha_du, Number alpha_pr, Index ls_trials,
	      	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 };



// class for using IPOPT for Stress problem
class ToptimizIPOPTStr : public Ipopt::TNLP, Toptimiz
	{
	public:
		GridFunction *Qdisp;
		LinearForm *adjrhs;
		double &maxstress;
		GridFunction *MyStress;
		RelaxStress *strcoef;
		GridFunction *Rhohat;
		double beta;
		double eta;
		const IntegrationRule *irs[Geometry::NumGeom];
        double cte,mstr;
        int umbral;
		Vector &lower_bound;
		// Constructor without Heaviside Projection
		ToptimizIPOPTStr(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			LinearForm *_adjrhs,
			const IntegrationRule *_irs[Geometry::NumGeom],
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			double &_mstres,
			Filter *_rho,
			Vector &_lb,
			bool &_visual,
			int &_umb,
			GridFunction *_stress,
			RelaxStress *_coef) :
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual),
			Qdisp(_q), adjrhs(_adjrhs), maxstress(_mstres), lower_bound(_lb), 
			umbral(_umb), MyStress(_stress), strcoef(_coef)
			{
				cte = 1.;
				for(int i = 0; i < Geometry::NumGeom ;i++)
    				irs[i] = _irs[i];
				D = Vector(y->Size());
	    		rho->FilMult(*dconres,D);
    			D *= Volconstant;
			}
		// Constructor with Heaviside Projection
		ToptimizIPOPTStr(
			Mesh *_m,
			GridFunction *_rhob,
			GridFunction *_u,
			GridFunction *_q,
			GridFunction *_y,
			BilinearForm *_a,
			LinearForm *_b,
			LinearForm *_der,
			LinearForm *_v,
			LinearForm *_d,
			LinearForm *_adjrhs,
			const IntegrationRule *_irs[Geometry::NumGeom],
			double &_obj,
			double &_volc,
			Array<int> &_ess,
			double &_mstres,
			Filter *_rho,
			Vector &_lb,
			bool &_visual,
			int &_umb,
			GridFunction *_stress,
			RelaxStress *_coef,
			GridFunction *_rhat,
			double _beta,
			double _eta) :
			Toptimiz(_m, _rhob, _u, _y, _a, _b, _der, _v, _d,
				_obj, _volc, _ess, _rho, _visual), Qdisp(_q),
			adjrhs(_adjrhs),  maxstress(_mstres), lower_bound(_lb),  
			umbral(_umb), MyStress(_stress), strcoef(_coef),
			Rhohat(_rhat), beta(_beta), eta(_eta)
			{ 
                	cte = 1.;
    				for(int i = 0; i < Geometry::NumGeom ;i++)
    					irs[i] = _irs[i];
			}
    	    void Update()
    	    {
    	    	cte = (siter < umbral) ? mstr/compliance : cte;
    	    }
		~ToptimizIPOPTStr() { }

		// same functions as in ToptimizIPOPT
	 	virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
	        Index& nnz_h_lag, IndexStyleEnum& index_style);
	  	virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
	        Index m, Number* g_l, Number* g_u);
	  	virtual bool get_starting_point(Index n, bool init_x, Number* x,
	        bool init_z, Number* z_L, Number* z_U, Index m, bool init_lambda,
	        Number* lambda);
	  	virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);
	  	virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);
	  	virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);
	  	virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
	        Index m, Index nele_jac, Index* iRow, Index *jCol, Number* values);
	  	virtual bool eval_h(Index n, const Number* x, bool new_x,
	    	Number obj_factor, Index m, const Number* lambda, bool new_lambda,
	    	Index nele_hess, Index* iRow, Index* jCol, Number* values);
	  	virtual void finalize_solution(SolverReturn status,
	 		Index n, const Number* x, const Number* z_L, const Number* z_U, Index m,
	 		const Number* g, const Number* lambda, Number obj_value,
	 		const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 	virtual bool intermediate_callback(AlgorithmMode mode,
	    	Index iter, Number obj_value, Number inf_pr, Number inf_du,
	    	Number mu, Number d_norm, Number regularization_size,
	      	Number alpha_du, Number alpha_pr, Index ls_trials,
	      	const IpoptData* ip_data, IpoptCalculatedQuantities* ip_cq);
	 };

