#include "functions.hpp"
#include "func_NLOPT.hpp"

#ifndef TOPTIMIZ_NLOPT
#define TOPTIMIZ_NLOPT

// returns the value of the compliance function for NLOPT
double ToptimizNLOPT::myfunc(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$

		a->Update();
		a->Assemble();

		$density$

		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

	    // Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif

		compliance = ((Vector) *b) * U;
		double obj_value = Mobj*compliance;

		if (!grad.empty())	
		{	
			Vector Y(x.size());
			$extra$
			der->Update();
			der->Assemble();
			rho->FilMult(*der,Y);
			Y *= Mobj;

			for (unsigned int i=0; i<x.size(); i++)
    			grad[i] = Y(i);
 		}
		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ") ";
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Compliance: " << compliance << "'" << endl;    
		}

		siter++;

		return obj_value;
	}

// return the value of the volume constraint: g(x) for NLOPT
double ToptimizNLOPT::myconstraint(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$
				
	    vol->Update();
		vol->Assemble();

		double cons = ((Vector) *y) * ((Vector) *vol);
		if (!grad.empty())
		{
			$vectorD_NLOPT$
		}
		cons -= 1;
		cout << "  Restr: "<< cons << endl;
		return cons;
	}


// returns the value of the volume function for NLOPT
double ToptimizNLOPT::myfuncVol(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$

	    vol->Update();
		vol->Assemble();

		double obj_value = ((Vector) *y) * ((Vector) *vol);

		if (!grad.empty())
		{
			$vectorVolD_NLOPT$
		}
		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		"  ";
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar; 
			sout << "valuerange 0.0 1.0\n";            
			sout << "plot_caption 'Iteration: " << siter <<
			"  Volume: " << obj_value << "'" << endl;    
		}

		siter++;

		return obj_value;
	}



// returns the value of the compliance function for NLOPT
double ToptimizNLOPT::myconstraintVol(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$

		a->Update();
		a->Assemble();

		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

	    // Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif
		
		compliance = ((Vector) *b) * U;
		double cons = Mobj*compliance -1.;
 
		if (!grad.empty())	
		{	
			Vector Y(x.size());

			$extra$

			der->Update();
			der->Assemble();
			rho->FilMult(*der,Y);
			Y *= Mobj;

			for (unsigned int i=0; i<x.size(); i++)
    			grad[i] = Y(i);
 		}
		cout << "Restr:  "  << cons << " (" << compliance << ")" << endl;

		return cons;
	}







double ToptimizNLOPTML::myfunc(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		
		$filtering$


	    SparseMatrix A[n_f];
	    Vector B[n_f], U[n_f];
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M;
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	    #endif
	    compliance = 0.;
	    double compli;

	    a->Update();
	    a->Assemble();
	    for (int i=0; i<n_f; i++)
	    {
	        a->FormLinearSystem(ess_tdof_list, *Udisp[i], *b[i], A[i], U[i], B[i]);

	        // Solving system with PCG or UMFPACK (if installed)   
	        #ifndef MFEM_USE_SUITESPARSE
	            M = GSSmoother(A[i]);
	            PCG(A[i], M, B[i], U[i], 0, ITER, TOL, 0.0);
	        #else
	            umf_solver.SetOperator(A[i]);
	            umf_solver.Mult(B[i], U[i]);
	        #endif
	        
	        a->RecoverFEMSolution(U[i], *b[i], *Udisp[i]);
	        compli = ((Vector) *b[i]) * U[i];
	        compliance += lambdacoef[i]*compli;
	    }

		double obj_value = Mobj*compliance;

		if (!grad.empty())	
		{	
			Vector Y[n_f];
			$extra$
			for (int i=0;i<n_f;i++)
			{
				Y[i].SetSize(x.size());
				


				der[i]->Update();
				der[i]->Assemble();

				rho->FilMult(*der[i],Y[i]);
			}
            
			for (unsigned int i=0; i<x.size(); i++)
			{
				grad[i] = 0;
				for (int k=0;k<n_f;k++)
    				grad[i] += Mobj*lambdacoef[k]*Y[k](i);
			}
 		}
		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ") ";
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Compliance: " << compliance << "'" << endl;    
		}

		siter++;

		return obj_value;
	}

// return the value of the volume constraint: g(x) for NLOPT
double ToptimizNLOPTML::myconstraint(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		
		$filtering$

				
	    vol->Update();
		vol->Assemble();

		double cons = ((Vector) *y) * ((Vector) *vol);
		if (!grad.empty())
		{
			
		$vectorD_NLOPT$

		}
		cons -= 1;
		cout << "  Restr: "<< cons << endl;
		return cons;
	}










// returns the value of the compliance function for NLOPT
double ToptimizNLOPTMec::myfunc(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$
		
		a->Update();
		a->Assemble();
		
		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

	    // Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif

		a->RecoverFEMSolution(U, *b, *Udisp);
		compliance = ((Vector) *c) * ((Vector) *Udisp);
		double obj_value = Mobj*compliance;

		if (!grad.empty())	
		{	

			// adjoint problem
			Vector BAdj, QAdj;
			a->FormLinearSystem(ess_tdof_list, *Qdisp, *c, A, QAdj, BAdj);

		    // Solving system with PCG or UMFPACK (if installed)   
		    #ifndef MFEM_USE_SUITESPARSE
		        PCG(A, M, BAdj, QAdj, 0, ITER, TOL, 0.0);
		    #else
		        umf_solver.Mult(BAdj, QAdj);
		    #endif

            a->RecoverFEMSolution(QAdj, *c, *Qdisp);
            
			Vector Y(x.size());

			$extra$

			der->Update();
			der->Assemble();
			rho->FilMult(*der,Y);
			Y *= Mobj;

			for (unsigned int i=0; i<x.size(); i++)
    			grad[i] = Y(i);
 		}
		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		" (" << compliance << ") ";
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar << endl; 
			sout << "plot_caption 'Iteration: " << siter <<
			"  Objective: " << compliance << "'" << endl;    
		}

		siter++;

		return obj_value;
	}

// return the value of the volume constraint: g(x) for NLOPT
double ToptimizNLOPTMec::myconstraint(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$
		$extra$
				
	    vol->Update();
		vol->Assemble();

		double cons = ((Vector) *y) * ((Vector) *vol);
		if (!grad.empty())
		{
			$vectorD_NLOPT$
		}
		cons -=  1;
		cout << "  Restr: "<< cons << endl;
		return cons;
	}


// returns the value of the Volume for Stress problem
double ToptimizNLOPTStr::myfunc(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$

	    vol->Update();
		vol->Assemble();

		double obj_value = ((Vector) *y) * ((Vector) *vol);

		if (!grad.empty())
		{
			$vectorVolD_NLOPT$
		}
		cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
		"  ";
		if (visualization && sout.good())
		{
			sout << "solution\n" << *mesh << *Rhobar; 
			sout << "valuerange 0.0 1.0\n";            
			sout << "plot_caption 'Iteration: " << siter <<
			"  Volume: " << obj_value << "'" << endl;    
		}

		siter++;

		return obj_value;
	}


// returns the value of the P-Norm stress (constraint) for Stress problem
double ToptimizNLOPTStr::myconstraint(const vector<double> &x, vector<double> &grad)
	{
		Filter &u = *rho;
		for (unsigned int i = 0; i<x.size() ; i++)
			u(i) = x[i];

		$filtering$
		$extra$
		
		a->Update();
		a->Assemble();
		
		SparseMatrix A;
		Vector B, U;
		a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

	    // Solving system with PCG or UMFPACK (if installed)   
	    #ifndef MFEM_USE_SUITESPARSE
	        GSSmoother M(A);
	        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
	    #else
	        UMFPackSolver umf_solver(true);
	        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
	        umf_solver.SetOperator(A);
	        umf_solver.Mult(B, U);
	    #endif

		a->RecoverFEMSolution(U, *b, *Udisp);
        
        compliance = ComputeLpNorm(PNORM, *strcoef, *mesh, irs);
        MyStress->ProjectCoefficient(*strcoef);
        mstr = MyStress->Max();
		
        Mobj = cte * pow(compliance,1-PNORM);
        double constr = cte*compliance/maxstress - 1.;

		if (!grad.empty())	
		{	

            adjrhs->Update();
            adjrhs->Assemble();
            
			// adjoint problem
			Vector BAdj, QAdj;
			a->FormLinearSystem(ess_tdof_list, *Qdisp, *adjrhs, A, QAdj, BAdj);

		    // Solving system with PCG or UMFPACK (if installed)   
		    #ifndef MFEM_USE_SUITESPARSE
		        PCG(A, M, BAdj, QAdj, 0, ITER, TOL, 0.0);
		    #else
		        umf_solver.Mult(BAdj, QAdj);
		    #endif

            a->RecoverFEMSolution(QAdj, *adjrhs, *Qdisp);
            
			Vector Y(x.size());

			$extra$

			der->Update();
			der->Assemble();
			rho->FilMult(*der,Y);
			Y /= maxstress;
			for (unsigned int i=0; i<x.size(); i++)
    			grad[i] = Y(i);
 		}

		cout << "Restr:  "  << constr << " cte = " << cte << " (P-norm: " << cte*compliance << ", Max-norm: " 
		     << mstr  <<")" << endl;

		Update();

		return constr;
	}



// wrapper functions for NLOPT
double wrapper(const vector<double> &x, vector<double> &grad, void *other) 
	{
		return reinterpret_cast<$problem$>(other)->$funcion$(x, grad);
	}
double conswrapper(const vector<double> &x, vector<double> &grad, void *other) 
	{
		return reinterpret_cast<$problem$>(other)->$restriccion$(x, grad);
	}

#endif