#include "functions.hpp"
$include$


$headers$

int main(int argc, char *argv[])
  {
    unsigned t0,t1;
    t0 = clock(); 
    cout << "PROCESS STARTED \n" << endl;
    
    bool visualization = 1; // set visualization
    double Volfrac = 0.25, radius = 2.5; // Volume fraction and filter radius
    int maxiter = 150;
    double tol = 1.e-8;
    int loops = 3;
    int betaupdate = 2;
    int umbral = 50;
    const char *mesh_file = "$mesh$";
    
    $gravity$

    double beta = 1.0, eta = 0.5;
    double zeta = 0.2, neta = 0.5;

    // reading data
    OptionsParser args(argc, argv);
    args.AddOption(&visualization, "-vis", "--visualization", "-no-vis",
                  "--no-visualization",
                  "Enable or disable GLVis visualization.");
    args.AddOption(&radius, "-r", "--radius",
                  "Filter radius");
    args.AddOption(&Volfrac, "-v", "--volfraction",
                  "Volume/Compliance fraction or Maximum Stress");
    args.AddOption(&maxiter, "-i", "--iter",
                  "Maximum number of iterations");
    args.AddOption(&tol, "-t", "--tolerance",
                  "Maximum tolerance (stopping criteria)");
    args.AddOption(&loops, "-l", "--betaloops",
                  "Number of loops of Heaviside projections");    
    args.AddOption(&betaupdate, "-bup", "--betaupdate",
                  "Factor for updating beta parameter");
    args.AddOption(&beta, "-b", "--beta",
                  "Beta parameter for Heaviside projection");
    args.AddOption(&eta, "-e", "--eta",
                  "Eta parameter for Heaviside projection");
    args.AddOption(&zeta, "-z", "--zeta",
                  "Zeta parameter for optimality conditions");
    args.AddOption(&neta, "-n", "--neta",
                  "Eta parameter for optimality conditions");
    args.AddOption(&umbral, "-u", "--umbral",
                  "Threshold parameter for stress constraint");

    args.Parse();
    if (!args.Good())
    {
      args.PrintUsage(cout);
      return 1;
    }
    // Mesh reading
    MyMesh *mesh = new MyMesh(mesh_file);
    
    // spatial dimension
    int dim = mesh->Dimension();
    // number of labels on boundary
    int natr = mesh->bdr_attributes.Max();
    // number of elements
    int ndim = mesh->GetNE();

    // FE spaces
    FiniteElementCollection *fec, *fec0;
    fec = new H1_FECollection(1, dim);
    fec0 = new L2_FECollection(0, dim);
    FiniteElementSpace *p1espace, *p0espace;
    p1espace = new FiniteElementSpace(mesh, fec, dim);
    p0espace = new FiniteElementSpace(mesh, fec0);

    // Functions
    ConstantCoefficient one(1.0);
    // GridFunction for densities
    GridFunction *Rhobar;
    Rhobar = new GridFunction(p0espace);
    Rhobar->ProjectCoefficient(one);

    // GridFunction for displacements
    GridFunction *Udisp;
    Udisp = new GridFunction(p1espace);
    *Udisp = 0.0; // define in this way to set b.c. 

    $displacements$

    // Auxiliar GridFunction to compute integrals
    GridFunction *y;
    y = new GridFunction(p0espace); 
    *y = 1.;

    // Auxiliary GridFunction to compute stress
    GridFunction *gridstress;
    gridstress = new GridFunction(p0espace);
    *gridstress = 0.;

	// Coefficient for computing relaxed stress

	$VonMises$ *sigmavm;    
    sigmavm = new $VonMises$(*Udisp);
	


    // problem data

    // Dirichlet conditions
    Array<int>ess_tdof_list;
    Array<int> ess_bdr(natr);
    Array<int> etiquetas;

    $dirichlet$

    // Right hand side for elasticity system
    $RHSdef$

    $linearform$

    $gravity_force$

    $assamblingb$

    $mechanism$

    // Computing Maximum Compliance and volume 
    // and Elasticity problem definition
    
    // Linear form to compute volume
    LinearForm *vol;
    vol = new LinearForm(p0espace);
    GridFunctionCoefficient R(Rhobar);

    vol->AddDomainIntegrator(new DomainLFIntegrator(R));
    vol->Assemble();

    // Measure of the domain
    double medida = ((Vector) *y) * ((Vector) *vol);
    cout << "Total Volume: " << medida << endl;

    
    // Bilinear form for elasticity problem
    BilinearForm *a;
    a = new BilinearForm(p1espace);
    SIMPCoeff E(*Rhobar);

    a->AddDomainIntegrator(new ElasticityIntegrator(E,lambda,mu));

    $matrixform$

    a->Assemble();

    // Normalization constant for compliance 
    ConstantCoefficient volfrac(Volfrac);
 
    $projectRhobar$

    $density$

    $initialsystem$

    $initcompliance$

    

    // constants to pass to the main class

    $mobjs$
    
    $volconstant$

    $yvol$

    // auxiliar function for adjoint problem (Mech/Stress Prob. only)
    $qdisp$ 

    // function for filtering and interpolation
    Filter *rho;

    $initialfilter$

  	// Linear form to compute derivative of compliance
    $defderivative$

    $additionalder$

    // self-weight hypothesis
    $gravity_der$

    $assamblingder$;

    $adjrhs$

    // Linear form to compute derivative of constraint
    LinearForm *dconres;
    dconres = new LinearForm(p0espace);

    $dconres$

    dconres->Assemble();

    $passive$
    
    $initialization$

    $method$

    $bucleHeaviside$

    $after$


    $finalsol$

    $savesol$

    $threshold$

    
    delete Udisp;
    delete Rhobar;
    delete y;
    delete gridstress;
    delete stressr;
    delete sigmavm;
    $delete$ 
    $deletearray$
    delete rho;
    delete a;
    delete vol;
    delete dconres;
    delete fec;
    delete fec0;
    delete p1espace;
    delete p0espace;
    delete mesh;

    t1 = clock();
    double time = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "Execution Time: " << time << endl;
    cout << "PROCESS ENDED" << endl;

    return 0;
  }


$analyticfuns$
