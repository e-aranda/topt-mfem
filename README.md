# Toptimiz3D
Graphical User Interface for designing and solving topological optimization problems with MFEM

Available for Linux and Mac


## Try without install

We have created a live CD with Toptimiz3D installed that can be used for booting a computer or a virtual machine and try the software without installing nothing. The CD image can be download [here](https://sourceforge.net/projects/live-toptimiz3d/files/)

You can burn a CD, make a bootable USB, or use the image in a Virtual Machine

## Installation 

#### Requirements:

  - Python 3
    - requires modules: `wxpython` (4.*), `vtk` (> 5.0), `numpy`, `pexpect`, `requests`, `dictdiffer`

  - G++ compiler and make tools

##### Also needs:

  - `MFEM`
  - `GLVIS`
  - `NLOPT`
  - `IPOPT`
  - `GSL`

##### Instructions:

- Python 3 and their modules, G++ compiler and `make` tools  must be installed before starting the installation process.

- IPOPT must be installed separately before starting the installation process. The installation of IPOPT requires a solver for sparse matrices. Check [below](#ipopt) for details.

- For the rest of the requirements, the installation script is able to download and install in your system, but there are some other requirements that must be fulfilled. See [Auxiliary Tools](#auxiliary-tools) section below.

##### Recommended:
  - `SuiteSparse`. Check [below](#suitesparse) for details.


### Installing Python modules:

Install a Python 3 environment with the required modules. We recommend install 
[Miniconda](https://conda.io/en/latest/miniconda.html) and add to the PATH 

Once Miniconda is installed, in a terminal write:

```
conda install numpy vtk wxpython
pip install dictdiffer, pexpect, requests
```

##### On Ubuntu/Debian systems

```
sudo apt-get install python3-vtk7 python3-wxgtk4.0 python3-pip python3-numpy
sudo pip3 install dictdiffer
```

### G++ and make

For Ubuntu/Debian
```
sudo apt install gcc make automake 
```

For Mac,install `XCode`

### Auxiliary Tools 

##### CMake

NLOPT compilation needs `CMake`. 

For Mac OSX, [download](https://cmake.org/download/) the `dmg` package and install.
Once installed, Open the application and choose in the menu Tools -> How To Install For Command Line Use and follows instructions

For Ubuntu/Debian:
```
sudo apt-get install cmake
```

##### Graphic libraries (XQuartz, FreeType, ...)

GLVIS compilation in Mac OSX needs `XQuartz`. [Download](https://www.xquartz.org/) the `dmg` package and install. Reboot.

In Ubuntu/Debian:
```
sudo apt-get install libx11-dev libpng-dev libfreetype6-dev libfontconfig1-dev freeglut3-dev
```

### Binaries for NLOPT, IPOPT, GSL, EIGEN

These packages exists in Ubuntu repositories. Install with `apt-get`:

```
sudo apt-get install libgsl-dev libnlopt-dev coinor-libipopt-dev libeigen3-dev
```

*Note*: In Ubuntu 18.10 also install `libnlopt-cxx-dev`


### IPOPT

IPOPT requires at least one linear solver for sparse matrices. Since this third party software is released under different licenses it cannot be distributed and you have to obtain by yourself. We recommend [HSL](http://hsl.rl.ac.uk/ipopt)

##### For Mac OSX

You may consider to use [Homebrew](https://brew.sh/) to install IPOPT

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install dartsim/dart/ipopt
```

After that, you must include `-I/usr/local/opt/ipopt/include` as a `--cpp` option and
`-L/usr/local/opt/ipopt/lib` as a `--ld` option.



### SuiteSparse

It is also recommended compile MFEM with SuiteSparse for a better perfomance. [Download](http://faculty.cse.tamu.edu/davis/SuiteSparse/SuiteSparse-5.4.0.tar.gz) here.

In Ubuntu, you can install from the repositories:

```
sudo apt-get install libsuitesparse-dev libmetis-dev
```

## Toptimiz3D installation

Get the compress file from Gitlab, uncompress and move into the folder. In a terminal run

```
python install.py [options]
```

The script will check your system and try to install the required packages.

#### Options:

- `--dir`: destination folder ($HOME/toptimiz3D by default)
- `--ld`: LDDFLAGS for configure. If you have custom installations of libraries in different paths, use as
`--ld="-L/path/to/lib1 -L/path/to/lib2"`
- `--cpp`: CPPFLAGS for configure. If you have custom installations of headers in different paths, use as
`--cpp="-I/path/to/include1 -I/path/to/include2"`
- `--suitesparse`: if you have/want MFEM compiled with SuiteSparse
- `--suitedir`: directory where you have SuiteSparse. Use as `--suitesparse=/path/to/SuiteSparse` 
- `--yes`: Answer YES to all

*Note 1*: in Ubuntu, if you have installed SuiteSpase from the repositories, please add `-I/usr/include/suitesparse` to `--cpp` to find the header `umfpack.h`. 

*Note 2*: in Ubuntu, if you find a linking error (`-lGL` not found) in GLVIS installation try:
```
sudo ln -s /usr/lib/libGL.so.1 /usr/lib/libGL.so
```
 
The installation will ask for creating a direct access in the Desktop. Also you can run the software from a terminal
```
python3 destinationfolder/code/toptimiz3d.py
```
where `destinationfolder` is the folder where the software has been installed.

## Using the software

- In the **File** menu, select `Load Config File` and choose an example from __examples__ folder. 
- In **Process** menu, select `Processing` the press __Run__ button.

