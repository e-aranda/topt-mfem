"""
Created on Thu Jul 27 11:57:09 2017

@author: earanda

Main menu of the application
"""

import wx
import pickle
import shutil
import os
import updater
import dictdiffer
from zipfile import ZipFile
import tempfile
import webbrowser


# ----------------------------------------------------------------------------
class AppMenu(object):
    
    """
    Panel containing tickers and label entries for individual entries
    """
    def __init__(self,padre):
        """
        Menus 
        """
        self.padre = padre
        self.menubar = wx.MenuBar()
        
        # File menu
        fileMenu = wx.Menu()
        m_load = fileMenu.Append(-1,"Load Config File")
        m_save = fileMenu.Append(-1,"Save Config File")
        
        self.savevtk = fileMenu.Append(-1,"Save VTK Results")
        
        self.m_txt = fileMenu.Append(-1,"Save Text Results")
        
        # choose file name for saving result
        savenumeric = wx.Menu()
        m_numpur = savenumeric.Append(600,"Save Density Result")
        m_numfil = savenumeric.Append(601,"Save Filtered Result")
        self.savenum = fileMenu.AppendSubMenu(savenumeric,"Save Numeric Result")
        
        # FreeFem edp file saving
        self.m_edp = fileMenu.Append(-1,"Save C++ Files")
        
        settings = fileMenu.Append(114,"Settings")
        
        # reset and exit
        m_reset = fileMenu.Append(-1,"Reset Default Values")
        m_salir = fileMenu.Append(-1,"Exit")
        self.menubar.Append(fileMenu,"File")
                
        # Problems menu
        probls = wx.Menu()
        self.m_prob1 = probls.AppendRadioItem(-1,"Compliance")
        self.m_prob5 = probls.AppendRadioItem(-1,"Multiload")
        self.m_prob2 = probls.AppendRadioItem(-1,"Volume")
        self.m_prob3 = probls.AppendRadioItem(-1,"Mechanism")
        self.m_prob4 = probls.AppendRadioItem(-1,"Stress")
        self.menubar.Append(probls,"Problem")
        
        # Parameters menu
        parameters = wx.Menu()
        
        dimension = wx.Menu()
        self.m_dim2 = dimension.Append(-1,"Dimension 2","",wx.ITEM_RADIO)
        self.m_dim3 = dimension.Append(-1,"Dimension 3","",wx.ITEM_RADIO)
        parameters.Append(-1,"Dimension",dimension)
        
        m_elastic = parameters.Append(101,"Elastic Constants")
        m_optimiz = parameters.Append(102,"Optimization's Parameters")
        self.m_optimality = parameters.Append(103,"OC Parameters")
        self.m_stress = parameters.Append(115,"Stress Parameters")
        initden = parameters.Append(111,"Initial Density")      
        
        self.menubar.Append(parameters,"Parameters")
        
        
        # Loads and other conditions menu
        loads = wx.Menu()
        m_inload = loads.Append(106,"Body Loads")
        m_bdc = wx.Menu()
        m_bdload = m_bdc.Append(105,"Boundary Loads")
        m_robin = m_bdc.Append(109,"Robin Boundary Conditions")
        m_clamped = m_bdc.Append(104,"Fixed Displacements")
        self.m_multi = m_bdc.Append(116,"Boundary Multi Loads")
        loads.AppendSubMenu(m_bdc,"Boundary Conditions")
        
        self.m_passive = loads.Append(107,"Passive Zones")
        
        hip = wx.Menu()
        self.m_density = hip.Append(108,"Self-Weight")
        
        plane = wx.Menu()
        self.m_tension = plane.AppendRadioItem(-1,"Plane Stress")
        self.m_deform = plane.AppendRadioItem(-1,"Plane Strain")
        hip.Append(-1,"Plane Conditions",plane)
        loads.AppendSubMenu(hip,"Hypothesis")
        
        self.m_mech = loads.Append(110,"Mechanism Objective")
        
        self.menubar.Append(loads,"Input Data")
    
        
        # Methods menu
        method = wx.Menu()
        
        opt = wx.Menu()
        self.m_meth1 = opt.AppendRadioItem(-1,"MMA")
        self.m_meth3 = opt.AppendRadioItem(-1,"IPOPT")
        self.m_meth2 = opt.AppendRadioItem(-1,"Optimality Conditions")
        fil = wx.Menu()
        self.m_filtro1 = fil.AppendRadioItem(-1,"Conic filter")
        self.m_filtro2 = fil.AppendRadioItem(-1,"Helmholtz type filter")
        projection = wx.Menu()
        self.m_withbeta = projection.AppendRadioItem(-1,"With Heaviside Projection")
        self.m_withoutbeta = projection.AppendRadioItem(-1,"Without Heaviside Projection")
        
        method.Append(-1,"Optimization Method",opt)
        method.Append(-1,"Filter Type",fil)
        method.Append(-1,"Heaviside Projection",projection)
        
        self.menubar.Append(method,"Methods")
        
        # Processing menu
        proc = wx.Menu()
        m_procesar = proc.Append(-1,"Processing")
        m_output = proc.Append(113,"Back to terminal")
        self.menubar.Append(proc,"Process")
        
        # Adaptation mesh menu 
        adapt = wx.Menu()
        m_adaptation = adapt.Append(112,"Configure Adaptation")
#        self.menubar.Append(adapt,"Mesh Adaptation")
 

        

############################################################################3
#       Update menu        
        update = wx.Menu()
        lookfor = update.Append(-1,"Check for Updates")
        self.menubar.Append(update,"Update")      
        self.padre.Bind(wx.EVT_MENU, self.Update, lookfor)

        
############################################################################3
#       Help menu
        
        ayuda = wx.Menu()
        m_ayuda = ayuda.Append(-1,"Manual")
        m_about = ayuda.Append(-1,"About Toptimiz3D")
        self.menubar.Append(ayuda,"Help")        
                
        # Set Menu in application
        self.padre.SetMenuBar(self.menubar)
        
        
        
########################################################################        
        # bind events on File menu
        self.padre.Bind(wx.EVT_MENU, self.salir, m_salir)
        self.padre.Bind(wx.EVT_MENU, self.saveFile, m_save)
        self.padre.Bind(wx.EVT_MENU, self.loadFile, m_load)        
        self.padre.Bind(wx.EVT_MENU, self.saveVTKResult, self.savevtk)
        self.padre.Bind(wx.EVT_MENU, self.saveNumResult, m_numpur)
        self.padre.Bind(wx.EVT_MENU, self.saveNumResult, m_numfil)
        self.padre.Bind(wx.EVT_MENU, self.saveTxtResult, self.m_txt)
        self.padre.Bind(wx.EVT_MENU, self.saveCPPFile, self.m_edp)
        self.padre.Bind(wx.EVT_MENU, self.resetDefault, m_reset)
    
        
        # change dimension
        self.padre.Bind(wx.EVT_MENU,lambda event, dim=2: self.setDim(event,dim),self.m_dim2)
        self.padre.Bind(wx.EVT_MENU,lambda event, dim=3: self.setDim(event,dim),self.m_dim3)
        
        # bind events (Show Panels) on Parameters and Loads Menu
        for x in (m_elastic,m_optimiz,self.m_optimality,m_clamped,m_bdload,\
                  m_inload,m_robin,self.m_passive,self.m_density,self.m_mech,initden,m_adaptation,m_output,\
                  settings,self.m_stress,self.m_multi):
            self.padre.Bind(wx.EVT_MENU, self.show, x)
            
        # bind events for Problems
        self.padre.Bind(wx.EVT_MENU, lambda event, var='problem', val='Compliance', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_prob1)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='problem', val='Volume', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_prob2)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='problem', val='Mechanism', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_prob3)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='problem', val='Stress', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_prob4)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='problem', val='Multiload',\
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_prob5)
         
        # set optimization method and filter (activation of some panel)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='methodOPT', val='MMA', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_meth1)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='methodOPT', val='IPOPT', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_meth3)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='methodOPT', val='OC', \
                  call=self.checkActivation: self.setVal(event,var,val,call), self.m_meth2)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='methodFILTER', val='Conic', call=None: \
                  self.setVal(event,var,val,call),self.m_filtro1)    
        self.padre.Bind(wx.EVT_MENU, lambda event, var='betaPROJECTION', val=True, call=self.changeBeta: \
                  self.setValCalled(event,var,val,call),self.m_withbeta)    
        self.padre.Bind(wx.EVT_MENU, lambda event, var='betaPROJECTION', val=False, call=self.changeBeta: \
                  self.setValCalled(event,var,val,call),self.m_withoutbeta)    
        
        self.padre.Bind(wx.EVT_MENU, lambda event, var='methodFILTER', val='Hemholz', call=None: \
                  self.setVal(event,var,val,call),self.m_filtro2)    
        
        # set plain stress or strain hypothesis
        self.padre.Bind(wx.EVT_MENU, lambda event, var='tensionplana',val=True,call=None:\
                  self.setVal(event,var,val,call),self.m_tension)
        self.padre.Bind(wx.EVT_MENU, lambda event, var='tensionplana',val=False,call=None:\
                  self.setVal(event,var,val,call),self.m_deform)
        
        
        # bind menu for processing
        self.padre.Bind(wx.EVT_MENU, self.padre.Processing, m_procesar)
        
        # bind events for help menu
        self.padre.Bind(wx.EVT_MENU, self.OpenManual, m_ayuda)
        self.padre.Bind(wx.EVT_MENU, self.OpenAbout, m_about)
        

    ########################################################################################################
    ########################################################################################################
    ########################################################################################################
        
    def salir(self,event):
        """
        Exit event: close application
        """
        self.padre.Close(True)
        
    def loadFile(self,event,filename=None):
        """
        Load configuration file
        """
        if not filename:
            dlg = wx.FileDialog(self.padre, "Load Config File", self.padre.mainpath, "", 
                                          "Config files (*.topt)|*.topt", 
                                          wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
            else:
                return
            
        # open configuration file and update all values
        with open(filename,"rb") as input_file:
            # backup if error
            olds = self.padre.values
            self.padre.values = pickle.load(input_file)
            try:    
                self.padre.updating()
            except KeyError as e:
                result = dictdiffer.diff(self.padre.values, olds)
                patched = dictdiffer.patch(result, self.padre.values)
                message = "Configuration file is no longer valid for this version. If you continue some values may be changed"
                if self.YesNo(message,"Continue?"):
                    self.padre.values = patched
                else:
                    self.padre.values = olds
                self.padre.updating()
#       uncomment for debug
#        print(self.padre.values)
        if not filename:
            dlg.Destroy()
        
    def saveFile(self, event):
        """
        Save configuration file
        """
        dlg = wx.FileDialog(self.padre, "Save Config File", self.padre.mainpath, "", 
                                       "Config files (*.topt)|*.topt", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_CANCEL:
            return
        filename = dlg.GetPath()  
        with open(filename, "wb") as output_file:
            pickle.dump(self.padre.values, output_file, protocol=2)
        dlg.Destroy()       
        
        
    def saveVTKResult(self,event):
        """
        Save VTK result file
        """
        dlg = wx.FileDialog(self.padre, "Save VTK File", self.padre.mainpath, "", 
                                       "VTK files (*.vtk)|*.vtk", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            origfile = os.path.join(self.padre.basepath,self.padre.values['namefile'] + '.vtk')
            shutil.copyfile(origfile,filename)
        else:
            return
        dlg.Destroy()

    def saveNumResult(self,event):
        """
        Save Numeric result file
        """
        i = event.GetId() - 600
        result = ('resultadopure','resultadofilter')
        dlg = wx.FileDialog(self.padre, "Save Numeric Result", self.padre.mainpath, "", 
                                       "MFEM files (*.gf)|*.gf", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            
            origfile = os.path.join(self.padre.basepath,self.padre.values[result[i]])
            shutil.copyfile(origfile,filename)
        else:
            return
        
    def saveTxtResult(self,event):
        """
        Save txt result file
        """
        
        dlg = wx.FileDialog(self.padre, "Save Txt File", self.padre.mainpath, "", 
                                       "TXT files (*.txt)|*.txt", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            with open(filename,'w') as f:
                for x in self.padre.outputpanel.stack:
                    f.write(x.decode('utf-8'))
        else:
            return
       
        
    def saveCPPFile(self,event):
        """
        Save ZIP file with C++ source files to allow running in other machine
        """
        solpure = os.path.join(self.padre.basepath,self.padre.values['resultadopure'])
        solfilter =  os.path.join(self.padre.basepath,self.padre.values['resultadofilter'])
        solvtk = os.path.join(self.padre.basepath,self.padre.values['namefile']+'.vtk') 
        thres = os.path.join(self.padre.basepath,self.padre.values['namefile'] + '-threshold.vtk');
        files = ['constants.hpp','functions.hpp','functions.cpp']
        if self.padre.values['methodOPT'] == "MMA":
            sufix = "_NLOPT"
            library = "-lnlopt"
        elif self.padre.values['methodOPT'] == "OC":
            sufix = "_OC"
            library = ""
        elif self.padre.values['methodOPT'] == "IPOPT": 
            sufix = "_IPOPT"
            library = "-lipopt"
        else:
            self.padre.ShowMessage("Error","No optimization method?")
            return
        file = 'FUN' + sufix + '.cpp'
        files.append(file)
        files.append('func' + sufix + '.hpp')
        
        if self.padre.values['init_random']:
            library += ' -lgsl'
        
        
        mesh = os.path.abspath(os.path.normpath((os.path.join(self.padre.mainpath,self.padre.values['path'],self.padre.values['meshfile']))))  
        if self.padre.values['init_file']:
            rutainit = os.path.normpath(os.path.join(self.padre.mainpath,self.padre.values['init_file']))
        else:
            rutainit = None
        script_dir = os.path.dirname(os.path.abspath(__file__))
        
        
        dlg = wx.FileDialog(self.padre, "Save C++ File", "", "", 
                                       "ZIP files (*.zip)|*.zip", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            
            # change MAIN.cpp
            main = os.path.normpath(os.path.join(script_dir,'../src/MAIN.cpp'))
            with open(main,'r') as f:
                fmain = f.read()
            
            for x,y in zip((mesh,solpure,solfilter,solvtk,thres),
                (self.padre.values['meshfile'],
                 self.padre.values['resultadopure'],
                 self.padre.values['resultadofilter'],
                 self.padre.values['namefile']+'.vtk',
                 self.padre.values['namefile']+'-threshold.vtk')):
                newcontent = fmain.replace(x,y)
                fmain = newcontent
            
            if rutainit:    
                newcontent = fmain.replace(rutainit,os.path.basename(self.padre.values['init_file']))
                  
                
                
            nativemakef =  os.path.normpath(os.path.join(script_dir,'../src/makefile'))
            
            with open(nativemakef,'r') as f:
                content = f.readlines()

            for i,x in enumerate(content):
                if '$(LIBRARY)' in x:
                    line = i
                    break

            tmpdir = tempfile.mkdtemp()  
            mainfile = os.path.join(tmpdir,'main.cpp')
            makefile = os.path.join(tmpdir,'makefile')
            
            with open(mainfile,'w') as f:
                f.write(newcontent)
                
            cont = content[:line+1]    
            cont.insert(0,'LIBRARY='+library+'\n')
            cont.append('program : main.cpp functions.cpp {file}\n'.format(file=file))
            cont.append('\t$(FC) $(FFLAGS) $^ -o program $(OTHER)\n')
            
            with open(makefile,'w') as f:
                for x in cont:
                    f.write(x)
                        
            with ZipFile(filename,'w') as zipp:
                for f in files:
                    ff = os.path.normpath(os.path.join(script_dir,'../src',f))
                    zipp.write(ff,os.path.basename(ff))
                if rutainit:
                    zipp.write(rutainit,os.path.basename(rutainit))
                zipp.write(mesh,os.path.basename(mesh))
                zipp.write(mainfile,os.path.basename('main.cpp'))
                zipp.write(makefile,os.path.basename('makefile'))
        else:
            return
        
    
    def resetDefault(self,event):
        """
        Clean all values (as if we start the program)
        """
        if self.YesNo("All values will be reset to defaults"):
            # clean values
            self.padre.initvalues()
            self.padre.adaptpanel.first_use = True
            self.padre.updating()
            # clean vtk panel
            self.padre.graphicmesh.ren.RemoveAllViewProps()
            self.padre.graphicmesh.desplegable.Clear()
            self.padre.graphicmesh.desplegable.Enable(False)
            # clean graphic panel (reset information)
            self.padre.graphicbot.shownt.SetLabel("Number of Elements: ")
            self.padre.graphicbot.shownp.SetLabel("Number of Nodes: ")
            self.padre.graphicbot.showbounds.SetLabel("Bounding Box: ")
            self.padre.graphicbot.innercombo.Clear()
            self.padre.graphicbot.boundcombo.Clear()
            
            self.padre.graphicbot.Refresh()
            self.padre.outputpanel.log.SetValue("")
            self.padre.outputpanel.Refresh()
            self.padre.adaptpanel.log.SetValue("")
            self.padre.adaptpanel.Refresh()
        else:
            return
            
    def YesNo(self, question, caption = 'Are you sure?'):
        """
        Confirmation dialog
        """
        dlg = wx.MessageDialog(self.padre, question, caption, wx.YES_NO | wx.ICON_QUESTION)
        result = dlg.ShowModal() == wx.ID_YES
        dlg.Destroy()
        return result
    
    
    
    ############################################################################
    
    
    
    def show(self,event):
        """
        Show the selected EntryPanel
        """
        i = event.GetId() - 101
        self.activar(self.padre.listofpanels[i])
        
    def activar(self,panel):
        """
        ReplaceWindow action on splitting panel
        """
        self.padre.active.Hide()
        panel.Show()
        self.padre.leftpanel.ReplaceWindow(self.padre.active,panel)
        self.padre.active = panel


    def setDim(self,evt,m):
        """
        Activage/deactive some entries when dimension is changed
        """
        self.padre.values['dimension'] = m
        
        # clean entries in clamped conditions, robin panel
        # block, clean and change color in 3rd component of loads    
        for panel in [self.padre.bdloadspanel,self.padre.inloadpanel,\
                      self.padre.mechanism,self.padre.multiload,\
                      self.padre.robinpanel,self.padre.clampedpanel]:
            panel.set_dim(m)
        
        
        if m == 2:
            # change entry in menu ---> tiene que ser puesto en este orden, o da problemas
            self.m_dim2.Check(True)
            self.m_dim3.Check(False)
            
#            self.padre.values['tensionplana'] = True

#            self.menubar.EnableTop(6,True) # change to true to activate mesh adaptation

        if m == 3:
            # change entry in menu ----> tiene que ser puesto en este orden, o da problemas
            self.m_dim3.Check(True)            
            self.m_dim2.Check(False)

            self.padre.values['tensionplana'] = False   
            
            self.menubar.EnableTop(6,False)                 
         
        # menu of hypothesis of plain stress and strain    
            
        if self.padre.values['tensionplana']:
            self.m_tension.Check(True)
            self.m_deform.Check(False)
        else:
            self.m_deform.Check(True)
            self.m_tension.Check(False)

        self.checkActivation(None)
            
            
    def setVal(self,event,var,val,call):
        """
        Set some specific values and call a function if exists
        """
        self.padre.values[var] = val
        if call != None:
            call(None)            
            
    def setValCalled(self,event,var,val,call):
        """
        Set some specific values and call a function if exists
        """
        self.padre.values[var] = val
        if call != None:
            call(val)                        
        
            
    def checkActivation(self,event):
        """
        Check conditions and selecting correct disable/enable menus
        """
        # problems
        if self.padre.values['problem'] != "Compliance":
            self.m_density.Enable(False)
            self.padre.values['density'] = ''
            self.padre.values['density_tick'] = False
            if self.padre.densitypanel.IsShown():
                self.activar(self.padre.windparam)
        if self.padre.values['problem'] == "Multiload" \
            or self.padre.values['problem'] == "Compliance":
            self.m_meth2.Enable(True)
        else:
            self.m_meth2.Enable(False)
                
        if self.padre.values['problem'] != "Mechanism":
            self.m_mech.Enable(False)
            self.padre.mechanism.grid.ClearGrid()
            self.padre.values['obj_mechanism'] = [['','','','']]
            if self.padre.mechanism.IsShown():
                self.activar(self.padre.windparam)
                
            boo = True
            if self.padre.values['dimension'] == 3:
                boo = False
            
        else:
            self.m_mech.Enable(True)
            boo = False
            if self.padre.values['dimension'] == 2:
                self.padre.values['tensionplana'] = True
                self.m_tension.Check(True)
                     
        for x in [self.m_tension,self.m_deform]:
            x.Enable(boo)

            
        if self.padre.values['problem'] == "Volume":
            self.Volume_Select(True)
            self.m_stress.Enable(False)
        elif self.padre.values['problem'] == "Stress":
            self.Volume_Select(True,False)
            self.m_stress.Enable(True)
        else: 
            self.Volume_Select(False)
            self.m_stress.Enable(False)
            
            
        if self.padre.values['problem'] != "Multiload":
            self.m_multi.Enable(False)
        else:
            self.m_multi.Enable(True)
            
        # optimization methods
        if self.padre.values['methodOPT'] == 'OC':
            self.m_optimality.Enable(True)
            self.m_passive.Enable(False)
            self.m_density.Enable(False)
            self.m_prob2.Enable(False)
            self.m_prob3.Enable(False)
            self.m_prob4.Enable(False)
            if self.padre.passivepanel.IsShown() or self.padre.densitypanel.IsShown():
                self.activar(self.padre.windparam)
        else:
            self.m_optimality.Enable(False)
            self.m_passive.Enable(True)
            if self.padre.values['problem'] == "Compliance":
                self.m_density.Enable(True)
            if not self.padre.values['density_tick']:
                self.m_prob2.Enable(True)
                self.m_prob3.Enable(True)
                self.m_prob4.Enable(True)
            if self.padre.optimalparam.IsShown(): 
                self.activar(self.padre.windparam)
        
        self.resultActivation()
        
        
    def resultActivation(self):
        """
        Activation of files menus once is has been run or processed
        """
        
        self.m_edp.Enable(self.padre.isprocess)
        self.savevtk.Enable(self.padre.isrun)
        self.m_txt.Enable(self.padre.isrun)
        self.savenum.Enable(self.padre.isrun)

                
    def Volume_Select(self,a,b=True):
        """
        Select volume fraction or compliance fraction or maximum stress
        depending on problem (wrritten in this way due to strange error on Mac's)
        """
        if a and b:
            self.padre.optparam.paneles[1].Enable(True)
            self.padre.optparam.paneles[0].Enable(False)
            self.padre.optparam.paneles[2].Enable(False)
        elif b:
            self.padre.optparam.paneles[1].Enable(False)
            self.padre.optparam.paneles[0].Enable(True)
            self.padre.optparam.paneles[2].Enable(False)
        elif not b:
            self.padre.optparam.paneles[0].Enable(False)
            self.padre.optparam.paneles[1].Enable(False)
            self.padre.optparam.paneles[2].Enable(True)
            return
                

    def changeBeta(self,a):
        for x in (7,8):
            self.padre.optparam.paneles[x].Enable(a)
        

    ############################################################################
    
        
    def Update(self,evt):
        """
        Open new frame with updating information
        """
        frame = updater.UpdaterFrame(self.padre)
        frame.Show()
        
        
    def OpenManual(self,evt):
        fichero = os.path.join(self.padre.mainpath,'doc','manual.pdf')
        print(fichero)
        webbrowser.open_new(fichero)
        
    def OpenAbout(self,evt):
        ver = os.path.join(self.padre.mainpath,'VERSION')
        with open(os.path.realpath(ver),'r') as f:
            x = f.readline().strip()    
        y = (x.split('/')[0]).strip()

        self.padre.ShowMessage("About Toptimiz3D","Version " + y)
