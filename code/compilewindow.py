"""
Created on Wed Jan 23 13:14:41 2019

Window frame to show while system is compiling (fake time)

@author: earanda
"""


import wx
import time

class Progress(wx.Frame):
    def __init__(self, title, parent=None):
      wx.Frame.__init__(self, parent=parent, title=title)
    
      pnl = wx.Panel(self) 
      vbox = wx.BoxSizer(wx.VERTICAL)
    		
      hbox1 = wx.BoxSizer(wx.HORIZONTAL) 
      hbox2 = wx.BoxSizer(wx.HORIZONTAL)
    		
      self.gauge = wx.Gauge(pnl, size = (250, 25), style =  wx.GA_SMOOTH) 
      self.btn1 = wx.StaticText(pnl, label = "Please wait ...") 
    		
      hbox1.Add(self.gauge, proportion = 1, flag = wx.ALIGN_CENTRE) 
      hbox2.Add(self.btn1, proportion = 1, flag = wx.RIGHT, border = 10) 
     
      vbox.Add((0, 30)) 
      vbox.Add(hbox1, flag = wx.ALIGN_CENTRE) 
      vbox.Add((0, 20)) 
      vbox.Add(hbox2, proportion = 1, flag = wx.ALIGN_CENTRE) 
      pnl.SetSizer(vbox) 
     
      self.SetSize((300, 200)) 
      self.Centre() 
      self.Show(True)   
      self.gauge.SetValue(0)
      count = 0
      
      while count < 100:
          count += 1
          self.gauge.SetValue(count)
          wx.Yield()
          time.sleep(0.05)  
 
 