# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:57:09 2017

@author: earanda
"""

import wx
import paneltitle
import os


# ----------------------------------------------------------------------------
class MyPanelInit(wx.Panel):
    """
    Panel containing radio buttons for initialization of the algorithm
    """
    def __init__(self,parent,padre,title):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        
        title = paneltitle.MyPanelTitle(self,title)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        # creating a main panel
        panel = wx.Panel(self,style=wx.BORDER_SUNKEN)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        # panel for group buttons (initial type)
        panleft= wx.Panel(panel)
        minisizer = wx.BoxSizer(wx.VERTICAL)
        self.rb1 = wx.RadioButton(panleft,601, label = 'Constant value',style=wx.RB_GROUP) 
        self.rb2 = wx.RadioButton(panleft,602, label = 'Load File') 
        self.rb3 = wx.RadioButton(panleft,603, label = 'Random Density') 
        
        minisizer.Add(self.rb1,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
        minisizer.Add(self.rb2,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
        minisizer.Add(self.rb3,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
        
        sizer.Add(panleft,1, wx.TOP|wx.EXPAND , 1)
        panleft.SetSizer(minisizer)
        
        #  panel for data linked to each type
        panright = wx.Panel(panel)
        minisizer2 = wx.BoxSizer(wx.VERTICAL)
        self.cte = wx.TextCtrl(panright,-1,str(self.padre.values['init_cte']),\
                               style=wx.TE_PROCESS_ENTER|wx.TE_RICH)        
        self.loadfile = wx.StaticText(panright,-1,self.padre.values['init_file'])
        self.rbox = wx.RadioBox(panright, label = ' Seed Initialization ', 
                                choices = ['Same Seed','New Seed'],      
                                majorDimension = 1, style = wx.RA_SPECIFY_ROWS) 

        minisizer2.Add(self.cte,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
        minisizer2.Add(self.loadfile,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
        minisizer2.Add(self.rbox,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)

        sizer.Add(panright,1, wx.TOP|wx.EXPAND , 1)
        panright.SetSizer(minisizer2)

        m_sizer.Add(panel,0, wx.TOP|wx.EXPAND , 1)
        panel.SetSizer(sizer)
        
        self.SetSizer(m_sizer)
        
        # deactivate seed initialization
        self.rbox.Enable(False)
        
        # bindings
        self.Bind(wx.EVT_RADIOBUTTON, self.OnRadiogroup) 
        self.rbox.Bind(wx.EVT_RADIOBOX,self.onRadioBox)
        self.cte.Bind(wx.EVT_TEXT, self.set_val)
        
        
    def OnRadiogroup(self, e): 
        """
        Initialization type
        """
        rb = e.GetEventObject() 
        if (rb.GetId() == 603): # random initialization
            self.rbox.Enable(True)
            self.cte.Enable(False)
            self.padre.values['init_random'] = True
            self.padre.values['init_file'] = ''
        elif (rb.GetId() == 602):  # file initialization
            self.rbox.Enable(False)
            self.padre.values['init_random'] = False
            self.cte.Enable(False)
            a = self.loadInit()
            if a:
                if self.padre.values['init_file']:
                    return
                self.rb1.SetValue(1)  
                self.cte.Enable(True)
                self.padre.values['init_file'] = ''
        else: # constant initialization
            self.cte.Enable(True)
            self.padre.values['init_random'] = False
            self.padre.values['init_file'] = ''
                                        
        self.loadfile.SetLabel(os.path.basename(self.padre.values['init_file']))
            
            
    def onRadioBox(self,e): 
        """
        Seed choice
        """
        if self.rbox.GetSelection():
            self.padre.values['seed'] = True
        else:
            self.padre.values['seed'] = False
            
            
    def loadInit(self):
        """
        Load initialization density
        """
        dlg = wx.FileDialog(self.padre, "Load Solution File", self.padre.mainpath, "", 
                                      "Solution files (*.gf)|*.gf", 
                                      wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            self.padre.values['init_file'] = os.path.relpath(filename,self.padre.mainpath)
        else:
            return 1
        dlg.Destroy()
        
    def set_val(self,event):
        """
        Set constant initialization
        """
        self.padre.values['init_cte'] = event.EventObject.GetValue()
