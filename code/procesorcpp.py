"""
Created on Sat Jul 15 14:04:05 2017

@author: earanda
"""

import dictcode as d

from extfun import is_number,checkempty
import os
from subprocess import Popen
import compilewindow
from importlib import reload
import re

def showmessage(s,t):
    print(s,t)

class Processor(object):
    def __init__(self,padre,compilation,values):
        self.padre = padre
        self.v = values
        self.basepath = self.padre.basepath
        self.mainpath = self.padre.mainpath
        if compilation:
            reload(d)
            self.procesar()    
            if self.v['methodOPT'] == 'MMA':
                prob = 'nlopt'
            elif self.v['methodOPT'] == 'IPOPT':
                prob = 'ipopt'
            elif self.v['methodOPT'] == 'OC':
                prob = 'oc'
            else:
                showmessage("Error","No method?")
                return
            a = self.compilar(prob)           
            if a:    
                showmessage("Error","Compilation Error")
                self.padre.Destroy()
            else:
                print("Compilation done!")
        else:
            print("No compilation is needed")
        self.caller = self.runner()        
  
    def procesar(self):
        """
        Processing values and building c++ files
        """
        maindir = os.path.dirname(os.path.abspath(__file__))
        srcdir = os.path.join(maindir,'../src/')
        
        #--------------------------
        # file constants.hpp
        
        # Plane conditions hypothesis
        if self.v['tensionplana']:
            lamda = 'nu/(1.-nu*nu)' 
            vonmises = 'VonMises2D'
            tipo = '2D'
        else:
            lamda = 'nu/((1+nu)*(1-2*nu))'
            if self.v['dimension'] == 2:
                vonmises = 'VonMises2Ddp'
                tipo = '2Ddp'
            else:
                vonmises = 'VonMises3D'
                tipo = '3D'
        
        if self.padre.settings['suitesparse']:
            suitesparse = ''
        else:
            suitesparse = '#undef MFEM_USE_SUITESPARSE'
            
        if self.v['problem'] == 'Multiload':
            assamblingder = d.codigo['$assamblingder$_ML']
            assamblingb = d.codigo['$assamblingb$_ML']
            initialsystem = '$initialsystem$_ML'
            rhsdef = '$RHSdef$_ML'
            displacements = '$displacements$_ML'
            defderivative = '$defderivative$_ML'
            pesos = self.computepesos(self.v['bd_multi'])
            deletearray = '$deletearray$_ML'
            extras = ''
        else:
            assamblingder = '\tder->Assemble();'
            assamblingb = '\tb->Assemble();'
            initialsystem = '$initialsystem$'
            rhsdef = '$RHSdef$'
            displacements = '$dummy$'
            defderivative = '$defderivative$'
            pesos = ('1','0.')
            deletearray = '$deletearray$'
            extras = ',gridstress,Udisp'
            
        fileconstants = d.codigo['constants'].format(dimension = self.v['dimension'],
                simp = self.v['simp'],
                young = self.v['young'],
                young_void = self.v['young_void'],
                poisson = self.v['poisson'],
                initcte = self.v['init_cte'],
                lamb = lamda,
                qstress = self.v['qstress'],
                pnorm = self.v['Pnorm'],
                multiload_dim = pesos[0],
                multiload_val = pesos[1],
                TOL = self.padre.settings['TOL'],
                ITER = self.padre.settings['ITER'],
                suitesparse = suitesparse)
        
        # write file constants.hpp        
        filec = os.path.join(srcdir,'constants.hpp')
        with open(filec,'w') as fichero:
            fichero.write(fileconstants)
            
       #---------------------------
       # file mainprogram.cpp     

        # analytic functions (headers and definitions)
        
        self.analytic_header = ''
        self.analytic_defs = ''
        
        # dirichlet conditions
        dirichlet = ''
        for y,x in enumerate(self.padre.clamps):
            if self.v['clamped_tick'][x]:
                if checkempty(self.v['clamped'][x]):
                    dirichlet += self.dirichlet(x,y)
                    dirichlet += '\n'
                else:
                    showmessage("Error","Empty Dirichlet entry. Check your data")
                    return
                
        # linear forms
        # inner loads
        linearform = ''
        for y,x in enumerate(self.v['in_loads']):
            if checkempty(''.join(x[:-1])):
                linearform += self.linearforms(x,y,'f','fff')
                linearform += 'b->AddDomainIntegrator(new VectorDomainLFIntegrator(f{y}));\n\t'.format(y=y)
        # boundary loads    
        for y,x in enumerate(self.v['bd_loads']):
            if checkempty(''.join(x[:-1])):
                linearform += self.linearforms(x,y,'g','ggg')
                linearform += 'b->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(g{y}));\n\t'.format(y=y)        
        
        # robin condition
        matrixform = ''        
        for y,x in enumerate(self.v['robin']):
            # load
            if checkempty(''.join(x[1])):
                z = x[1].copy()
                z.append(x[2])
                linearform += self.linearforms(z,y,'h','hhh')
                linearform +=  'b->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(h{y}));\n'.format(y=y)                    
    
            # matrix

            if checkempty(''.join(sum(x[0],[]))):
                matrixform += self.matrixforms(x[0],x[2],y)
                if matrixform:
                    matrixform += 'a->AddBoundaryIntegrator(new VectorMassIntegrator(Mrobin{y}));\n'.format(y=y)
                else:
                    showmessage("Error","Something wrong in Robin conditions")
                    return
            else:
                showmessage("Error","Matrix of Robin conditions empty")
                return
        
        # self-weight hypothesis
        if self.v['density_tick']:
            if not self.v['density']:
                 showmessage("Error","No density in Self-Weight")
                 return
            gravity = 'const double density = -9.81*(' + self.v['density'] + ');\n'
            dim1 = int(self.v['dimension']) - 1
            gravity_force = "\
            VectorArrayCoefficient Density(dim);\n\
            Density.Set({d},new DensityCoeff(*Rhobar,density));\n\
            b->AddDomainIntegrator(new VectorDomainLFIntegrator(Density));\n".format(d=dim1)
            if self.v['betaPROJECTION']:
                var = 'Psip,'
            else:
                var = 'one,'
            gravity_der = "DerDensityCoeff selfweight(*Udisp," + var + "density);\n\
            der->AddDomainIntegrator(new DomainLFIntegrator(selfweight));\n"
            
        else:
            gravity = ''
            gravity_force = ''
            gravity_der = ''
            d.codigo['$density$'] = ""
        
        
        
        # multiload case
        
        if self.v['problem'] == 'Multiload':
            linearform = self.multiload(self.v['bd_multi'],linearform)
        
        # initialization
        
        if self.v['methodOPT'] == "OC":
            initialgrid = "GridFunction &initialization = *rho;\n"
        else:
           initialgrid = "GridFunction initialization(p0espace);\n"
        
        if self.v['init_file']:
            if self.v['methodOPT'] == "OC":
                rutainit = os.path.normpath(os.path.join(self.mainpath,self.v['init_file']))
                initialization = "ifstream initfile(\"{name}\");\n\
                GridFunction initialxx(mesh,initfile);\n\
                GridFunction &initialization = *rho;\n\
                initialization = initialxx;\n".format(name=rutainit)    
            else:
                rutainit = os.path.normpath(os.path.join(self.mainpath,self.v['init_file']))
                initialization = "ifstream initfile(\"{name}\");\n\
                GridFunction initialization(mesh,initfile);\n".format(name=rutainit)                
        elif self.v['init_random']:
            initialization = initialgrid + "\
            const gsl_rng_type * Trandom;\n\
            gsl_rng * Rrandom;\n\
            gsl_rng_env_setup();\n\
            Trandom = gsl_rng_default;\n\
            Rrandom = gsl_rng_alloc (Trandom);\n\
            for (int i=0; i<ndim; i++)\n\
                initialization(i) = gsl_ran_beta(Rrandom, Initcte, 1-Initcte);\n\
            gsl_rng_free (Rrandom);\n"
        else:
            initialization = initialgrid + "\
            initialization = Initcte;\n"
            
        # passive zone
        passive = 'GridFunction xx(p0espace);\n'
        if self.v['passive_tick']:
            if checkempty(self.v['passive']):
                passive += "\
                Vector passive(mesh->attributes.Max());\n\
                passive = 0.0;\n"
                for x in self.v['passive'].split():
                    passive += 'passive(' + str(int(x)-1) + ') = 1.0;\n'
                passive += "\
                PWConstCoefficient passive_func(passive);\n\
                xx.ProjectCoefficient(passive_func);\n"
            else:
                showmessage("Error","No label in passive zone")
                return
        else:
            passive += 'xx = 0.;\n'
        
        
        # Mechanism and Stress problems
        if self.v['problem'] == 'Mechanism':
            # objective function for mechanism
            mechanism = "\
            LinearForm *c;\n\
            c = new LinearForm(p1espace);\n"
            for y,x in enumerate(self.v['obj_mechanism']):
                if checkempty(''.join(x[:-1])):
                    mechanism += self.linearforms(x,y,'mecobj','mecs')
                    mechanism += 'c->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(mecobj{y}));\n'.format(y=y)        
                else:
                    showmessage("Error","Empty mechanism objective")
                    return
            mechanism += "c->Assemble();\n"
            
            initcompliance = "double compliance = ((Vector) *c) * U;\n\
            RelaxStress *stressr;\n\
            stressr = new RelaxStress(*Rhobar,*sigmavm);\n\
            cout << \"Initial Displacement: \" << compliance << endl;\n"
           
            qdisp = "GridFunction *Qdisp;\n\
            Qdisp = new GridFunction(p1espace);\n\
            *Qdisp = 0.0;" 
            adjrhs = ''
           
            if self.v['betaPROJECTION']:
                derivativemec = "CoeffDerivativeAdj derivative(*Rhobar,*Udisp,*Qdisp,Psip);\n"
            else:
                derivativemec = "CoeffDerivativeAdj derivative(*Rhobar,*Udisp,*Qdisp,one);\n"
            
            d.codigo['$problem$'] = 'ToptimizNLOPTMec'
        elif self.v['problem'] == 'Stress':
            mechanism = d.codigo['$stress$']
            
            initcompliance = 'double rcompliance = ((Vector) *b) * ((Vector) *Udisp);\n\
            RelaxStress *stressr;\n\
            stressr = new RelaxStress(*Rhobar,*sigmavm);\n\
            double compliance = ComputeLpNorm(PNORM, *stressr, *mesh, irs);\n\
            cout << "Initial Compliance: " << rcompliance << endl;\n\
            cout << "Initial Pnorm Stress: " << compliance << endl;\n\n'
            
            qdisp = "GridFunction *Qdisp;\n\
            Qdisp = new GridFunction(p1espace);\n\
            *Qdisp = 0.0;" 
            adjrhs = d.codigo['$adjrhs$'].format(tipo=tipo)
            
            if self.v['betaPROJECTION']:
                derivativemec = "CoeffDerivativeAdj derivative(*Rhobar,*Udisp,*Qdisp,Psip);\n\
                CoeffDerivativeStress otherder(*Rhobar,*sigmavm,Psip,Mobj);\n"
            else:
                derivativemec = "CoeffDerivativeAdj derivative(*Rhobar,*Udisp,*Qdisp,one);\n\
            CoeffDerivativeStress otherder(*Rhobar,*sigmavm,one,Mobj);\n"
        else:
             mechanism = ''
             initcompliance = "double compliance = ((Vector) *b) * U;\n\
             RelaxStress *stressr;\n\
             stressr = new RelaxStress(*Rhobar,*sigmavm);\n"
             qdisp = ''
             
             if self.v['betaPROJECTION']:
                 derivativemec =  "\tCoeffDerivative derivative(*Rhobar,*Udisp,Psip);\n"    
             else:
                 derivativemec = "\tCoeffDerivative derivative(*Rhobar,*Udisp,one);\n"
             
             adjrhs = ''

             
        if self.v['problem'] == 'Volume':
            projectRhobar = 'Rhobar->ProjectCoefficient(one);\n'
            initcompliance += "cout << \"Best Compliance: \" << compliance << endl;\n"
            mobjs = "double Mobj = Volfrac/compliance;\n"        
            volconstant = "double Volconstant = 1./medida;\n"
            d.codigo['$funcion$'] = 'myfuncVol'
            d.codigo['$restriccion$'] = 'myconstraintVol'
            dthread = '$thresholding$_vol'
        else:
            projectRhobar = 'Rhobar->ProjectCoefficient(volfrac);\n'
            mobjs = "double Mobj = abs(1./compliance);\n"
            d.codigo['$funcion$'] = 'myfunc'
            d.codigo['$restriccion$'] = 'myconstraint'
            dthread = '$thresholding$'
            if self.v['methodOPT'] == "OC":
                volconstant = "double Volconstant = Volfrac*medida;\n"
            else:
                volconstant = "double Volconstant = 1./(Volfrac * medida);\n" 
                
        if self.v['problem'] == 'Stress':
            projectRhobar = 'Rhobar->ProjectCoefficient(one);\n'
            mobjs = "double Mobj = 1.;\n"
            volconstant = "double Volconstant = 1./medida;\n"
            addtder = 'der->AddDomainIntegrator(new DomainLFIntegrator(otherder));\n'
            dthread = '$thresholding$_vol'
        else:
            addtder = ''        
            
        if self.v['problem'] == "Compliance":
            initcompliance += "cout << \"Initial Compliance: \" << compliance << endl;\n"
            
        if self.v['problem'] == "Multiload":
            initcompliance = "\tRelaxStress *stressr;\n\
            \tstressr = new RelaxStress(*Rhobar,*sigmavm);\n\
            \tcout << \"Initial Compliance: \" << compliance << endl;\n"
            derivativemec = ''
            
        if self.v['methodFILTER'] == "Conic":
            if self.v['betaPROJECTION']:
                namefilter = "ConicFilter(mesh,radius,*Rhobar,beta,eta)"
            else:
                namefilter = "ConicFilter(mesh,radius,*Rhobar)"
        else:
            if self.v['betaPROJECTION']: 
                namefilter = "HelmholtzFilter(mesh,radius,*Rhobar,fec,fec0,beta,eta)" 
            else:
                namefilter = "HelmholtzFilter(mesh,radius,*Rhobar,fec,fec0)"
            
        yvol = "*y = Volconstant;"    
        
        
        # Thresholding
        thres1 = os.path.join(self.basepath,self.v['namefile'] + '-threshold.vtk');
        thres2 = os.path.join(self.basepath,self.v['namefile'] + '-threshold.vtk.table');
        
        threshold = d.codigo[dthread].format(name1=thres1,name2=thres2)
        
        
        # process each method    
            
        if self.v['methodOPT'] == "MMA":
            suffix = "_NLOPT"
            metodo = "result = opt.optimize(x, minf);\n"
            if self.v['problem'] == "Mechanism":
                clase = "ToptimizNLOPTMec obj(mesh,Rhobar,Udisp,Qdisp,y,a,b,c,\n\
                der,vol,dconres,Mobj,Volconstant,ess_tdof_list,rho,visualization"
                problem = "ToptimizNLOPTMec*"
            elif self.v['problem'] == "Stress":
                clase = "ToptimizNLOPTStr obj(mesh,Rhobar,Udisp,Qdisp,y,a,b,\n\
                der,vol,dconres,adjrhs,irs,Mobj,Volconstant,ess_tdof_list,Volfrac,rho,\n\
                visualization,umbral,gridstress,stressr";
                problem = "ToptimizNLOPTStr*"
            elif self.v['problem'] == "Multiload":
                clase = "ToptimizNLOPTML obj(mesh,Rhobar,Vdisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,visualization"
                problem = "ToptimizNLOPTML*"
            else:
                clase = "ToptimizNLOPT obj(mesh,Rhobar,Udisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,visualization"
                problem = "ToptimizNLOPT*"
        else:
            problem = ''
                                    
                                        
        if self.v['methodOPT'] == "IPOPT":
            suffix = "_IPOPT"
            metodo = "status = app->OptimizeTNLP(mynlp);\n"
            if self.v['problem'] == "Mechanism":
                clase = "ToptimizIPOPTMec(mesh,Rhobar,Udisp,Qdisp,y,a,b,c,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,lb,visualization"
            elif self.v['problem'] == "Compliance":
                clase = "ToptimizIPOPT(mesh,Rhobar,Udisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,lb,visualization"
            elif self.v['problem'] == "Stress":
                clase = "ToptimizIPOPTStr(mesh,Rhobar,Udisp,Qdisp,y,a,b,der,vol,dconres,\n\
                adjrhs,irs,Mobj,Volconstant,ess_tdof_list,Volfrac,rho,lb,visualization,\
                umbral,gridstress,stressr"
            elif self.v['problem'] == 'Multiload':
                clase = "ToptimizIPOPTML(mesh,Rhobar,Vdisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,lb,visualization"
            else:
                clase = "ToptimizIPOPTVol(mesh,Rhobar,Udisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,lb,visualization"

                    
        if self.v['methodOPT'] == "OC":
            suffix = "_OC"
            metodo = "obj.OCiteration(maxiter,tol);\n"
            yvol = ''
            if self.v['problem'] == "Compliance":
                clase = "ToptimizOC obj(mesh,Rhobar,Udisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,visualization,zeta,neta"
            elif self.v['problem'] == "Multiload":
                clase = "ToptimizOCML obj(mesh,Rhobar,Vdisp,y,a,b,der,vol,dconres,\n\
                Mobj,Volconstant,ess_tdof_list,rho,visualization,zeta,neta"
                
        
        namevtk = os.path.join(self.basepath,self.v['namefile']+'.vtk')    
                
        # --------------------------------------------------------        
        # processing files
        filetoprocess = "func" + suffix + '.cpp'
        if self.v['methodOPT'] == "IPOPT":
            namesch = ["$filtering$","$vectorD$","$vectorVolD$","$extra$"]
            namessh = ["$density$","$solpure$","$solfilter$","$solvtk$"]
        elif self.v['methodOPT'] == "MMA":
            namesch = ["$filtering$","$vectorD_NLOPT$","$vectorVolD_NLOPT$","$extra$"]
            namessh = ["$density$","$solpure$","$solfilter$","$problem$","$funcion$","$restriccion$"]
        elif self.v['methodOPT'] == "OC":
            namesch = ["$filtering$","$vectorD_OC$","$extra$"]
            namessh = ["$density$"]
        else:
            showmessage("Error","There is no method")
            return

        rnamesch = []
        if self.v['betaPROJECTION']:
            clase +=',Rhohat,beta,eta);\n'
            for x in namesch:
                y = x + '_WH'
                rnamesch.append(y)
        else:
            rnamesch = namesch[:]
            clase += ');\n'
        
        d.codigo['$solpure$'] = os.path.join(self.basepath,self.v['resultadopure'])
        d.codigo['$solfilter$'] = os.path.join(self.basepath,self.v['resultadofilter'])
        d.codigo['$solvtk$'] = namevtk
        d.codigo['$problem$'] = problem
        
        # create funMETHOD        
        archivo = os.path.join(srcdir,filetoprocess)

        with open(archivo,'r') as myfile:
            content = myfile.read()
        for x in namessh:
            newcontent = content.replace(x,d.codigo[x])
            content = newcontent
        for x,y in zip(namesch,rnamesch):
            newcontent = content.replace(x,d.codigo[y])
            content = newcontent            
        namef = 'FUN' + suffix + '.cpp'
        newarchivo = os.path.join(srcdir,namef)
        with open(newarchivo,'w') as myfile:
            myfile.write(content)
            
        # process main.cpp
        marchivo = os.path.join(srcdir,'mainprogram.cpp')
        with open(marchivo,'r') as myfile:
            content = myfile.read()
        
        namesfch = ["$initialfilter$","$dconres$","$bucleHeaviside$","$delete$"]
        namesfwm = ["$include$","$method$","$finalsol$","$savesol$","$after$"]
        if self.v['betaPROJECTION']:
            rnamesfch = []
            for x in namesfch:
                y = x + '_WH'
                rnamesfch.append(y)
        else:
            rnamesfch = namesfch[:]
        rnamesfwm = []    
        for x in namesfwm:
            y = x + suffix
            rnamesfwm.append(y)
        
        if self.v['methodOPT'] == "MMA":
            if self.v['problem'] == 'Volume': 
                rnamesfwm[2] += '_Vol'
            elif self.v['problem'] == 'Stress':
                rnamesfwm[2] += '_Str'
            elif self.v['problem'] == 'Multiload':
                rnamesfwm[2] += '_ML'     
        if self.v['methodOPT'] == "OC" and self.v['problem'] == 'Multiload':
                rnamesfwm[2] += '_ML'        
        
        
        mesh = os.path.abspath(os.path.normpath((os.path.join(self.mainpath,self.v['path'],self.v['meshfile']))))
       
        
        # replacement from dictcode
        d.codigo[rnamesfch[0]] = d.codigo[rnamesfch[0]].format(namefilter=namefilter,derivativemech=derivativemec)
        d.codigo[rnamesfwm[1]] = d.codigo[rnamesfwm[1]].format(clase=clase)
        d.codigo[rnamesfch[2]] = d.codigo[rnamesfch[2]].format(metodo=metodo)
        d.codigo[rnamesfwm[2]] = d.codigo[rnamesfwm[2]].format(filtering=d.codigo[rnamesch[0]])
        d.codigo[rnamesfwm[3]] = d.codigo[rnamesfwm[3]].format(solepure=d.codigo['$solpure$'],
                solfilter=d.codigo['$solfilter$'],solvtk=namevtk,extras=extras)
        
        for x,y in zip(namesfch + namesfwm,rnamesfch + rnamesfwm):
            newcontent = content.replace(x,d.codigo[y])
            content = newcontent
        
        
        # replacement from procesor
        replacers = {'$mesh$': mesh, '$gravity$': gravity,
                     '$displacements$' : d.codigo[displacements],
                     '$density$': d.codigo['$density$'], 
                     '$dirichlet$': dirichlet,
                     '$RHSdef$': d.codigo[rhsdef],
                     '$assamblingb$' : assamblingb,
                     '$assamblingder$' : assamblingder,                     
                     '$linearform$': linearform, '$gravity_force$': gravity_force,
                     '$mechanism$': mechanism, '$matrixform$': matrixform, 
                     '$initialsystem$' : d.codigo[initialsystem],
                     '$defderivative$' : d.codigo[defderivative],
                     '$projectRhobar$': projectRhobar, '$initcompliance$': initcompliance,
                     '$mobjs$': mobjs, '$volconstant$': volconstant, '$qdisp$': qdisp,
                     '$gravity_der$': gravity_der, '$passive$': passive, 
                     '$initialization$': initialization, '$yvol$': yvol,
                     '$headers$': self.analytic_header,
                     '$analyticfuns$': self.analytic_defs,
                     '$threshold$': threshold,
                     '$adjrhs$': adjrhs,
                     '$additionalder$': addtder,
                     '$deletearray$' : d.codigo[deletearray],
                     '$VonMises$':vonmises}
        
        for x in replacers.keys():
            newcontent = content.replace(x,replacers[x])
            content = newcontent
         
        mainprog = os.path.join(srcdir,'MAIN.cpp')
        
        with open(mainprog,'w') as myfile:
            myfile.write(content)
        
        
        
        
        
        
    def dirichlet(self,x,y):
        cadena = self.labels(self.v['clamped'][x])
        cadena += 'p1espace->GetEssentialTrueDofs(ess_bdr, etiquetas,{});\n\t'.format(y)
        cadena += 'ess_tdof_list.Append(etiquetas);\n\t'
        return cadena
        
        
    def labels(self,string,bd='_bdr'):
        namearray = 'ess' + bd
        cadena = '\t' + namearray + ' = 0;\n\t'   
        for x in string.split():
            cadena += namearray +'[' + str(int(x)-1) +'] = 1;\n\t' 
        return cadena
    
    def linearforms(self,x,y,namef,namep):
        if x[3]:
            etiquetas = self.labels(x[3])
            res = True
        else:
            etiquetas = '\n\t'
            res = False
        cadena = etiquetas    
        cadena += 'VectorArrayCoefficient ' + namef + '{}(dim);\n\t'.format(y)
        for i,z in enumerate(x[:-1]):
            if checkempty(z):                
                if is_number(z):
                    cadena += 'ConstantCoefficient ' + namep + '{y}{i}({cte});\n\t'.format(i=i,cte=float(z),y=y)
                else:
                    cadena += 'FunctionCoefficient ' + namep + '{y}{i}(func'.format(i=i,y=y) + namep + 'per{y}{i});\n\t'.format(i=i,y=y)
                    self.analytic_definition('func'+ namep + 'per{y}{i}'.format(i=i,y=y),z)
                if res:
                    cadena += namef + '{comp}.Set({i},new RestrictedCoefficient('.format(i=i,comp=y) + namep + '{comp}{i},ess_bdr));\n\t'.format(i=i,comp=y)        
                else:
                    cadena += namef + '{comp}.Set({i},&'.format(i=i,comp=y) + namep + '{comp}{i});\n\t'.format(i=i,comp=y)
        return cadena
    
    
    def matrixforms(self,matrix,label,ind):
        if checkempty(label):
            etiquetas = self.labels(label)
        else:
            showmessage("Error","Label in Robin condition is empty. Check your data")
            return ''
        cadena = 'MatrixArrayCoefficient Mrobin{}(dim);\n'.format(ind)
        cadena += etiquetas
        
        for i,row in enumerate(matrix):
            for j,col in enumerate(row):
                if checkempty(col):
                    if is_number(col):
                        cadena += 'ConstantCoefficient m{ind}{i}{j}({cte});\n'.format(i=i,
                                                                j=j,ind=ind,cte=float(col))
                        cadena += 'Mrobin{ind}.Set({i},{j},new RestrictedCoefficient(m{ind}{i}{j},ess_bdr));\n'.format(ind=ind,i=i,j=j)
                    else:
                        showmessage("Error","Matrix of Robin must be constant. Check your data")
                        return ''
        return cadena
            

    def analytic_definition(self,name,funcion):
        
        self.analytic_header += 'double ' + name + '(const Vector &x);\n'
        self.analytic_defs += 'double ' + name + '(const Vector &x)\n\
        {\nreturn ' + funcion + ';\n}'  
        

    def computepesos(self,val):
        if len(val)<=1:
            showmessage("Error","You have choose Multiload problem but there is not enough loads. Check your data")
            return '',''
        else:
            dim = len(val)
            weights = [x[-1] for x in val]
            if abs(sum([float(x) for x in weights]) - 1.0) > 1.e-2:
                showmessage("Error","Your weights does not sum 1.Check your data")
                return '',''
            coefs = ', '.join(weights)
            return (dim,coefs)
        
        
    def multiload(self,val,linearform):
        mylinearform = linearform.split('\n')
        if len(val) < 2:
            showmessage("Error","You have choose Multiload problem but there is not enough loads. Check your data")
            return ''
        newlinearform = ''
        aplies = ''
        for line in mylinearform:
            if not re.search('Integrator',line):
                newlinearform += line + '\n'
            else:
                aplies += line + '\n'
                
        
        
        for y,x in enumerate(val):
            if checkempty(''.join(x[:-2])):
                newlinearform += self.linearforms(x[:-1],y,'k','kkk')
                newlinearform += 'b[{y}]->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(k{y}));\n'.format(y=y)
                newlinearform += aplies.replace('b->','b[{y}]->'.format(y=y))
            else:
                showmessage("Error","There are no loads for Multiload problem")
                return ''
        return newlinearform
            
    
    def compilar(self,problem):
        myfile = os.path.normpath(os.path.join(self.padre.mainpath,'src/makefile'))
        if self.v['methodOPT'] == 'IPOPT':
            link = '-lipopt'
        elif self.v['methodOPT'] == 'MMA':
            link = '-lnlopt'
        else:
            link = ''
        
        if self.v['init_random']:
            link += ' -lgsl'

        if link:
            lib = 'LIBRARY='+link
            res = ["make","-f",myfile,lib,problem] 
        else:
            res = ["make","-f",myfile,problem] 
        # Compiling in second plane
        r = Popen(res)
        # Show a progress window
        newframe = compilewindow.Progress("Compiling...")
        r.communicate()[0]   
        newframe.Destroy()
        return r.returncode
    

    def runner(self):
        myfile = os.path.normpath(os.path.join(self.padre.mainpath,'bin/program'))
        if self.v['problem'] == 'Volume':
            volfrac = self.v['compliance']
        elif self.v['problem'] == 'Stress':
            volfrac = self.v['maxstress']
        else:
            volfrac = self.v['volfrac']
            
        if self.v['methodOPT'] == 'OC':
            addoption = [' -z ',self.v['zeta'],' -n ',self.v['eta']]
        else:
            addoption = []
        options = [' -r ',self.v['filter'],' -v ',volfrac,' -i ',self.v['maxiter'],
                   ' -t ',self.v['tol'],' -l ',self.v['betaloop'],' -bup ',self.v['betaup'],
                   ' -u ',self.v['umbral']]
        if self.padre.settings['visualization']:
            options[0:0] = ' -vis '
        else:
            options[0:0]= ' -no-vis '
        options.extend(addoption)
        caller = [myfile]
        caller.extend(options)
        return caller    
