#!/usr/bin/env python3

"""
Created on July 2017
 
@author: Ernesto Aranda
 
Graphic front end for elaborating FreeFem++ code for 
topology optimization 
"""
import wx
# my panels
import panelvtk
import panelentry
import panellogo
import panelcheck
import panelload
import panelmultiload
import panelterminal
import panelrobin
import paneltitle
import panelgraphbot
import panelsettings
import paneladapt
import panelinit
import menus
# other 
import os
import sys
# extra code
import procesorcpp
import dictdiffer
import copy
import subprocess

        
# ---------------------------------------------------------------------------   
class MainFrame(wx.Frame):
    """
    Main Window with two parts: panels with information and a panel with VTK window
    """
    def __init__(self,parent,title,file=None):
        wx.Frame.__init__(self,parent,title=title,size=(1200,800))
        
        self.values = {}
        self.backvalues = {} # backup values
        
        self.settings = {}
        self.settings['ITER'] = '5000'
        self.settings['TOL'] = '1.e-22'
        self.settings['visualization'] = True
        self.settings['suitesparse'] = True
        self.backsettings = {}
        self.opciones = ''
        self.clamps = ('X','Y','Z')
        self.mainpath = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
        self.basepath = os.path.join(os.path.expanduser('~'),'.toptimiz3D')
        self.isprocess = False
        self.isrun = False

        # create base directory
        if not os.path.exists(self.basepath):
            try:
                os.makedirs(self.basepath)
            except:
                print("Fatal error: there is no base directory to write")
                exit()
        
        # initialization of values
        self.initvalues()

        # list to manage panels
        self.listofpanels = []
        
        # splitting windows
        self.totalpanel = wx.SplitterWindow(self)
        self.leftpanel = wx.SplitterWindow(self.totalpanel)
        self.rightpanel = wx.SplitterWindow(self.totalpanel)
        self.totalpanel.SplitVertically(self.leftpanel,self.rightpanel,425)

        ################################
        # Panels of left window
        
        # Elastic constants
        self.windparam = panelentry.MyPanelEntry(self.leftpanel,self,["Young's modulus of material","Young's modulus of void","Poisson's ratio"],\
                                      ["young","young_void","poisson"],"Elastic Constants")
        self.listofpanels.append(self.windparam)
        
        # Optimization parameters
        self.optparam = panelentry.MyPanelEntry(self.leftpanel,self,["Volume fraction","Stiffness fraction","Max. Stress",\
                                                      "Filter radius","Tolerance","Max. Iterations","SIMP parameter",\
                                                      "Heaviside parameter update","Heaviside projection loops"],
                                     ["volfrac","compliance","maxstress","filter","tol","maxiter","simp","betaup","betaloop"],\
                                     "Optimization's Parameters")
        self.listofpanels.append(self.optparam)        
        
        # By default, compliance fraction is inactive
        self.optparam.paneles[1].Enable(False)        
        
        # Parameters for optimality conditions
        self.optimalparam = panelentry.MyPanelEntry(self.leftpanel,self,["Eta","Zeta"],["eta","zeta"],\
                                         "Parameters for Optimality Conditions")
        self.listofpanels.append(self.optimalparam)        

        # Panel for clamped condition
        self.clampedpanel = panelcheck.MyPanelCheck(self.leftpanel,self,"Clamped Conditions",self.clamps,'clamped')
        self.listofpanels.append(self.clampedpanel)
        
        # Panel for boundary loads
        self.bdloadspanel = panelload.MyPanelLoads(self.leftpanel,self,"Boundary Loads",'bd_loads')
        self.listofpanels.append(self.bdloadspanel)
        
        # Panel for inner loads
        self.inloadpanel = panelload.MyPanelLoads(self.leftpanel,self,"Body Loads",'in_loads')
        self.listofpanels.append(self.inloadpanel)
        
        # Panel for Passive Zone
        self.passivepanel = panelcheck.MyPanelCheckSimple(self.leftpanel,self,"Passive Zones","Passive Zones"," Labels",'passive')
        self.listofpanels.append(self.passivepanel)
        
        # Panel for Self-Weight
        self.densitypanel = panelcheck.MyPanelCheckSimple(self.leftpanel,self,"Self-Weight","Self-Weight"," Density",'density')
        self.listofpanels.append(self.densitypanel)

        # Panel for Robin conditions
        self.robinpanel = panelrobin.MyPanelRobin(self.leftpanel,self,"Robin's Conditions",'robin')
        self.listofpanels.append(self.robinpanel)
        
        # Panel for displacement objective
        self.mechanism = panelload.MyPanelLoads(self.leftpanel,self,"Mechanism Objective",'obj_mechanism')
        self.listofpanels.append(self.mechanism)

        # Panel for initial density
        self.initpanel = panelinit.MyPanelInit(self.leftpanel,self,"Initial Density")
        self.listofpanels.append(self.initpanel)
    
        
        # Panel for adaptation process
        self.adaptpanel = paneladapt.MyPanelAdapt(self.leftpanel,self,"Mesh Adaptation")
        self.listofpanels.append(self.adaptpanel)
                

        ##############################
        
        # Panel simulating terminal process
        self.outputpanel = panelterminal.MyPanelTerminal(self.leftpanel,self,"Results")
        self.listofpanels.append(self.outputpanel)


        # Panel for main settings
        self.settingspanel = panelsettings.PanelSettings(self.leftpanel,self,"General Settings")
        self.listofpanels.append(self.settingspanel)
        
        
        # Parameters for stress problem
        self.stressparam = panelentry.MyPanelEntry(self.leftpanel,\
                           self,["Stress P-norm","Stress penalization","Threshold iterations"],\
                           ["Pnorm","qstress","umbral"],"Parameters for Stress Problem")
        self.listofpanels.append(self.stressparam)        
        
        # Panel for multiload case
        self.multiload = panelmultiload.MyPanelMultiLoads(self.leftpanel,self,"Boundary Multi Loads",'bd_multi')
        self.listofpanels.append(self.multiload)
        
        self.settingspanel.vis.SetValue(True)   
        self.settingspanel.suite.SetValue(True)   
        self.settingspanel.OnTicksuite(None)
                
        # hide panels
        for x in self.listofpanels:
            x.Hide()

        # logo panel at bottom
        self.logopanel = panellogo.MyPanelLogo(self.leftpanel)
	
        # set active panel to use replacewindow method
        self.active = paneltitle.MyPanelTitle(self.leftpanel,"Toptimiz3D")

        # splitting leftpanel
        self.leftpanel.SplitHorizontally(self.active,self.logopanel,560)

        ######################################

        # Panels in right window:
        self.graphicbot = panelgraphbot.MyPanelGraphButton(self.rightpanel,self)
        self.graphicmesh = panelvtk.MiPanelVtk(self.rightpanel,self)
        
        # splitting righpanel
        self.rightpanel.SplitHorizontally(self.graphicmesh,self.graphicbot,600)
         
        # menus
        self.menus = menus.AppMenu(self)
        
        # initialize dimension to block some elements of panels
        self.menus.setDim(None,self.values['dimension'])
        
        # block some panels
        self.Validating()
        if file:
            self.menus.loadFile(None,file)
        
                    
    def initvalues(self):
        """
        Initialization of values
        """
        self.values['young'] = '1.0'
        self.values['young_void'] = '1.e-9'
        self.values['poisson'] = '0.3'
        self.values['volfrac'] = '0.4'
        self.values['filter'] = '0.08'
        self.values['simp'] = '3'
        self.values['tol'] = '1.e-8'
        self.values['maxiter'] = '100'
        self.values['betaup'] = '2'
        self.values['betaloop'] = '3'
        
        # optimality conditions parameters
        self.values['eta'] = '0.5'
        self.values['zeta'] = '0.2'

        # stress parameters
        self.values['qstress'] = '0.5'
        self.values['Pnorm'] = '8'
        
        # Fixed Displacement
        self.values['clamped'] = {}
        self.values['clamped_tick'] = {}
        for x in self.clamps:
            self.values['clamped'][x] = ''
            self.values['clamped_tick'][x] = False      
        # inner and boundary loads
        self.values['bd_loads'] = [['','','','']]
        self.values['in_loads'] = [['','','','']]
    
        # boundary multiloads 
        self.values['bd_multi'] = [['','','','','']]
        # Robin conditions
        self.values['robin'] = []
        # hypothesis on plain stress
        self.values['tensionplana'] = True
        # Passive zone
        self.values['passive'] = ''
        self.values['passive_tick'] = False
        # Self-weight
        self.values['density'] = ''
        self.values['density_tick'] = False
        # Optimization method and filter type        
        self.values['methodOPT'] = 'MMA'
        self.values['methodFILTER'] = 'Conic'
        # Beta projection
        self.values['betaPROJECTION'] = False
        # dimension        
        self.values['dimension'] = 2
        # mesh
        self.values['meshfile'] = '' # name of the mesh file (no path!)
        self.values['namefile'] = '' # name of the mesh file (without extension)
        self.values['path'] = ''# relative path of meshfile (respect to mainpath) 
        
        # names of file solutions                    
        self.values['resultadopure'] = 'resultado-rho.gf'
        self.values['resultadofilter'] = 'resultado-rhobar.gf'
        
        # problem
        self.values['problem'] = "Compliance"
        # objective for mechanism
        self.values['obj_mechanism'] = [['','','','']]
        
        # compliance fraction in Volume problem
        self.values['compliance'] = '0.5'      
        
        # maximum stress for Stress problem
        self.values['maxstress'] = '1.0'
        self.values['umbral'] = '50'
        
        # initialization constant, file and random
        self.values['init_cte'] = self.values['volfrac']
        self.values['init_file'] = ''        
        self.values['init_random'] = False
        self.values['seed'] = False

        # values for adaptation        
        self.values['adapt_filters'] = ''
        self.values['adapt_iters'] = ''
        self.values['adapt_nbvx'] = '2'
           
    def updating(self):
        """
        Updating values after reading a configuration file
        """
        # Put processing and run variable False when load new data
        self.isprocess = False
        self.isrun = False
        
        # hack to presever init_cte
        initcte = self.values['init_cte']
        
        # put values in parameters panels
        self.windparam.static_e[0].SetValue(self.values['young'])
        self.windparam.static_e[1].SetValue(self.values['young_void'])
        self.windparam.static_e[2].SetValue(self.values['poisson'])
        
        self.optparam.static_e[0].SetValue(self.values['volfrac'])
        self.optparam.static_e[1].SetValue(self.values['compliance'])
        self.optparam.static_e[2].SetValue(self.values['maxstress'])
        self.optparam.static_e[3].SetValue(self.values['filter'])
        self.optparam.static_e[4].SetValue(self.values['tol'])
        self.optparam.static_e[5].SetValue(self.values['maxiter'])
        self.optparam.static_e[6].SetValue(self.values['simp'])
        self.optparam.static_e[7].SetValue(self.values['betaup'])
        self.optparam.static_e[8].SetValue(self.values['betaloop'])
        
        self.optimalparam.static_e[0].SetValue(self.values['eta'])
        self.optimalparam.static_e[1].SetValue(self.values['zeta'])

        self.stressparam.static_e[0].SetValue(self.values['Pnorm'])
        self.stressparam.static_e[1].SetValue(self.values['qstress'])
        self.stressparam.static_e[2].SetValue(self.values['umbral'])
        
#       put values in adaptation panel        
        self.adaptpanel.panelent.static_e[0].SetValue(self.values['adapt_filters'])
        self.adaptpanel.panelent.static_e[1].SetValue(self.values['adapt_iters'])
        self.adaptpanel.panelent.static_e[2].SetValue(self.values['adapt_nbvx'])        

        # updating clamped conditions
        for x in self.clamps:
            if self.values['clamped'][x]:
                self.clampedpanel.labels[x].SetValue(self.values['clamped'][x])
                self.clampedpanel.labels[x].Enable(True)
                self.clampedpanel.tickers[x].SetValue(self.values['clamped_tick'][x])
            else:
                self.clampedpanel.labels[x].SetValue('')
                self.clampedpanel.labels[x].Enable(False)
                self.clampedpanel.tickers[x].SetValue(False)
                
        # updating loads   
        for panel,val in zip([self.bdloadspanel,self.inloadpanel,self.mechanism],\
                             ['bd_loads','in_loads','obj_mechanism']):
            panel.grid.ClearGrid()
            old = panel.grid.GetNumberRows() 
            new = len(self.values[val])
            if new == 0:
                panel.grid.DeleteRows(0,1,True)
                for j in range(4):
                    panel.grid.SetCellValuew(0,j,'')
            else:
                if new < old:
                    panel.grid.DeleteRows(0,old-new,True)
                if new > old:
                    panel.grid.AppendRows(new-old)
                    # put data in grid
                for i in range(len(self.values[val])):
                    for j in range(4):
                        panel.grid.SetCellValue(i, j,self.values[val][i][j])        
                        
        # updating Robin's
        new = len(self.values['robin']) - self.robinpanel.i
        if new < 0:
            for i in range(-new):
                self.values['robin'].append([])
                self.robinpanel.delrow(None,-1)
        if new > 0:
            for i in range(new):
                self.robinpanel.addrow(None)
                self.values['robin'].pop()
        for i in range(len(self.values['robin'])):
            for row in range(3):
                self.robinpanel.force[i].SetCellValue(row, 0,\
                self.values['robin'][i][1][row])
                for col in range(3):
                    self.robinpanel.matrix[i].SetCellValue(row,col,\
                    self.values['robin'][i][0][row][col])
            self.robinpanel.label[i].SetValue(self.values['robin'][i][2])
            self.robinpanel.label[i].SetForegroundColour(wx.BLACK)
            
            
        # updating Multiload's
        self.multiload.grid.ClearGrid()
        old = self.multiload.grid.GetNumberRows() 
        new = len(self.values['bd_multi'])
        if new == 0:
            self.multiload.grid.DeleteRows(0,1,True)
            for j in range(5):
                self.multiload.grid.SetCellValue(0,j,'')
        else:
            if new < old:
                self.multiload.grid.DeleteRows(0,old-new,True)
            if new > old:
                self.multiload.grid.AppendRows(new-old)
                # put data in grid
            for i in range(len(self.values['bd_multi'])):
                for j in range(5):
                    self.multiload.grid.SetCellValue(i, j,self.values['bd_multi'][i][j])        

        

        # updating passive zone and self weight
        for x,y in zip([self.passivepanel,self.densitypanel],['passive','density']):
            if self.values[y+'_tick']:
                x.tickers.SetValue(True)
                x.labels.Enable(True)
                x.labels.SetValue(str(self.values[y]))
                x.labels.SetForegroundColour(wx.BLACK)
            else:
                x.tickers.SetValue(False)
                x.labels.SetValue('')
                x.labels.Enable(False)
                
        # updating mesh
        if self.values['meshfile']:
            filemesh = os.path.normpath(os.path.join(self.mainpath,self.values['path'],self.values['meshfile']))
        else:
            filemesh = ''
                    
        if filemesh:            
            self.graphicbot.Plot_Mesh(filemesh)     
                        
            
        # updating dimension
        self.menus.setDim(None,self.values['dimension'])
            
        # updating method
        if self.values['methodOPT'] == 'MMA':
            self.menus.m_meth1.Check(True)
        elif self.values['methodOPT'] == 'OC':
            self.menus.m_meth2.Check(True)
        elif self.values['methodOPT'] == 'IPOPT':
            self.menus.m_meth3.Check(True)
        else:
            print("Optimization method not recognize")
            exit()
        if self.values['methodFILTER'] == 'Conic':
            self.menus.m_filtro1.Check(True)
        elif self.values['methodFILTER'] == 'Hemholz':
            self.menus.m_filtro2.Check(True)
            
        # updating plain assumptions    
        if self.values['tensionplana']:
            self.menus.m_tension.Check(True)
        else:
            self.menus.m_deform.Check(True)
        
        # updating heaviside projection
        if self.values['betaPROJECTION']:
            self.menus.m_withbeta.Check(True)
        else:
            self.menus.m_withoutbeta.Check(True)            
            
        # updating problems
        if self.values['problem'] == 'Compliance':
            self.menus.m_prob1.Check(True)
            self.menus.Volume_Select(False)
        elif self.values['problem'] == 'Volume':
            self.menus.m_prob2.Check(True)
            self.menus.Volume_Select(True)
        elif self.values['problem'] == 'Mechanism':
            self.menus.m_prob3.Check(True)
            self.menus.Volume_Select(False)    
        elif self.values['problem'] == 'Stress':
            self.menus.m_prob4.Check(True)
            self.menus.Volume_Select(True)
        elif self.values['problem'] == 'Multiload':
            self.menus.m_prob5.Check(True)
            self.menus.Volume_Select(False)
            
        
        # updating initialization file
        if self.values['init_file']:
            self.initpanel.rb2.SetValue(True)
            self.initpanel.rbox.Enable(False)
            self.values['init_random'] = False
            self.initpanel.cte.Enable(False)
            self.initpanel.loadfile.SetLabel(os.path.basename(self.values['init_file']))
        elif self.values['init_random']:
            self.initpanel.rb3.SetValue(True)
            self.initpanel.rbox.Enable(True)
            
        else:
            self.initpanel.rb1.SetValue(True)
            self.initpanel.rbox.Enable(False)
            self.initpanel.cte.Enable(True)
            self.initpanel.loadfile.SetLabel('')
        if self.values['seed']:
            self.initpanel.rbox.SetSelection(1)
        else:
            self.initpanel.rbox.SetSelection(0)    
            
        self.initpanel.cte.SetValue(initcte)
        
        # block some panels
        self.Validating()
        
                      
    def Validating(self):
        """
        Check if we enable some menus depending on some values
        """
        
        if self.values['passive_tick'] or self.values['density_tick']:
            self.menus.m_meth2.Enable(False)
            if self.values['methodOPT'] == 'OC':
                self.values['methodOPT'] = 'MMA'
                self.menus.m_meth1.Enable(True)
                self.menus.m_meth1.Check(True)
                self.ShowMessage("Warning","Optimality Conditions are not available with some options. Optimization method has changed to MMA")
        else:
            self.menus.m_meth2.Enable(True)
            if self.values['methodOPT'] != 'OC':
                self.menus.m_prob2.Enable(True)
                self.menus.m_prob3.Enable(True)
                self.menus.m_prob4.Enable(True)
            
        if self.values['density_tick']:
            self.menus.m_prob2.Enable(False)
            self.menus.m_prob3.Enable(False)
            self.menus.m_prob4.Enable(False)
            if self.values['problem'] != 'Compliance':
                self.values['problem'] = 'Compliance'
                self.menus.m_prob1.Enable(True)
                self.ShowMessage("Warning","Problem has changed")
        else:
            if self.values['methodOPT'] != 'OC':
                self.menus.m_prob3.Enable(True)

        if self.values['betaPROJECTION']:
            self.menus.m_withbeta.Check(True)
        else:
            self.menus.m_withoutbeta.Check(True)      
            
        self.menus.changeBeta(self.values['betaPROJECTION'])
        self.menus.checkActivation(None)

        
    def ShowMessage(self,info,message):
        """
        Show a message in a popup window
        """
        wx.MessageBox(message, info, wx.OK | wx.ICON_INFORMATION)       
        
    def Processing(self,event):
        """
        Processing data to create FreeFem++ file and sent to execution
        """
        collection = ('betaloop','filter','eta','betaup','zeta','maxiter',
                      'volfrac','tol','compliance','umbral','maxstress')
        self.updating()
        
#       for debugging values uncommented next line
#        print(self.values)
#        print(self.backvalues)

#        check differences to set up compilation
        result = dictdiffer.diff(self.backvalues,self.values)
        li = list(result)
        
        for x in li:
            if x[0] == 'change' and x[1] in collection:
                continue
            else:
                iscompile = True
                break
        else:
            iscompile = False
            
        self.backvalues = copy.deepcopy(self.values)    

        result = dictdiffer.diff(self.backsettings,self.settings)
        li = list(result)
        if li:
            iscompile = True       
       
            
        self.backsettings = copy.deepcopy(self.settings) 

        procesorcpp.showmessage = self.ShowMessage
        foo = procesorcpp.Processor(self,iscompile,self.values)
        self.caller = foo.caller
        
        self.outputpanel.log.SetValue("")
        self.menus.activar(self.outputpanel)
        self.isprocess = True
        self.menus.m_edp.Enable(True)
        
    def Plot_Result(self):
        """
        Plot vtk result
        """
        del self.graphicmesh.widget
        del self.graphicmesh.ren
        del self.graphicmesh.sizer
        
        self.graphicmesh.initialize()
        filename = os.path.join(self.basepath,self.values['namefile']+'.vtk')
        
        if self.values['problem'] == 'Multiload':
            self.graphicmesh.choices = ['Mesh','Density','Thresholding']
        else:
            self.graphicmesh.choices = ['Mesh','Density','Stress','Deformed Conf.','Thresholding']

        self.graphicmesh.desplegable.Clear()
        
        self.graphicmesh.desplegable.SetItems(self.graphicmesh.choices)
        self.graphicmesh.desplegable.SetSelection(1)
        
        self.graphicmesh.vtkdatos(filename)
        self.graphicmesh.computing_mesh()
        self.graphicmesh.plot_cells()

        self.graphicmesh.mapper_mesh()
        self.graphicmesh.density()
        self.graphicmesh.mapper_den()
        self.graphicmesh.choice_widget.SetSelection(1)
        self.graphicmesh.renderthis(1)


    def Write_Options(self,val):
        message = ''
        
        message += 'Problem: ' + val['problem'] + '\n'
        message += 'Optimizer: ' + val['methodOPT'] + '\n'
        message += 'Filter: ' + val['methodFILTER'] + '\n'
        message += 'Filter radius: ' + val['filter'] + '\n'
        if val['problem'] == 'Volume':
            message += 'Compliance fraction: ' + val['compliance'] + '\n'
        else:
            message += 'Volume fraction: ' + val['volfrac'] + '\n'
            
            
        message += 'Elastic constants: ' + val['young'] + ', ' + \
                  val['young_void'] + ', ' + val['poisson'] + '\n'
        message += 'SIMP parameter: ' + val['simp'] + '\n'
        message += 'Tolerance: ' + val['tol'] + '\n'
        message += 'Max. iterations: ' + val['maxiter'] + '\n'   
        if val['betaPROJECTION']:
            message += 'Heaviside projection loops: ' + val['betaloop'] + '\n'        
            message += 'Heaviside update parameter: ' + val['betaup'] + '\n'
        if val['methodOPT'] == 'OC':
            message += 'OC eta parameter: ' + val['eta'] + '\n'
            message += 'OC zeta parameter: ' + val['zeta'] + '\n'
        if val['problem'] == 'Stress':
            message += 'Stress P norm: ' + val['Pnorm'] + '\n'
            message += 'Stress penalization (q): ' + val['qstress'] + '\n'
        
        return message
    

if __name__ == "__main__":
    
    glv = True
    try:
        glvis = subprocess.Popen('glvis')
    except:
        try:
            gliv = os.path.join(os.path.dirname(os.path.abspath(__file__)),'..','bin','glvis')    
            glvis = subprocess.Popen(gliv)
        except:
            print("No glvis found. Proceed without graphic results.")
            glv = False
    app = wx.App()

    if len(sys.argv)>1:
        file = sys.argv[1]
    else:
        file = None
    frame = MainFrame(None,"Toptimiz3D",file)
    frame.Centre(True)
    frame.Show()
    app.MainLoop()
    if glv:
        glvis.kill()
    print("Application Closed")
