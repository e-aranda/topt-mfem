"""
Created on Thu Jul 27 11:55:55 2017

@author: earanda
"""
import wx
import os
import updater

# ----------------------------------------------------------------------------
class MyPanelLogo(wx.Panel):
    """
    Panel for putting a graphic logo in panel
    """
    def __init__(self,parent):
        wx.Panel.__init__(self,parent)
        t_sizer = wx.BoxSizer(wx.VERTICAL)
        MaxImageSize = 100
        Image = wx.StaticBitmap(self, bitmap=wx.Bitmap(MaxImageSize, MaxImageSize))
        CURRENT_DIR = os.path.dirname(__file__)
        logofile = os.path.join(CURRENT_DIR, os.path.join('icons','logo.ppm'))   
        Img = wx.Image(logofile, wx.BITMAP_TYPE_ANY)
        
        Img = Img.Scale(400,84)
        Image.SetBitmap(wx.Bitmap(Img))
        t_sizer.Add(Image,1,wx.ALL|wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL, 5)
        
        font = wx.Font(5, wx.DEFAULT, wx.NORMAL, wx.NORMAL)   
        script_dir = os.path.dirname(os.path.abspath(__file__))
        myfile = os.path.join(script_dir,'../VERSION')

        with open(os.path.realpath(myfile),'r') as f:
            x = f.readline().strip()
        ver = (x.split('/')[0]).strip()
        version = "Version " + ver   + '\n'+updater.RAMANAME + ' powered with MFEM'
        
        intext = wx.StaticText(self,-1,version)
        intext.SetFont(font)
        t_sizer.Add(intext,1,wx.LEFT)        
        self.SetSizer(t_sizer)

