# -*- codigin: utf-8 -*-
"""
Created on Thu Jul 27 12:01:04 2017

@author: earanda
"""

import wx
import paneltitle
import pexpect
import time
import datetime
import os
from subprocess import getoutput

# ----------------------------------------------------------------------------
class MyPanelTerminal(wx.Panel):
    """
    Panel to show the exit of running FreeFem++. It had to be used with pexpect
    because of a buffering problem
    """
    def __init__(self,parent,padre,titulo):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # variable to stop the running program
        self.stoper = True
        self.padre = padre
        
        
        # title panel
        title = paneltitle.MyPanelTitle(self,titulo)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)

        # panel for terminal box
        panel = wx.Panel(self,-1)
        b_sizer = wx.BoxSizer(wx.VERTICAL)
        
        style = wx.EXPAND|wx.ALL|wx.TE_MULTILINE|wx.TE_READONLY|wx.VSCROLL|wx.TE_RICH2
        self.log = wx.TextCtrl(panel,-1,style=style,size=(200,450))
        
        
        b_sizer.Add(self.log, 0, wx.ALL|wx.EXPAND, 5)
        
        # panel for buttons
        paneldos = wx.Panel(self,-1)
        sizerdos = wx.BoxSizer(wx.HORIZONTAL)
        btn = wx.Button(paneldos,-1, 'Run')
        btn2 = wx.Button(paneldos, -1, 'Stop')

        sizerdos.Add(btn, 2, wx.TOP, 5)
        sizerdos.Add(btn2,2,wx.TOP,5)

        m_sizer.Add(panel,0,wx.TOP|wx.EXPAND,1)
        m_sizer.Add(paneldos,0,wx.CENTER,1)

        panel.SetSizer(b_sizer)
        paneldos.SetSizer(sizerdos)
        
        self.SetSizer(m_sizer)
        
        # bindings
        self.Bind(wx.EVT_BUTTON, self.onButton, btn)
        self.Bind(wx.EVT_BUTTON, self.onStop, btn2)        

    def onStop(self,event):
        """
        set variable to stop line printing
        """
        self.stoper = False
        self.log.AppendText('\n\nEXECUTION STOPPED\n\n')
        
    def onButton(self, event):
        """
        Run and print lines of program
        """
        
        self.stack = []
        self.log.SetValue("")
        self.start_time = time.time()
        
        message = self.padre.Write_Options(self.padre.values)
        strcaller = ''
        for x in self.padre.caller:
            strcaller += x
        self.log.AppendText(strcaller +'\n'+ message + '\n\n')
        self.stack.append(bytes(strcaller +'\n' + message + '\n\n','utf-8'))
        
        os.environ['OMP_NUM_THREADS']='1'
        if self.padre.values['seed']:
            os.environ['GSL_RNG_SEED'] = getoutput("head -1 /dev/urandom | od -N 10 | awk '{print $2}'")[:-1]

        child = pexpect.spawn(strcaller,timeout=None)

        finished = False
    
        # Loop for printings exit lines of running program
        while True and self.stoper:
            wx.Yield()
            try:
                child.expect('\n')
                self.log.AppendText(child.before.decode('utf-8'))
                self.stack.append(child.before)
                if b'PROCESS ENDED' in child.before:
                    self.stoper = False
                    finished = True
                    seconds = (time.time() - self.start_time)
                    tim = str(datetime.timedelta(seconds=seconds))
                    finaltime = "--- Computing Time: {0} ---".format(tim)
                    self.log.AppendText(finaltime)
                    self.stack.append(bytes(finaltime,'utf-8'))
            except pexpect.EOF:
                break
        # To allow new running
        self.stoper = True
        # Plot results in case of finished
        if finished:
            self.padre.Plot_Result()
            self.padre.isrun = True
            self.padre.menus.resultActivation()
        
