"""
Created on Tue Jan 22 10:30:26 2019

@author: earanda
"""


import wx
import paneltitle


# ---------------------------------------------------------------------------   
class PanelSettings(wx.Panel):
    """
    Panel for create settings variables
    """
    def __init__(self,parent,padre,title):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        title = paneltitle.MyPanelTitle(self,title)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        paneles = []
        sizers = []
        static = []
        self.static_e = {}
        # creating panels
        for i,x in enumerate(["ITER","TOL"]):
            paneles.append(wx.Panel(self,style=wx.BORDER_SUNKEN))
            sizers.append(wx.BoxSizer(wx.HORIZONTAL))
            static.append(wx.StaticText(paneles[i],-1,x))
            self.static_e[x] = wx.TextCtrl(paneles[i],-1,str(self.padre.settings[x]),\
                                             style=wx.TE_PROCESS_ENTER|wx.TE_RICH)
            sizers[i].Add(static[i],1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
            sizers[i].Add(self.static_e[x],1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
            m_sizer.Add(paneles[i], 0, wx.TOP|wx.EXPAND , 1)
            paneles[i].SetSizer(sizers[i])
            # bindins
            self.Bind(wx.EVT_TEXT, lambda event, val=x: self.set_val(event,val), self.static_e[x])
        
        # panel for visualization
        panel = wx.Panel(self,-1)
        b_sizer = wx.BoxSizer(wx.VERTICAL)
        
        
        self.vis = wx.CheckBox(panel,-1,"Visualization")
        b_sizer.Add(self.vis, 0, wx.ALL|wx.EXPAND, 5)
        
        self.suite = wx.CheckBox(panel,-1,"SuiteSparse")
        b_sizer.Add(self.suite, 0, wx.ALL|wx.EXPAND, 5)
        panel.SetSizer(b_sizer)
        
        m_sizer.Add(panel,0,wx.TOP|wx.EXPAND,1)    
            
        # Main sizer
        self.SetSizer(m_sizer)
    
        self.Bind(wx.EVT_CHECKBOX,self.OnTick,self.vis)
        self.Bind(wx.EVT_CHECKBOX,self.OnTicksuite,self.suite)
        
    def set_val(self,event,x):
        self.padre.settings[x] = event.EventObject.GetValue()
        
    def OnTick(self,event):
        if self.vis.Value:
            self.padre.settings['visualization'] = True
        else:
            self.padre.settings['visualization'] = False
            
    def OnTicksuite(self,event):
        if self.suite.Value:
            self.padre.settings['suitesparse'] = True
            for x in self.static_e:
                self.static_e[x].Enable(False)
        else:
            self.padre.settings['suitesparse'] = False  
            for x in self.static_e:
                self.static_e[x].Enable(True)