# -*- coding: utf-8 -*-
"""
Created on Tue May 23 13:12:33 2017

@author: earanda

Basic c++ codes
"""

adaptacion = None

codigo = {}

# carga de paquetes
codigo['constants'] = """
const int spdim = {dimension};
static int p = {simp}; 
static double E1 = {young};
static double E0 = {young_void};
static double nu = {poisson};
static double Initcte = {initcte};
const double E10 = E1-E0;
const double lambda = {lamb};
const double mu = 1/(2.*(1.+nu));

static double q = {qstress}; // stress penalty
static double PNORM = {pnorm}; // norm stress

const int n_f = {multiload_dim};
const double lambdacoef[{multiload_dim}] = {{ {multiload_val} }};

{suitesparse}
#ifndef MFEM_USE_SUITESPARSE
static double TOL = {TOL};
static int ITER = {ITER};
#endif
"""

codigo['$dummy$'] = ''


codigo['$displacements$_ML'] = """
    GridFunction *Vdisp[n_f];
    for (int i = 0; i<n_f; i++)
    {
        Vdisp[i] = new GridFunction(p1espace);
        *Vdisp[i] = 0.0; // define in this way to set b.c. 
    }
    
"""

codigo['$RHSdef$'] = """
    LinearForm *b;
    b = new LinearForm(p1espace);
"""

codigo['$RHSdef$_ML'] = """
    LinearForm *b[n_f];
    for (int i = 0; i<n_f; i++)
        b[i] = new LinearForm(p1espace);
"""


codigo['$assamblingb$_ML'] = """
    for (int i=0;i<n_f;i++)
        b[i]->Assemble();
"""

codigo['$assamblingder$_ML'] = """
    for (int i=0;i<n_f;i++)
        der[i]->Assemble();
"""


codigo['$defderivative$'] = """
    LinearForm *der;
    der = new LinearForm(p0espace);
    der->AddDomainIntegrator(new DomainLFIntegrator(derivative));
"""    


codigo['$defderivative$_ML'] = """
    LinearForm *der[n_f];
    CoeffDerivative *derivative[n_f];
    for (int i=0; i<n_f; i++)
    {
       der[i] = new LinearForm(p0espace);
	   derivative[i] = new CoeffDerivative(*Rhobar,*Vdisp[i],one);
       der[i]->AddDomainIntegrator(new DomainLFIntegrator(*derivative[i]));
    }
"""    


codigo['$initialsystem$'] = """
    SparseMatrix A;
    Vector B, U;
    a->Update();
    a->Assemble();
    a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

    // Solving system with PCG or UMFPACK (if installed)   
    #ifndef MFEM_USE_SUITESPARSE
        GSSmoother M(A);
        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
    #else
        UMFPackSolver umf_solver(true);
        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
        umf_solver.SetOperator(A);
        umf_solver.Mult(B, U);
    #endif
"""


codigo['$initialsystem$_ML'] = """
    SparseMatrix A[n_f];
    Vector B[n_f], U[n_f];
    #ifndef MFEM_USE_SUITESPARSE
        GSSmoother M;
    #else
        UMFPackSolver umf_solver(true);
        umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
    #endif
    double compliance = 0.;
    double compli;

    a->Update();
    a->Assemble();
    for (int i=0; i<n_f; i++)
    {
        a->FormLinearSystem(ess_tdof_list, *Vdisp[i], *b[i], A[i], U[i], B[i]);

        // Solving system with PCG or UMFPACK (if installed)   
        #ifndef MFEM_USE_SUITESPARSE
            M = GSSmoother(A[i]);
            PCG(A[i], M, B[i], U[i], 0, ITER, TOL, 0.0);
        #else
            umf_solver.SetOperator(A[i]);
            umf_solver.Mult(B[i], U[i]);
        #endif
        
        compli = ((Vector) *b[i]) * U[i];
        compliance += lambdacoef[i]*compli;
    }
"""



codigo['$filtering$'] = """
    u.Filtering(*Rhobar);
"""

codigo['$filtering$_WH'] = """
    u.FilteringWH(*Rhobar);
"""



codigo['$vectorD_NLOPT$'] = """
    for (int i=0; i<x.size(); i++)
        grad[i] = D(i);
"""    



codigo['$vectorD_NLOPT$_WH'] = """
    u.Filtering(*Rhohat);
    dconres->Update();
    dconres->Assemble();
    			
    Vector Y(x.size());
    rho->FilMult(*dconres,Y);
    Y *= Volconstant;
    for (int i=0; i<x.size(); i++)
    	grad[i] = Y(i);
"""    



codigo['$vectorVolD_NLOPT$'] = """
    for (int i=0; i<x.size(); i++)
        grad[i] = D(i);
"""      

codigo['$vectorVolD_NLOPT$_WH'] = """
    u.Filtering(*Rhohat);
    dconres->Update();
    dconres->Assemble();
    Vector Y(x.size());
    rho->FilMult(*dconres,Y);
    Y *= Volconstant;
    for (int i=0; i<x.size(); i++)
    	grad[i] = Y(i);
"""      




codigo['$vectorVolD$'] = """
    for (int i=0; i<D.Size(); i++)
        grad_f[i] = D(i);
"""


codigo['$vectorVolD$_WH'] = """
    Filter &u = *rho;
    for (int i = 0; i<n ; i++)
    	u(i) = x[i];	
    			
    u.Filtering(*Rhohat);
    dconres->Update();
    dconres->Assemble();
    			
    Vector Y(n);
    rho->FilMult(*dconres,Y);
    Y *= Volconstant;
    for (int i=0; i<n; i++)
    	grad_f[i] = Y(i);
"""


codigo['$vectorD_OC$'] = ""    

codigo['$vectorD_OC$_WH'] = """
    u.Filtering(*Rhohat);
    dconres->Update();
    dconres->Assemble();
    rho->FilMult(*dconres,D);
"""




codigo['$vectorD$'] = """
    for (int i=0; i<n; i++)
    	values[i] = D(i);
"""    


codigo['$vectorD$_WH'] = """
    Filter &u = *rho;
    for (int i = 0; i<n ; i++)
    	u(i) = x[i];	
    			
    u.Filtering(*Rhohat);
    dconres->Update();
    dconres->Assemble();
    			
    Vector Y(n);
    rho->FilMult(*dconres,Y);
    Y *= Volconstant;
    for (int i=0; i<n; i++)
    	values[i] = Y(i);
"""    



codigo['$extra$_WH'] = """
    u.Filtering(*Rhohat);
""" 
codigo['$extra$'] = ""

codigo['$stress$'] = """
    // definition of constant integration rule for computing Lp norm
    int order_to_integrate = 0;
    
    const IntegrationRule *irs[Geometry::NumGeom];
    for (int i=0; i < Geometry::NumGeom; ++i)
    {
      irs[i] = &(IntRules.Get(i, order_to_integrate));
    }	        		
"""

codigo['$include$_IPOPT'] = """
#include "func_IPOPT.hpp"
#include "coin/IpIpoptApplication.hpp" 
#include "coin/IpSolveStatistics.hpp"
"""

codigo['$include$_NLOPT'] = """
#include "func_NLOPT.hpp"
#include <nlopt.hpp> // for nlopt
"""

codigo['$include$_OC'] = """
#include "func_OC.hpp" 
"""


codigo['$initialfilter$'] = """
    ToptimizBase::boolbeta = 0;
    rho = new {namefilter};
    {derivativemech}
"""

codigo['$initialfilter$_WH'] = """
    ToptimizBase::boolbeta = 1;
    rho = new {namefilter};
    GridFunction *Rhohat;
    Rhohat = new GridFunction(p0espace);
    *Rhohat = 0.;
    CoeffPsip Psip(*Rhohat,beta,eta);
    {derivativemech}
"""


codigo['$density$'] = """
    b->Update();
    b->Assemble(); 
"""

codigo['$dconres$'] = """
    dconres->AddDomainIntegrator(new DomainLFIntegrator(one));
"""

codigo['$dconres$_WH'] = """
    dconres->AddDomainIntegrator(new DomainLFIntegrator(Psip));
"""


codigo['$adjrhs$'] = """
    // rhs of adjoint problem for stress derivative
    LinearForm *adjrhs;
    adjrhs = new LinearForm(p1espace);
        
    MatrixAdjStress{tipo} phiv(*Rhobar,*Udisp,Mobj);
    adjrhs->AddDomainIntegrator(new SigmaIntegrator(phiv));
    
    adjrhs->Assemble();
"""


codigo['$delete$'] = ""

codigo['$delete$_WH'] = "delete Rhohat;"

codigo['$deletearray$'] = """
    delete b;
    delete der;
"""

codigo['$deletearray$_ML'] = """
    for (int i=0;i<n_f;i++)
    {
        delete b[i];
        delete der[i];
        delete Vdisp[i];
    }
"""


codigo['$method$_IPOPT'] = """
    // normal inizialization for IPOPT
    Vector lb(ndim);
    GridFunction &uu = *rho;
    for (int i=0; i<ndim; i++)
       {{
           lb(i) = xx(i);
           uu(i) = max(initialization(i),lb(i));
       }}
    Ipopt::SmartPtr<Ipopt::TNLP> mynlp;
        
    mynlp = new {clase}
        
    Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
    
    app->RethrowNonIpoptException(true);
    app->Options()->SetNumericValue("tol", tol);
    app->Options()->SetIntegerValue("max_iter", maxiter);
    app->Options()->SetIntegerValue("print_level", 0);
    app->Options()->SetStringValue("mu_strategy", "adaptive");
    app->Options()->SetStringValue("hessian_approximation", "limited-memory");
    
    Ipopt::ApplicationReturnStatus status;
    status = app->Initialize();
    
    if (status != Solve_Succeeded) 
    {{
      cout << endl << endl << "*** Error during initialization!" << endl;
      return (int) status;
    }}
    
    status = app->OptimizeTNLP(mynlp);

"""

codigo['$after$_IPOPT'] = """
    if (status == 0 || status == -1 || status == 1) 
      cout << endl << endl << "*** Problem solved!" << endl;
    else 
      cout << endl << endl << "*** Exit with status " << status << endl;
    double objfin = (double)app->Statistics()->FinalObjective();

"""


codigo['$after$_OC'] = 'double objfin = obj.compliance;'
codigo['$after$_NLOPT'] = 'double objfin = minf;'


codigo['$method$_NLOPT'] = """
    {clase}
    
    unsigned n = p0espace->GetTrueVSize();
    
    nlopt::opt opt(nlopt::LD_MMA, n);
    vector<double> lb(n), ub(n), x(n);
    // initialization of bounds and starting point
    
    for (int i=0; i<n; i++)
    {{
        lb[i] = xx(i);
        ub[i] = 1.;
        x[i] = max(initialization(i),lb[i]);
    }}
    
    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);
    opt.set_xtol_rel(tol);
    opt.set_maxeval(maxiter);
    opt.set_min_objective(wrapper, &obj);
    opt.add_inequality_constraint(conswrapper, &obj, 1e-8);
    
    nlopt::result result;
    double minf;
    
    result = opt.optimize(x, minf);
    """
    
codigo['$method$_OC'] = """
    {clase}
    obj.OCiteration(maxiter,tol); 
    """
    
codigo['$bucleHeaviside$'] = ""
    
codigo['$bucleHeaviside$_WH'] = """
    for (int i = 1; i< loops; i++)
    {{
        beta *= betaupdate;
        cout << "Beta changed: " << beta << endl;
        Psip.UpdateConstant(beta,eta);
        rho->UpdateConstant(beta,eta);
        if (i == loops-1) ToptimizBase::boolbeta = 0;
        {metodo}
}}      
"""     


codigo['$finalsol$_NLOPT'] = """
Filter &u = *rho;
for (int i = 0; i<x.size() ; i++)
    u(i) = x[i];    

{filtering}
  
// updating displacements

    a->Update();
    a->Assemble();
    a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);

    // Solving system with PCG or UMFPACK (if installed)   
    #ifndef MFEM_USE_SUITESPARSE
        PCG(A, M, B, U, 0, ITER, TOL, 0.0);
    #else
        umf_solver.SetOperator(A);
        umf_solver.Mult(B, U);
    #endif
  
    if (visualization && ToptimizBase::sout.good())
    {{
      ToptimizBase::sout << "solution\\n" << *mesh << *Rhobar << endl; 
      ToptimizBase::sout << "plot_caption 'Final Result -- Iterations: " << obj.siter <<
      "  Compliance: " << minf/Mobj << "'" << endl;    
    }}
   
"""


codigo['$finalsol$_NLOPT_Vol'] = """
    Filter &u = *rho;
    for (int i = 0; i<x.size() ; i++)
        u(i) = x[i];
    
    {filtering}
        
    // updating displacements
        a->Update();
        a->Assemble();
        a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
    
        // Solving system with PCG or UMFPACK (if installed)   
        #ifndef MFEM_USE_SUITESPARSE
            PCG(A, M, B, U, 0, ITER, TOL, 0.0);
        #else
            umf_solver.SetOperator(A);
            umf_solver.Mult(B, U);
        #endif
    
    if (visualization && ToptimizBase::sout.good())
    {{
      ToptimizBase::sout << "solution\\n" << *mesh << *Rhobar << endl; 
      ToptimizBase::sout << "plot_caption 'Final Result -- Iterations: " << obj.siter <<
      "  Volume: " << minf << "'" << endl;    
    }}
    
"""


codigo['$finalsol$_NLOPT_ML'] = """
    Filter &u = *rho;
    for (int i = 0; i<x.size() ; i++)
        u(i) = x[i];    
    
    {filtering}

        *Udisp = 0.;
      
    if (visualization && ToptimizBase::sout.good())
    {{
      ToptimizBase::sout << "solution\\n" << *mesh << *Rhobar << endl; 
      ToptimizBase::sout << "plot_caption 'Final Result -- Iterations: " << obj.siter <<
      "  Compliance: " << minf/Mobj << "'" << endl;    
    }}
       
"""






codigo['$finalsol$_NLOPT_Str'] = """
    Filter &u = *rho;
    for (int i = 0; i<x.size() ; i++)
        u(i) = x[i];
    
    {filtering}
        
      
    // updating displacements
        a->Update();
        a->Assemble();
        a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
    
        // Solving system with PCG or UMFPACK (if installed)   
        #ifndef MFEM_USE_SUITESPARSE
            PCG(A, M, B, U, 0, ITER, TOL, 0.0);
        #else
            umf_solver.SetOperator(A);
            umf_solver.Mult(B, U);
        #endif
    
    
    if (visualization && ToptimizBase::sout.good())
    {{
      ToptimizBase::sout << "solution\\n" << *mesh << *Rhobar << endl; 
      ToptimizBase::sout << "plot_caption 'Final Result -- Iterations: " << obj.siter <<
      "  Volume: " << minf << " Max. norm: " << obj.mstr << "'" << endl;    
    }}
   
"""


codigo['$finalsol$_IPOPT'] = ''

codigo['$finalsol$_IPOPT_ML'] = ""

codigo['$finalsol$_OC'] = """
    Filter &u = *rho;
    
    {filtering}
        
    if (visualization && ToptimizBase::sout.good())
    {{
      ToptimizBase::sout << "solution\\n" << *mesh << *Rhobar << endl; 
      ToptimizBase::sout << "plot_caption 'Final Result -- Iterations: " << obj.siter <<
      "  Compliance: " << obj.compliance << "'" << endl;    
    }}

"""



codigo['$finalsol$_OC_ML'] = """
    Filter &u = *rho;
    
    {filtering}
        
    if (visualization && ToptimizBase::sout.good())
    {{
      ToptimizBase::sout << "solution\\n" << *mesh << *Rhobar << endl; 
      ToptimizBase::sout << "plot_caption 'Final Result -- Iterations: " << obj.siter <<
      "  Compliance: " << obj.compliance << "'" << endl;    
    }}
    
"""


codigo['$savesol$_NLOPT'] = """

    gridstress->ProjectCoefficient(*stressr);
    
    ofstream sol_pure(\"{solepure}\");
    sol_pure.precision(8);
    u.Save(sol_pure);
    
    ofstream sol_ofs(\"{solfilter}\");
    sol_ofs.precision(8);
    Rhobar->Save(sol_ofs);
    
    
    // saving vtk file
    ofstream ff(\"{solvtk}\");
    mesh->VTKConverter(ff,Rhobar{extras});  
"""

codigo['$savesol$_IPOPT'] = """
    Filter &u = *rho;
    
    // Prepare stress for saving
    
    gridstress->ProjectCoefficient(*stressr);
    
    // saving rho
    ofstream sol_pure(\"{solepure}\");
    sol_pure.precision(8);
    u.Save(sol_pure);
    
    // saving filtered rho
    ofstream sol_ofs(\"{solfilter}\");
    sol_ofs.precision(8);
    Rhobar->Save(sol_ofs);


    // saving vtk file
    ofstream ff(\"{solvtk}\");
    mesh->VTKConverter(ff,Rhobar{extras});  
"""

codigo['$savesol$_OC'] = codigo['$savesol$_NLOPT']


codigo['$thresholding$'] = """
    *y = 1.;
    string name1 = \"{name1}\";
    string name2 = \"{name2}\";
    Thresholding thre(40,Rhobar,y,vol,medida,Volfrac,name1,name2);
    
    
    double maxstress = gridstress->Max();
    cout << "-------- Max. Stress: " << maxstress << endl;
    cout << "--------------------------------" << endl;
"""

codigo['$thresholding$_vol'] = """
    *y = 1.;
    string name1 = \"{name1}\";
    string name2 = \"{name2}\";
    Thresholding thre(Rhobar,y,vol,medida,objfin,name1,name2,40);
    
    
    double maxstress = gridstress->Max();
    cout << "-------- Max. Stress: " << maxstress << endl;
    cout << "--------------------------------" << endl;
"""
