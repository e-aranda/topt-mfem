# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:44:30 2017

@author: earanda
"""
import os
import wx
import vtk
from vtk.wx.wxVTKRenderWindowInteractor import wxVTKRenderWindowInteractor
import vtk.util.numpy_support as VN
import numpy as np
import mainbar
import math
import windowvtk

# ----------------------------------------------------------------------------
class MiPanelVtk(wx.Panel):
    """
    Special panel for VTK visualization 
    """

    def __init__(self,parent,padre):
        wx.Panel.__init__(self, parent)
        
        self.isplotedbefore = False
        self.padre = padre   
        self.choices = ['Mesh']
        self.initialize()
        self.nt = 0 # number of elements
        self.np = 0 # number of nodes
        self.bounds = None
        self.axisview = False
        self.plotfromcombo = False
        
        
    def initialize(self):     
        """
        Window and sizer initialization
        """
        self.widget = wxVTKRenderWindowInteractor(self, -1)
        self.widget.GetInteractorStyle().SetCurrentStyleToTrackballCamera()

        self.ren = vtk.vtkRenderer()
        self.ren.SetBackground((.96,.96,.96))        
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.widget.GetRenderWindow().AddRenderer(self.ren)        
        # control plot bar (axis, wireframe/surface)        
        self.plotbar = mainbar.MainBar(self)
        self.controlbar()
        
        self.sizer.Add(self.plotbar, 0, wx.EXPAND)
        self.sizer.Add(self.widget, 1, wx.EXPAND)
        
        self.SetSizer(self.sizer)
        self.Layout()
        

    def vtkdatos(self,filename):  
        """
        Reading vtk unstructuredgrid file and get information of
        number of cells and types
        Connected to MainFrame
        """
        self.reader = vtk.vtkUnstructuredGridReader()
        self.reader.SetFileName(filename)
        self.reader.ReadAllScalarsOn()
        self.reader.Update() # Needed because of GetScalarRange
        self.output = self.reader.GetOutput()
        self.set_plotbar_enable()
        
        
    def computing_mesh(self):
        """
        Computing dimension, nodes and cells from mesh, 
        inner and boundary labels
        """
        # extract cell type  
        
        celdas = self.output.GetCellTypesArray()
        if celdas:
            self.tipoceldas = VN.vtk_to_numpy(celdas)
        else:
            self.padre.ShowMessage("Warning","Mesh has no cells.")
            return 1
        
        # extract labels
        vtklabel = self.output.GetCellData().GetArray(0)
        self.nplabels = VN.vtk_to_numpy(vtklabel) # array with all labels
        
        # check the celltype of the mesh and pass to setdim 
        # set index of inner cell and boundary cell
        rangcel = celdas.GetValueRange()
#        print("Celdas",rangcel)
        self.ind_int = max(rangcel)
        self.ind_boun = min(rangcel)
        if self.ind_int == 5:
            arrayname = "Mesh Triangle Quality"
            objeto = 'SetTriangleQualityMeasureToArea'
        elif self.ind_int == 9:
            arrayname = "Mesh Quadrilateral Quality"
            objeto = 'SetQuadQualityMeasureToArea'
        elif self.ind_int == 10:
            arrayname = "Mesh Tetrahedron Quality"
            objeto = 'SetTetQualityMeasureToVolume'
        elif self.ind_int == 12:
            arrayname = "Mesh Hexahedron Quality"
            objeto = 'SetHexQualityMeasureToVolume'
        else:
            self.padre.ShowMessage("Warning","Mesh is not recognized.")
            return 1
        # set inner and boundary labels    
        mask, = np.where(self.tipoceldas==self.ind_boun)
        self.boundarylabels = np.unique(self.nplabels[mask])
        mask2, =  np.where(self.tipoceldas==self.ind_int)
        self.innerlabels = np.unique(self.nplabels[mask2])
        
        # set the number of main cells (triangles or tetrahedron)
        self.nt = np.sum(self.tipoceldas==self.ind_int)
        # set the number of points
        self.np = self.output.GetNumberOfPoints()
        
        self.bounds = self.output.GetBounds()
        
        # Mesh quality: measure of elements 
        # quality = [min,mean,max]
        ir = vtk.vtkMeshQuality()
        ir.SetInputConnection(self.reader.GetOutputPort())
        getattr(ir,objeto)()
        ir.Update()
        
        an = ir.GetOutput().GetFieldData().GetArray(arrayname)
        self.quality = []
        for i in range(3):
            self.quality.append(an.GetComponent(0,i))
        return 0

        
    def plot_cells(self):
        """
        Computing actors for all plotting inner and boundary regions
        """
        
        self.actorInnList = {}
        self.actorBndList = {}
        
        for tipo,act,labels in zip( (self.ind_int,self.ind_boun), (self.actorInnList,self.actorBndList),
                             (self.innerlabels,self.boundarylabels)):
            
            for a in labels:
                # get indices where there are line cells
                lineas, = np.where(np.logical_and(self.nplabels==a,self.tipoceldas==tipo))
                
                # associate to a vtk vector
                ids = VN.numpy_to_vtk(lineas)
        
                # get selection
                selectionNode = vtk.vtkSelectionNode()
                selectionNode.SetFieldType(vtk.vtkSelectionNode.CELL)
                selectionNode.SetContentType(vtk.vtkSelectionNode.INDICES)
                selectionNode.SetSelectionList(ids)
         
                # add to generic selection object
                selection = vtk.vtkSelection()
                selection.AddNode(selectionNode)
         
                # npi
                extractSelection = vtk.vtkExtractSelection()
                extractSelection.SetInputConnection(self.reader.GetOutputPort())
                extractSelection.SetInputData(1,selection)
                extractSelection.Update()
         
                # In selection
                selected = vtk.vtkUnstructuredGrid()
                selected.ShallowCopy(extractSelection.GetOutput())
             
                selectedMapper = vtk.vtkDataSetMapper()
                selectedMapper.SetInputData(selected)
                selectedMapper.ScalarVisibilityOff()    
         
                selectedActor = vtk.vtkActor()
                selectedActor.SetMapper(selectedMapper)    
                
                selectedActor.GetProperty().SetRepresentationToWireframe()
                
                act[a] = selectedActor

    
    def render_actor(self,a,type=0):
        """
        Render specific actor (boundary (type 1) or inner (type 0)) 
        with label=a
        """
        
        self.plotfromcombo = True
        if type:            
            actorlist = self.actorBndList
            color = (1.,0.,0.)
            width = 5
        else:
            actorlist = self.actorInnList
            color = (1.,0.,1)
            width = 4
            

        for actor in actorlist.values():
            actor.GetProperty().SetColor((0.0, 0.0, 1.0))
            actor.GetProperty().SetLineWidth(1.0)  
            
        if not type:
            self.actor.GetProperty().SetRepresentationToSurface()
            self.choice_widget.SetSelection(2)
        selectedActor = actorlist[a]
        selectedActor.GetProperty().SetDiffuseColor(color)
        selectedActor.GetProperty().SetLineWidth(width)

        self.widget.Render()
 
        
    def mapper_mesh(self):
        """
        Create the mapper that corresponds the objects of the vtk file
        into graphics elements
        """
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputData(self.output)
        self.mapper.ScalarVisibilityOff()
        
    def mapper_den(self):
        """
        Set mapper for visualization in case of plotting result (density)
        """
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputData(self.output)
        lut = vtk.vtkLookupTable()
        lut.SetTableRange(0,100)
        lut.SetHueRange(0, 0)
        
        lut.SetSaturationRange(0, 0)
        lut.SetValueRange(1., 0.)
        
#        scalar_range = self.output.GetScalarRange()
#        self.mapper.SetScalarRange(scalar_range)
        self.mapper.SetLookupTable(lut)
        
        
        
    def mapper_stress(self):
        """
        Set mapper for visualization in case of plotting result (stress)
        """
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputData(self.output)
        
        self.output.GetCellData().SetActiveScalars('stress')
        b = self.output.GetCellData().GetScalars()
        scalar_range = b.GetRange()
#        scalar_range = self.output.GetPointData().Set
        
        lut = vtk.vtkLookupTable()
        lut.SetTableRange(0,100)
        lut.SetHueRange(0.65,0.5)
        lut.SetSaturationRange(0, 4.)
        lut.SetValueRange(1, 0.)
#        lut.Build()
        
        
        self.mapper.SetScalarRange(scalar_range)
        self.mapper.SetLookupTable(lut)
 
    # create the scalar_bar

        self.legend_actor = vtk.vtkScalarBarActor()
        self.legend_actor.SetLookupTable(lut)
        self.legend_actor.SetTitle('Stress')
        self.legend_actor.SetWidth(0.1)
        self.legend_actor.SetPosition(.88,.1)

    def displacement(self):
        """
        For displaced structure
        """
        self.output.GetCellData().SetScalars(self.output.GetCellData().GetArray(1))
        
        self.output.GetPointData().SetActiveVectors('displacements')
        
        warp = vtk.vtkWarpVector()
        warp.SetInputData(self.output)
        
        # compute scale factor
        maxnorm = self.output.GetPointData().GetArray(0).GetMaxNorm()
        bo = self.bounds
        bom = max(bo[1]-bo[0],bo[3]-bo[2],bo[5]-bo[4])
        a = maxnorm/bom*100
        if a>20:
            a = 20/a
        elif a<2:
            a = 2/a
        warp.SetScaleFactor(a)
        warp.Update()
        
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputData(warp.GetOutput())
        
               
        lut = vtk.vtkLookupTable()
        lut.Build()
        
        lut.SetTableRange(0,100)
        lut.SetHueRange(0, 0)
        
        lut.SetSaturationRange(0, 0)
        lut.SetValueRange(1., 0.)
        self.mapper.SetLookupTable(lut)
        
        

        
    def density(self,array = 0):
        """
        Special scalar for density visualization
        """
        self.output.GetCellData().SetActiveScalars('density')
        
#        self.output.GetCellData().SetScalars(self.output.GetCellData().GetArray(array))
	

    def renderthis(self,a=0,b=False,c=False,d=True):
        """
        Showing data: render + actor + camera positioning
        """ 
        if self.padre.values['dimension'] == 2:  
            self.widget.RemoveObservers('RightButtonPressEvent')
            self.cellPicker = vtk.vtkCellPicker()
            self.cellPicker.SetTolerance(1.e-2)  
            self.widget.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())    
#            self.widget.AddObserver("MouseMoveEvent", self.OnMotionTwo)   
            self.widget.AddObserver("RightButtonPressEvent",self.OnMotionTwo)
        else:
            self.widget.RemoveObservers("MouseMoveEvent")
            self.cellPicker = vtk.vtkCellPicker()
            self.cellPicker.SetTolerance(1.e-6)  
            self.widget.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
            self.widget.AddObserver("RightButtonPressEvent", self.OnMotionThree)     

        self.texto = ''
        
        self.txt = vtk.vtkTextActor()
        self.txt.SetInput(' ')
        
        txtprop=self.txt.GetTextProperty()
        txtprop.SetFontFamilyToArial()
        txtprop.SetFontSize(22)
        txtprop.SetColor(.3,.3,.3)
        self.txt.SetDisplayPosition(10,10)

        self.ren.RemoveAllViewProps()
        # not add boundary and inner actors
        if d:
            for act in self.actorBndList.values():
                self.ren.AddActor(act)
            for act in self.actorInnList.values():
                self.ren.AddActor(act)
                  
            self.ren.AddActor(self.txt)
            
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)
        
        # style representation
        if a == 0:
            self.actor.GetProperty().EdgeVisibilityOn()
            self.actor.GetProperty().SetRepresentationToWireframe()
        elif a == 1:
            self.actor.GetProperty().SetRepresentationToSurface()
            self.actor.GetProperty().EdgeVisibilityOff()
        elif a == 2:
            self.actor.GetProperty().SetRepresentationToSurface()
            self.actor.GetProperty().EdgeVisibilityOn()

        self.actor.GetProperty().SetColor((0.,0.,1.))
        
        self.ren.AddActor(self.actor)
        # add a color bar
        if b:
            self.ren.AddActor(self.legend_actor)
                       
        self.ren.ResetCamera()
        self.ren.ResetCameraClippingRange()
        cam = self.ren.GetActiveCamera()
        cam.Elevation(0)
        cam.Azimuth(0)
        

        
        
    def controlbar(self):
        """
        Buttons for controlling graphic
        """
        
        # add views
        self.add_centering()
        
        # add wireframe/surface 
        self.add_styleview()
        
        # add show axis
        self.add_showaxis()
        
        # add show results
        self.add_showresult()
        
        self.plotbar.actual_add()
  

        
    def add_centering(self):
        """
        Change orientation button
        """
        orient_choices = ('XY view', 'XZ view', 'YZ view', 'iso view')
        self.orient_widget = wx.Choice(self.plotbar, wx.ID_ANY, choices = orient_choices)
        self.orient_widget.SetSelection(0) # in windows appears without selection
        self.orient_widget.Bind(wx.EVT_CHOICE, self.orient_event)
        # add to bar of buttons
        self.plotbar.add(self.orient_widget)
        
        
    def add_styleview(self):
        """
        Change wireframe/surface view style
        """
        style_choices = ('Wireframe', 'Surface', 'Surface+Edges')
        self.choice_widget = wx.Choice(self.plotbar, wx.ID_ANY, choices = style_choices)
        self.choice_widget.Bind(wx.EVT_CHOICE, self.style_event)
        # add to bar of buttons
        self.plotbar.add(self.choice_widget)
        self.choice_widget.SetSelection(0) # in windows appears without selection        
        
    def add_showaxis(self):
        """
        Show axis
        """
        self.axis_widget = wx.Button(self.plotbar,wx.ID_ANY, label="Axis On/Off")
        self.axis_widget.Bind(wx.EVT_BUTTON, self.axis_event)
        self.plotbar.add(self.axis_widget)
        
        
    def orient_event(self, event):
        """
        Event to change orientation
        """
        pos = self.orient_widget.GetSelection()
        self.orient_inter(pos)
        self.widget.Render()


    def orient_inter(self, pos):
        """
        Select orientation
        """
        if pos == 0:
            self.orient_handler((0.0,0.0,1.0))
        elif pos == 1:
            self.orient_handler((0.0,1.0,0.0),(0,0,1)) # necesario cambiar ViewUp
        elif pos == 2:
            self.orient_handler((1.0,0.0,0.0))
        elif pos == 3:
            v = math.sqrt(1.0/3.0)
            self.orient_handler((v,v,v))
            
    def orient_handler(self, unit, up=(0.0,1.0,0.0)):
        """
        Set orientation
        """
        self.ren.ResetCamera() # posicion, tamanho
        fp = self.ren.GetActiveCamera().GetFocalPoint()
        p = self.ren.GetActiveCamera().GetPosition()
        dist = math.sqrt( (p[0]-fp[0])**2 + (p[1]-fp[1])**2 + (p[2]-fp[2])**2 )
        self.ren.GetActiveCamera().SetPosition(fp[0]+dist*unit[0], fp[1]+dist*unit[1], fp[2]+dist*unit[2])
        self.ren.GetActiveCamera().SetViewUp(up)
        
    def style_event(self,event):
        """
        Event for changing the mesh representation: wireframe, surface or both
        """
        
        pos = self.choice_widget.GetSelection()
        if pos == 0:
            self.actor.GetProperty().SetRepresentationToWireframe()
            self.actor.GetProperty().EdgeVisibilityOn()
        elif pos == 1:
            self.actor.GetProperty().SetRepresentationToSurface()
            self.actor.GetProperty().EdgeVisibilityOff()
        elif pos == 2:
            self.actor.GetProperty().EdgeVisibilityOn()
            self.actor.GetProperty().SetRepresentationToSurface()
#            self.actor.GetProperty().SetLineWidth(1);
#            self.actor.GetProperty().SetEdgeColor(1,1,1);
            
        self.Refresh()
        
    def axis_event(self,event):

        pos = not self.axisview        
        if pos:
            axesActor = vtk.vtkAxesActor()
            self.orientation = vtk.vtkOrientationMarkerWidget()
            self.orientation.SetOrientationMarker(axesActor)
            self.orientation.SetInteractor(self.widget)
            self.orientation.EnabledOn()
            self.orientation.InteractiveOn()           
        else:
            self.orientation.EnabledOff()
                     
        self.Refresh()
        self.axisview = pos
        
        
    def add_showresult(self):
        """
        Button for showing results
        """
        
        self.desplegable = wx.ComboBox(self,-1, pos=(300,30),size =(100,-1),\
                    choices = self.choices, style = wx.ALIGN_CENTRE)
        self.desplegable.SetSelection(0)
        
        if not self.isplotedbefore:
            self.desplegable.Enable(False)    
            self.padre.graphicbot.innercombo.Enable(False)
            self.padre.graphicbot.boundcombo.Enable(False)

            
        self.Bind(wx.EVT_COMBOBOX,self.show_results,self.desplegable)    
        # add to bar of buttons
        self.plotbar.add(self.desplegable)    
        self.plotbar.actual_add()
        
        
    def set_plotbar_enable(self):
        """
        Enabling the bar buttons
        """            
        if self.isplotedbefore:
            self.plotbar.Enable(True)
        else:
            self.plotbar.Enable(False)
        

    def show_results(self,event):
        pos = self.desplegable.GetStringSelection()
        del self.widget
        del self.ren
        del self.sizer
        self.initialize()
        self.desplegable.SetStringSelection(pos) # 
        
        if pos == 'Mesh':
            filename = os.path.join(self.padre.basepath,self.padre.values['namefile'] + "_mesh.vtk")
            self.vtkdatos(filename)
            self.computing_mesh()
            self.plot_cells()
            self.mapper_mesh()
            self.choice_widget.SetSelection(0)
            self.renderthis()
        elif pos == 'Density':
            filename = os.path.join(self.padre.basepath,self.padre.values['namefile'] + ".vtk")
            self.vtkdatos(filename)
            self.output.GetPointData().SetActiveScalars('density')
            self.density()
            self.mapper_den()
            self.choice_widget.SetSelection(1)
            self.renderthis(1,d=False)
        elif pos == 'Stress':
            filename = os.path.join(self.padre.basepath,self.padre.values['namefile'] + ".vtk")
            self.vtkdatos(filename)
            self.output.GetPointData().SetActiveScalars('stress')
#            self.density('stress')
            self.mapper_stress()
            self.choice_widget.SetSelection(1)
            self.renderthis(1,True,d=False)    
        elif pos == 'Deformed Conf.':
            filename = os.path.join(self.padre.basepath,self.padre.values['namefile'] + ".vtk")
            self.vtkdatos(filename)
            self.displacement()
            self.choice_widget.SetSelection(1)
            self.renderthis(1,d=False)  
        elif pos == 'Thresholding':
            filename = os.path.join(self.padre.basepath,self.padre.values['namefile'] + ".vtk")
            self.vtkdatos(filename)
            self.output.GetPointData().SetActiveScalars('density')
            self.density()
            self.mapper_den()
            self.choice_widget.SetSelection(1)
            
            self.renderthis(1,d=False)

            filenames = os.path.join(self.padre.basepath,self.padre.values['namefile']+'-threshold.vtk')
            threshold_frame = windowvtk.New1DFrame(self,filenames,"Threshold computation")
            threshold_frame.Show()
            self.desplegable.SetSelection(1) 
                       
        # Block labels selection in case of no mesh showed
        if pos == 0:
            self.padre.graphicbot.innercombo.Enable(True)
            self.padre.graphicbot.boundcombo.Enable(True)
        else:
            self.padre.graphicbot.innercombo.Enable(False)
            self.padre.graphicbot.boundcombo.Enable(False)


    def OnMotionThree(self, obj, event):
       
      
       renwin = obj.GetRenderWindow()
       ren = renwin.GetRenderers().GetFirstRenderer()
       x, y = obj.GetEventPosition()
           
       self.cellPicker.Pick(x, y, 0, ren)

       
       if self.plotfromcombo:
           for actor in self.actorBndList:
               self.actorBndList[actor].GetProperty().SetColor((0.0, 0.0, 1.0))
               self.actorBndList[actor].GetProperty().SetPointSize(1.0)
               self.actorBndList[actor].GetProperty().SetLineWidth(1.0)   
               self.texto = ''     
           for actor in self.actorInnList:
               self.actorInnList[actor].GetProperty().SetColor((0.0, 0.0, 1.0))
               self.actorInnList[actor].GetProperty().SetPointSize(1.0)
               self.actorInnList[actor].GetProperty().SetLineWidth(1.0)   
               
       self.plotfromcombo = False
      
       dataSet = self.cellPicker.GetDataSet()
       if dataSet is not None:           
           for actor in self.actorBndList:
               if self.actorBndList[actor].GetMapper().GetInput() == dataSet:
                   self.padre.graphicbot.boundcombo.SetSelection(self.padre.graphicbot.boundlabel.index(str(actor)))
                   self.actorBndList[actor].GetProperty().SetColor((1.0, 0.0, 0.0))
                   self.actorBndList[actor].GetProperty().SetPointSize(3.0)
                   self.actorBndList[actor].GetProperty().SetLineWidth(5.0)  
                   self.texto +=  ' ' + str(actor) 
                   self.txt.SetInput(self.texto)                   
       else:
           for actor in self.actorBndList:
               self.actorBndList[actor].GetProperty().SetColor((0.0, 0.0, 1.0))
               self.actorBndList[actor].GetProperty().SetPointSize(1.0)
               self.actorBndList[actor].GetProperty().SetLineWidth(1.0)   
               self.texto = ' '   
               self.txt.SetInput(self.texto)
       renwin.Render()
       
       
    def OnMotionTwo(self, obj, event):
       
       renwin = obj.GetRenderWindow()
       ren = renwin.GetRenderers().GetFirstRenderer()
       x, y = obj.GetEventPosition()

       self.cellPicker.Pick(x,y, 0, ren)
       if self.plotfromcombo:
           for actor in self.actorBndList:
               self.actorBndList[actor].GetProperty().SetColor((0.0, 0.0, 1.0))
               self.actorBndList[actor].GetProperty().SetPointSize(1.0)
               self.actorBndList[actor].GetProperty().SetLineWidth(1.0)   
               self.texto = ''     
           for actor in self.actorInnList:
               self.actorInnList[actor].GetProperty().SetColor((0.0, 0.0, 1.0))
               self.actorInnList[actor].GetProperty().SetPointSize(1.0)
               self.actorInnList[actor].GetProperty().SetLineWidth(1.0)   
               
       self.plotfromcombo = False
        
       dataSet = self.cellPicker.GetDataSet()
       if dataSet is not None:   
           for actor in self.actorBndList:
               if self.actorBndList[actor].GetMapper().GetInput() == dataSet:
                   self.padre.graphicbot.boundcombo.SetSelection(self.padre.graphicbot.boundlabel.index(str(actor)))
                   self.actorBndList[actor].GetProperty().SetColor((1.0, 0.0, 0.0))
                   self.actorBndList[actor].GetProperty().SetPointSize(3.0)
                   self.actorBndList[actor].GetProperty().SetLineWidth(5.0)  
                   self.texto +=  ' ' + str(actor) 
                   self.txt.SetInput(self.texto)   
       else:
           for actor in self.actorBndList:
               self.actorBndList[actor].GetProperty().SetColor((0.0, 0.0, 1.0))
               self.actorBndList[actor].GetProperty().SetPointSize(1.0)
               self.actorBndList[actor].GetProperty().SetLineWidth(1.0)   
           self.texto = ' '
           self.txt.SetInput(self.texto)
       renwin.Render()       
       
       
          