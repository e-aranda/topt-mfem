# -*- coding: utf-8 -*-
"""
Created on Sat Jul 15 19:51:21 2017

@author: earanda

External functions
"""
import os
from subprocess import getstatusoutput, run

def is_number(s):
    """ Returns True if string is a number. """
    try:
        float(s)
        return True
    except ValueError:
        return False        
    
def checkempty(cad):
    """ Returns True is not empty string """
    if cad.isspace() == True or not cad:
        return False
    else:
        return True

def string2number(s):
    """
    Convert a string to a number (int or float)
    """
    try:
        return int(s)
    except ValueError:
        return float(s)
    
    
def vtkbuilder(inp,out):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    myfile = os.path.normpath(os.path.join(script_dir,'../bin/meshtovtk'))
    command = myfile + " -m " + inp + " -o " + out
    err,info2 = getstatusoutput(command) 
    if not err:
        return err,''        
    else:
        return err,info2


def readdim(inp):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    myfile = os.path.normpath(os.path.join(script_dir,'../bin/meshtovtk'))
    res = run([myfile,"-m",inp]) 
    return res.returncode

