# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:58:59 2017

@author: earanda
"""

import wx
import wx.grid as  gridlib
import paneltitle
import os
        
class MyPanelMultiLoads(wx.Panel):
    """
    Panel containing grid with boundary loads for multiload case
    """
    def __init__(self,parent,padre,titulo,var):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        self.var = var
        
        # title panel
        title = paneltitle.MyPanelTitle(self,titulo)
        
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        # add button panel
        addbot = wx.Panel(self)
        b_sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        tot = wx.StaticText(addbot,-1,"Add new load")
        CURRENT_DIR = os.path.dirname(__file__)
        pngfile = os.path.join(CURRENT_DIR, os.path.join('icons','list-add.png'))   
        png = wx.Image(pngfile, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        bot = wx.BitmapButton(addbot, -1, png)
        bot.SetBackgroundColour(wx.Colour(240, 240, 240))  
        bot.Bind(wx.EVT_BUTTON, self.addrow)
        b_sizer.Add(tot,3,wx.RIGHT|wx.ALIGN_CENTER_VERTICAL,3) 
        b_sizer.Add(bot,1, 1)
        m_sizer.Add(addbot,0, wx.ALL|wx.ALIGN_RIGHT,15)
        addbot.SetSizer(b_sizer)
        
        # blank grid with data
        self.grid = wx.grid.Grid(self)
        self.grid.CreateGrid(1,5)
        self.grid.SetColLabelValue(0, "fx")
        self.grid.SetColLabelValue(1, "fy")
        self.grid.SetColLabelValue(2, "fz")
        self.grid.SetColLabelValue(3, "Labels")
        self.grid.SetColLabelValue(4, "Weights")
        
        for i in range(5):
            self.grid.SetColSize(i,65)
        
        m_sizer.Add(self.grid, 1, wx.EXPAND|wx.ALL|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 10)
        self.SetSizer(m_sizer)
        
        # bindings
        self.Bind(gridlib.EVT_GRID_LABEL_RIGHT_CLICK,self.showPopupMenu,self.grid)
        
        self.Bind(gridlib.EVT_GRID_CELL_CHANGED, self.OnCellChange)

    def showPopupMenu(self, event):
        """
        Create and display a popup menu on right-click event
        on label cell to delete it
        Connected to MainFrame through delrow
        """            
        menu = wx.Menu()
        itemdel = wx.MenuItem(menu,-1, "Delete Row")
        self.Bind(wx.EVT_MENU, lambda event, x=event.GetRow(): self.delrow(event,x), itemdel)
        menu.AppendItem(itemdel)
        self.PopupMenu(menu)
        menu.Destroy()        

    def addrow(self,event):
        """
        Add a row to a grid and the corresponding values in MainFrame
        Connected to MainFrame
        """
        self.grid.AppendRows(1)
        row = self.grid.GetNumberRows()-1
        self.padre.values[self.var].append(['','','','',''])
        if self.padre.values['dimension'] == 2:
            self.grid.SetCellBackgroundColour(row, 2, (225,225,225))
            self.grid.SetReadOnly(row,2,True)
        
    def delrow(self,event,x):
        """
        Delete a row and the corresponding values in MainFrame
        Connected to MainFrame
        """
        self.grid.DeleteRows(x)
        self.padre.values[self.var].pop(x)
        
    def OnCellChange(self, evt):
        """
        Update value in corresponding values in MainFrame
        Connected to MainFrame
        """
        self.padre.values[self.var][evt.GetRow()][evt.GetCol()] = \
            self.grid.GetCellValue(evt.GetRow(), evt.GetCol())

    def set_dim(self,dim):
        """
        Change access to 3rd coordinate in case of changing dim
        """
        if dim == 2:
            for row in range(self.grid.GetNumberRows()):
                    self.grid.SetCellValue(row,2,'')
                    self.grid.SetCellBackgroundColour(row, 2, (225,225,225))
                    self.grid.SetReadOnly(row,2,True)
        else:
            for row in range(self.grid.GetNumberRows()):
                    self.grid.SetReadOnly(row,2,False)
                    self.grid.SetCellBackgroundColour(row, 2, (255,255,255))
                    self.grid.Refresh()
